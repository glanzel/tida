<div class="proposals form">
<?php echo $this->Form->create('Decision', array('action' => 'save'));?>
	<fieldset>
 		<legend><?php echo __('Diskussionsthema hinzufügen'); ?></legend>
	<?php
		echo $this->Form->input('laufzeit', array('div' => false, 'class' => 'laufzeit', 'value' => '14')); 
		echo " (in tagen)<br>";
		echo $this->Form->input('title', array('class' => 'title_input'));
		echo $this->Form->input('beschreibung');
		echo $this->Form->input('kptnTodocat_id', array('label' => 'Typ'));
		if($cox){
			echo $this->Form->input('admingroup_id', array('label' => 'Sichtbarkeit', 'value' => 8));
			echo $this->Form->label('stimmberechtigt');
			echo $this->Form->select('stimmberechtigt', $admingroups, array('value' => 8));
		}
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
