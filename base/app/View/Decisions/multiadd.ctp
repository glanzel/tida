<div class="kptnTodoMultiadd index">

<?php echo $this->Form->create('Decision', array('action' => 'multiadd'));?>

<div>
<?php echo __('Gesprächstermin'); ?>
<? echo $this->Form->select('appointment_id', $applist, array('name' => "data[all][Decision][appointment_id]", 'value' => $appointment_id)); ?>

</div>
		
<table class="todotable" cellpadding="0" cellspacing="0"  border="0" rules="cols"  >
<tr class="firsttr">
	<th class="firsttd">Titel</th>
	<th>Beschreibung</th>
	<th>Typ</th>
	<th>laufzeit</th>
	<th>wer</th>
</tr>

<?php
$i = 0;
for($nr = 0; $nr < 10; $nr++){
?>
	<tr style="border=0">
		<td class="firsttd">
			<?php echo $this->Form->input("title$nr", array('label' => false, 'name' => "data[$nr][Decision][title]", 'class' => "todoWas", 'onkeydown' => "ctr_navigieren(this, event.keyCode, 'KptnTodoWas', $nr, null, 'KptnTodoBeschreibung$nr')"));  ?>
		</td>
		<td>
			<?php echo $this->Form->textarea("beschreibung$nr", array('rows' => 1, 'label' => false, 'name' => "data[$nr][Decision][beschreibung]", 'class' => "todoBeschreibung", 'onkeydown' => "ctr_navigieren(this, event.keyCode, 'KptnTodoBeschreibung', $nr, 'KptnTodoWas$nr', 'KptnTodoTyp$nr')"));  ?>
		</td>
		<td>
			<? echo $this->Form->input("kptn_todocat_id", array('label' => false, 'id' => "KptnTodoTyp$nr", 'name' => "data[$nr][Decision][kptnTodocat_id]", 'onkeydown' => "ctr_navigieren(this, event.keyCode, 'KptnTodoTyp', $nr, 'KptnTodoBeschreibung$nr', 'KptnTodoWer$nr')")); ?>
		</td>
		<td>
		<? echo $this->Form->input('laufzeit', array('label' => false, 'div' => false, 'class' => 'todoWer', 'value' => '14', 'name' => "data[$nr][Decision][laufzeit]")); ?>
		</td>

		
	</tr>
<? } ?>
</table>
</div>
<div class="paging">
<div class="howmuch">
	<?php echo $this->Form->end('Speichern');?>
</div><br>
<div style="font-size:9px">
</div>
</div>
