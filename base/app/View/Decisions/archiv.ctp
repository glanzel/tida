<div class="decisions index">
<!--	
	[<?php echo $this->Html->link(__('Neues Diskussionsthema'), array('controller' => 'Decisions', 'action'=>'add')); ?>]
	[<?php echo $this->Html->link(__('Neuer Vorschlag'), array('controller' => 'Proposals', 'action'=>'create')); ?>]
	[<?php echo $this->Html->link(__('offene Entscheidungen'), array('controller' => 'Decisions', 'action'=>'index')); ?>]
	<br><br>
-->
	<?
		foreach($typen as $id => $typ){
			echo $this->Html->link($typ, array('action' => 'archiv', "typ" => $typ));
			echo " | ";
		}
		echo $this->Html->link('alle', array('action' => 'archiv'));
		
	?>
	<br>
	<br>
	<?= $this->Form->create('Decision', array('action' => 'saveCat'));?>

	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd"><?php echo $this->Paginator->sort('stichtag');?></th>
			<th><?php echo $this->Paginator->sort('Decision.title');?></th>
			<th><?php echo $this->Paginator->sort('kptnTodocat_id');?></th>
			<th><?php echo $this->Paginator->sort('Proposal.title');?></th>
			<th></th>
	</tr>
	<?php
	$i = 0;
	foreach ($decisions as $decision):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td class="firsttd"><?php echo date('d.m.Y', strtotime($decision['Decision']['stichtag'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['Decision']['title'], array('action' => 'result', $decision['Decision']['id'])); ?>&nbsp;</td>
		<td><?= $this->Form->select($decision['Decision']['id'], $typen, array('value' => $decision['KptnTodocat']['id'])); ?>

		<td>
		<? 
			foreach($decision['Votegroup'] as $votegroup){
				if(isset($votegroup['Solution']['title'])){
					echo $this->Html->link($votegroup['Solution']['title'], array('controller' => 'Proposals','action' => 'view', $votegroup['Solution']['id']));
					echo "</br>";
				}
			} 
		?>
		</td>
	<td style="width:59px">
		<?	
			if($decision['Decision']['umgesetzt_am'] != '0000-00-00'){
				echo $this->Html->link($this->Html->image("checkmark_green.gif", array("border" => 0)), array('action'=>'done', $decision['Decision']['id']),array('escape' => false),null,false); 
			}else{
				echo $this->Html->link($this->Html->image("checkmark_grey.gif", array("border" => 0)), array('action'=>'done', $decision['Decision']['id']),array('escape' => false),null,false); 
								
			}
		?>
		<?php echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0, 'title' => 'bearbeiten')), array('action'=>'edit', $decision['Decision']['id']),array('escape' => false),null,false); ?>
		<?php if($cox) echo $this->Html->link($this->Html->image("x.gif", array("border" => 0, 'title' => 'in Kat Unwichtig')), array('action'=>'papierkorb', $decision['Decision']['id']), array('escape' => false), null, false); ?>
		<?= $this->Html->link($this->Html->image("milestone.png", array("border" => 0, 'width' => 16)), array('action'=>'solute', $decision['Decision']['id'], 0),array('escape' => false),null,false);?> 

	</td>
	</tr>
<?php endforeach; ?>
<tr ><td class="firsttd"></td><td></td>
<td>
	<? echo $this->Form->end('Kat speichern');?>
</td>
<td colspan=3 style="height:35px;">
	<div class="suche" style=" padding-top:3px;height:30px">
	<?php echo $this->Form->create('Decision', array('action' => 'suche/archiv'));?>
	<? echo $this->Form->input('suche', array('div' => false, 'label' => false)); ?>
	<?php echo $this->Form->submit(__('Suche'), array('div' => false));?>
	</div>

</td>
</tr>
	</table>
	

<div>
	<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous'), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next').' >>', array(), null, array('class'=>'disabled'));?>
	</div>
</div>

</div>
