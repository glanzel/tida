<div class="decisions view">
	[<?php echo $this->Html->link(__('Abstimmen'), array('controller' => 'Decisions', 'action'=>"view/".$decision['Decision']['id'])); ?>]
<? if(Configure::read('Decision.gewichten')) {?>
	[<?php echo $this->Html->link(__('Gewichtung ausschalten'), array('controller' => 'Decisions', 'action'=>"result/".$decision['Decision']['id']."/0")); ?>]
<? } ?>

<h4><?php echo $decision['Decision']['title']; ?> </h4>


<table class="result">
<tr>
<td></td>
<? foreach($voteSummen as $vote){ ?>
	<? $i = $vote['Votegroup']['lfdNr']%2; ?>
	<td class="result_v<?=$i?>"> <?= $this->Html->link($vote['Proposal']['title'], array('controller' => 'Proposals', 'action'=>"view/".$vote['Proposal']['id'])) ?>
	</td> 
<?	}  ?>
</tr>
<tr style="font-size:10px;">
<td></td>
<? foreach($voteSummen as $vote){ ?>
	<td> <?= substr($vote['Proposal']['beschreibung'], 0, 120); ?><? if(strlen($vote['Proposal']['beschreibung']) > 120) echo "..."; ?>&nbsp;
	<? if(! empty($vote['DecisionComment']['comment'])) echo "[k]"; ?>
	</td> 
<?	}  ?>
</tr>
<? foreach($votes as $username => $uservotes){ ?>
	<tr><td class="col_title"><?= $username ?>&nbsp;<span class="decision_gewichtung"><? if(isset($users[$username]['gewichtung'])) echo '('.$users[$username]['gewichtung'].'%)' ?></span> </td>
	<? foreach($uservotes as $nr => $vote){ ?>
		<td class="value">
		<span class="decision_gewichtung">
			<?= $vote['Votecat']['name']; 
			if(isset($users[$username]['gewichtung'])){
				if(! isset($voteSummen[$nr][0]['neupkt'])) $voteSummen[$nr][0]['neupkt'] = 0; 
				$neupkt = $users[$username]['gewichtung']*$vote['Votecat']['pkt'];
				echo '('.$neupkt.')';
				$voteSummen[$nr][0]['neupkt'] += $neupkt;
				
			} 
			?> 
		</span>
		&nbsp;<span class="value_desc"><? if(isset($vote['Vote']['erstellt_von'])) echo $vote['Vote']['erstellt_von']; ?><span></td> 
	<? } ?>
	</tr>
<? } ?>

<td>pkt</td>
<? foreach($voteSummen as $vote){ ?>
	<td><?= isset($vote[0]['neupkt']) ? $vote[0]['neupkt'] : $vote[0]['pkt']; ?> <? if($vote[0]['aufschiebend']) echo(" <span class='veto'>(veto)</span>")  ?></td> 
<?	}  ?>
</tr></table>	
<span class="votegroup"><?= $this->Html->link('->plenumsmodus', array('controller' => 'Decisions', 'action'=>"multivote/".$decision['Decision']['id'])); ?>
</div>
