<div class="decisions index">
	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd"><?php echo $this->Paginator->sort('stichtag');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('votes');?></th>
			<th><?php echo $this->Paginator->sort('Typ','kptnTodocat_id');?></th>
			<th><?php echo $this->Paginator->sort('Autor', 'user_id');?></th>
			<th></th>
	</tr>
	<?php
	$i = 0;
	foreach ($decisions as $decision):
		$class = null;
		if($decision['Decision']['sumProposal'] > count($decision['Vote'])) $class = "class=openProposal";
	?>
	<tr <?=$class?>>
		<td class="firsttd"><?php echo date('d.m.Y', strtotime($decision['Decision']['stichtag'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['Decision']['title'], array('action' => 'view', $decision['Decision']['id'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['Decision']['voteQuote'], array('action' => 'result', $decision['Decision']['id'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['KptnTodocat']['title'], array('action' => 'index', 'typ' => $decision['KptnTodocat']['title'])); ?>&nbsp;</td>
		<td><?= $decision['User']['name']; ?>&nbsp;</td>
		<? if($decision['Decision']['solutions'] < 2) { ?> 
			<td><?= $this->Html->link($this->Html->image("milestone_grey.png", array("border" => 0, 'width' => 16)), array('action'=>'solute', $decision['Decision']['id'], 2),array('escape' => false),null,false);?> 
		<? } else { ?> 
		<td><?= $this->Html->link($this->Html->image("milestone.png", array("border" => 0, 'width' => 16)), array('action'=>'solute', $decision['Decision']['id'], 0),array('escape' => false),null,false);?>
		<? } ?> 
	</tr>
<?php endforeach; ?>
	</table>

<div style="padding-bottom:10px; ">
	<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous'), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next').' >>', array(), null, array('class'=>'disabled'));?>
	</div>

	<div class="suche" style="float:right; padding-bottom:20px; ">
	<?php echo $this->Form->create('Decision', array('action' => 'suche'));?>
	<? echo $this->Form->input('suche', array('div' => false, 'label' => false)); ?>
	<?php echo $this->Form->submit(__('Suche'), array('div' => false));?>
	</div>
</div>
</div>
