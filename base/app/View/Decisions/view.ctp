<div class="decisions view">
	[<?php echo $this->Html->link(__('Vorschlag hinzufügen'), array('controller' => 'Proposals', 'action'=>"add/".$decision['Decision']['id'])); ?>]
	<? if($stimmberechtigt){?> 
	[<?php echo $this->Html->link(__('Abstimmungsgruppe hinzufügen'), array('controller' => 'Decisions', 'action'=>"addGroup/".$decision['Decision']['id'])); ?>]
	[<?php echo $this->Html->link(__('Ergebnis ansehen'), array('controller' => 'Decisions', 'action'=>"result/".$decision['Decision']['id'])); ?>]
	<? } ?>
	[<?php echo $this->Html->link(__('bearbeiten'), array('controller' => 'Decisions', 'action'=>"edit/".$decision['Decision']['id'])); ?>]
	<? if($empty){ ?> 	
	[<?php echo $this->Html->link(__('löschen'), array('controller' => 'Decisions', 'action'=>"delete/".$decision['Decision']['id'])); ?>]
	<? } ?>


<div class="decision_title"> <?php echo $decision['Decision']['title']; ?>
</div> bis <?php echo date('d.m.Y', strtotime($decision['Decision']['stichtag'])); ?>
 (<? 
 echo $this->Html->link('+1W Bedenkzeit', array('action' => "stichtagPlus/".$decision['Decision']['id']."/7"));
 if($decision['Decision']['bedenkzeit']) echo " - bereits ".$decision['Decision']['bedenkzeit']."mal verlängert";
 ?>)  
<div class="decision_beschreibung"><?php echo $decision['Decision']['beschreibung']; ?></div>
<br>
<? 	foreach($votegroups as $groupname => $prop){ 
	$proposals = $prop['proposals'];
	?>
	<span class="votegroup">Abstimmungsgruppe <?= $groupname ?></span>
	<? 		//pr($proposals);
	if(empty($proposals)){
		$url = array('action'=>'delGroup', $prop['id'],$decision['Decision']['id']);
		echo $this->Html->link($this->Html->image("x.gif", array("border" => 0, "title" => "Gruppe löschen")), $url, array('escape' => false), null ,false); 
	} ?>
	<br>
	
	<?	foreach($proposals as $key => $proposal){ ?> 
		<div class="proposal_vote">	
		<? 
		if($stimmberechtigt)
			foreach($votecats as $votecat_id => $votecat_name){ 
				$htmlclass = isset($votes[$proposal['Proposal']['id']]) &&  $votes[$proposal['Proposal']['id']] == $votecat_id ? "class=voted" : "" ?>
				<div  <?=$htmlclass?> >
				<?= $this->Html->link($votecat_name, array('controller' => 'Votes', 'action' => 'add/'
						.$decision['Decision']['id'].'/'
						.$proposal['Proposal']['id'].'/'
						.$votecat_id)); 
						?>
				</div>		
			<? } ?>
	</div>
	<div class="proposal">
	<div class="proposal_title"><?= $proposal['Proposal']['title'] ?>	
	<div class=right>		

	<? 
	$commenturl = array('controller' => 'decision_comments', 'action'=>'add', $decision['Decision']['id'], $proposal['Proposal']['id'],'backid' => $decision['Decision']['id']);
	echo $this->Html->link($this->Html->image("note.gif", array("border" => 0, "title" => "Kommentar hinzufügen")), $commenturl, array('escape' =>false), null,false); 
	//pr($proposal);
	if($proposal['User']['username'] == $username || isset($superuser)){ 
		$editurl = array('controller' => 'proposals', 'action'=>'edit', $proposal['Proposal']['id'],$decision['Decision']['id'],'backid' => $decision['Decision']['id']);
		echo " ".$this->Html->link($this->Html->image("edit.gif", array("border" => 0, "title" => "Vorschlag editieren")), $editurl, array('escape' => false), sprintf(__('Achtung alle abstimmungen gehen verloren - Wirklich editieren'), $proposal['Proposal']['title']),false); 
		$url = array('controller' => 'proposals', 'action'=>'delete', $proposal['Proposal']['id'],'backid' => $decision['Decision']['id']);
		echo " ".$this->Html->link($this->Html->image("x.gif", array("border" => 0, "title" => "Vorschlag löschen")), $url, array('escape' => false), sprintf(__('Wirlich # %s löschen'), $proposal['Proposal']['title']),false); 

		echo $this->Form->create('Proposal', array('name' => 'agchange'.$proposal['Proposal']['id'], 'action' => "changeGroup/".$proposal['Proposal']['id']."/".$decision['Decision']['id'], 'class' => 'votegroupChooser'));
		echo $this->Form->select('votegroup_id', $grouplist, array('value' => $prop['id'],'onchange' => "this.form.submit()"));
		//echo $this->Form->submit();
		echo $this->Form->end();
	}
	?>
	</div>
	</div>	
	<div class="proposal_desc"><div class="proposal_desc_inner"><?= $proposal['Proposal']['beschreibung'] ?></div>
	<? 
		if(!empty($proposal['DecisionComment'])){ 
			echo "<div class='proposal_comment'>";
			foreach($proposal['DecisionComment'] as $comment){
				echo($comment['comment']);
				echo(" - ".$comment['User']['name']);
				echo("<br>");
			}
			echo("</div>");
		}
	?>
	
	
	</div>
	</div>
		
	<br>
<? } ?>	
<? } ?>	
	
</div>

<div>
<?php echo __('Füge Termin hinzu'); ?>
			<?php 
				echo $this->Form->create('Appointment', array('action' => "addAppToDecision/".$decision['Decision']['id']));
				
				echo $this->Form->select('id', $applist);
				echo $this->Form->end('Submit')
			?>
			&nbsp;
</div>
<div >
	<dl>
	<dt><b>Termine</b></dt>
	<?php if (!empty($decision['Appointment']))?>
	<dd>
	<?php
		$i = 0;
		foreach ($decision['Appointment'] as $app){ ?>

			<div class="groupuser"> <? echo $app['title']; ?> 
			<? if($cox) echo " ".$this->Html->link($this->Html->image("x.gif", array("border" => 0, 'title' => 'löschen')), 
						array('controller' => 'appointments', 'action'=>'removeUserFromGroup', $decision['Decision']['id'], $app['id']), array('escape' => false), 
							sprintf(__('Wirlich # %s löschen'), $decision['Decision']['title']), false); 
			?>
			<?= ', ' ?>
			</div>	
		<? } ?>
	</dd>
	</dl>
	<br class="break">
</div>
