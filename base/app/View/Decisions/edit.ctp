<div class="proposals form">
<?php echo $this->Form->create('Decision');?>
	<fieldset>
 		<legend><?php echo __('Diskussionsthema bearbeiten'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title', array('class' => 'title_input'));
		echo $this->Form->input('beschreibung');
		echo $this->Form->input('kptnTodocat_id', array('label' => 'Typ'));
		if($cox){ 
			echo $this->Form->input('admingroup_id', array('label' => 'Sichtbarkeit'));
			echo $this->Form->label('stimmberechtigt');
			echo $this->Form->select('stimmberechtigt', $admingroups, array('value' => $this->request->data['Decision']['stimmberechtigt']));
		}	
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
