<div class="crewview admingroups view">
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($admingroup['Admingroup']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Desc'); ?></dt>
		<dd>
			<?php echo h($admingroup['Admingroup']['desc']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div >
	<dl>
	<dt><b><?php echo 'Mitglieder';?></b></dt>
	<?php if (!empty($admingroup['User'])):?>
	<dd>
	<?php
		$i = 0;
		foreach ($admingroup['User'] as $user): ?>
			<div> <? echo $user['username']; ?> 
			<? if($cox) echo " ".$this->Html->link($this->Html->image("x.gif", array("border" => 0, 'title' => 'löschen')), 
						array('controller' => 'users', 'action'=>'removeUserFromGroup', $admingroup['Admingroup']['id'], $user['id']), array('escape' => false), 
							sprintf(__('Wirlich # %s löschen'), $admingroup['Admingroup']['name']), false); 
			?>
			<?= ', ' ?>
			</div>	
		<?php endforeach; ?>
	</dd>
	</dl>
	<br class="break">
	<dl>
		<dt><?php echo __('Füge Benutzer hinzu'); ?></dt>
		<dd>
			<?php 
				echo $this->Form->create('User', array('action' => "addUserToGroup/".$admingroup['Admingroup']['id']));
				
				echo $this->Form->select('id', $userlist);
				echo $this->Form->end('Submit')
			?>
			&nbsp;
		</dd>
	</dl>
	
<?php endif; ?>

</div>
