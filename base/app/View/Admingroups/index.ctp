<div class="admingroups index crewindex">
	<table class="crewtable" cellpadding="0" cellspacing="0">
	<tr class="firsttr">
			
		<th class=firsttd ><?php echo $this->Paginator->sort('name');?></th>
			
		<th  ><?php echo $this->Paginator->sort('desc');?></th>
			
		<th class="actions"><?php echo __(' ');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($admingroups as $admingroup): ?>
	<tr>
		<td class=firsttd ><?php echo $this->Html->link($admingroup['Admingroup']['name'], array('action' => 'view', $admingroup['Admingroup']['id']));  ?>&nbsp;</td>
		<td  ><?php echo h($admingroup['Admingroup']['desc']); ?>&nbsp;</td>
		<td class="actions">

			<? if($cox){ ?>
				<?php echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0, 'title' => 'bearbeiten')), array('action'=>'edit', $admingroup['Admingroup']['id']),array('escape' => false),null,false); ?>
				<?php if($superuser) echo $this->Html->link($this->Html->image("x.gif", array("border" => 0, 'title' => 'löschen')), 
						array('action'=>'delete', $admingroup['Admingroup']['id']), array('escape' => false), 
							sprintf(__('Wirlich # %s löschen'), $admingroup['Admingroup']['name']), false); ?>
			<? } ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>

	<div class="paging">
	<div class="symbols">

	<?php
		echo $this->Paginator->prev('<< ', array(), null, array('class' => 'prev disabled'));
		echo '|';
		echo $this->Paginator->numbers(array('separator' => ''));
		echo '|';
		echo $this->Paginator->next(' >>', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	<div class="howmuch">
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Zeige Datensätze %start% - %end% (%count% insgesamt)')
	));
	?>	</div>
	</div>
	
</div>

