<div class="mails form">
Emails exportiern : 
<?= $this->Html->link('Alle Zusagen', array('controller' => 'Participations', 'action' => 'exportAdresses', ZUSAGE))?> |
<?= $this->Html->link('Zusage aber nicht bezahlt', array('controller' => 'Participations', 'action' => 'exportAdresses', ZUSAGE, 0))?> |
<?= $this->Html->link('Bezahlt', array('controller' => 'Participations', 'action' => 'exportAdresses', ZUSAGE, 1))?>

<?php echo $this->Form->create('Mail', array('type' => 'file', 'url' => "sendwebmail/"));?>
	<fieldset>
 		<legend><?php echo __('Email an Gruppe versenden'); ?></legend>
	<?php
		echo $this->Form->label('an');
		echo $this->Form->select('to', $groups, array('value' => 16));
		echo "<br>";
		//echo $this->Form->label('kategorie');
		//echo $this->Form->select('kptn_todocat', $kptn_todocats, array('value' => 8));
		echo $this->Form->input('subject');
		echo $this->Form->input('content');
		echo $this->Form->label('Anhang');
		echo $this->Form->file('Document.file');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
