<div class="kptnTodos index">

<?
		foreach($typen as $id => $typ){
			echo $this->Html->link($typ, array('action' => 'index', 'typ' => $typ));
			echo " | ";
		}
		echo $this->Html->link('alle', array('action' => 'index'));
?>
<br><br>
		
<table class="todotable" cellpadding="0" cellspacing="0"  >
<tr class="firsttr">
	<th class="firsttd"><?php echo $this->Paginator->sort('datum');?></th>
	<th><?php echo $this->Paginator->sort('receiver');?></th>
	<th><?php echo $this->Paginator->sort('subject');?></th>
	<th><?php echo $this->Paginator->sort('content');?></th>
	<th><?php echo $this->Paginator->sort('Typ','kptn_todocat_id');?></th>
	<th class="actions"></th>
</tr>

<?php
echo $this->Form->create('Mail', array('action' => 'saveCat'));

$i = 0;
foreach ($mails as $key => $mail):
?>
	<tr style="border=0">
		<td class="firsttd">
			<span  <? if($lastlogin < $mail['Mail']['datum']) echo('style=font-weight:bold;') ?>  >
			<?php echo date('d.m.Y' , strtotime( $mail['Mail']['datum'])); ?>
			</span>
		</td>
		<td>
			<?php echo substr($mail['Mail']['receiver'],0,stripos($mail['Mail']['receiver'],'@')); ?>
		</td>
		<td>
			<?php echo $this->Html->link($mail['Mail']['subject'], array('action'=>'view', $mail['Mail']['id'])); ?>
		</td>
		<td>
			<?php echo substr(strip_tags($mail['Mail']['content']),0, 60); ?>
		</td>
		<td>
			<? 
			if($cox){
				echo $this->Form->select($mail['Mail']['id'], $kptnTodocats, array('value' => $mail['KptnTodocat']['id'])); 
			} else {
				echo $mail['KptnTodocat']['title']; 
			}
			?>
		</td>
		
		<td class="actions">
			<?php echo $this->Html->link($this->Html->image("info.gif", array("border" => 0, 'title' => 'ansehen')), array('action'=>'view', $mail['Mail']['id']),array('escape' => false),null,false); ?>

			<? if($cox){ ?>
				<?php //echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0, 'title' => 'bearbeiten')), array('action'=>'edit', $mail['Mail']['id']),array('escape' => false),null,false); ?>
				<?php echo $this->Html->link($this->Html->image("trash.gif", array("border" => 0, 'title' => 'In den Papierkorb')), array('action'=>'papierkorb', $mail['Mail']['id']), array('escape' => false), null,false); ?>
			<? } ?>
			<?
				if(! empty($mail['Mail']['attachment'])) 
					foreach(explode(',', $mail['Mail']['attachment']) as $filename){
						 echo $this->Html->link($this->Html->image("document.gif", array("border" => 0, 'title' => $filename)), array('controller' => '/media/', 'action' => $mail['Mail']['id'].'/'.$filename),array('escape' => false),null,false); 
					}
			?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
<?php if ($cox) echo $this->Form->end('Kategorien speichern');?>
</div>
<div class="paging">
<div class="symbols">
	<?php echo $this->Paginator->prev('<< ', array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(' >>', array(), null, array('class'=>'disabled'));?>
</div>	
<div class="howmuch">
	<?php
		echo $this->Paginator->counter(array(
		'format' => __('Zeige Datensätze %start% - %end% (%count% insgesamt)')
		));
	?>
</div><br>
<div style="font-size:9px">
</div>
</div>
