<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<?= $this->element('header') ?>
	<body onload="otloaded()" style="padding-left:13%;padding-right:13%">
		<div id="page_margins">
			<div id="page" class="hold_floats">

				<?= $this->element('logo',array('title_for_layout' => $title_for_layout)) ?>
				<?= $this->element('navi') ?>
				<div id="main">
					<?=$content_for_layout?>			
				</div>
				<?= $this->element('footer') ?>
			</div>
		</div>
	</body>
</html>
