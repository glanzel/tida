<div class="proposals form">
<?php echo $this->Form->create('Proposal');?>
	<fieldset>
 		<legend><?php echo __('Edit Proposal'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('beschreibung');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
