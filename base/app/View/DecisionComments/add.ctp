<div class="proposals form">
<?php echo $this->Form->create('DecisionComment', array('action' => "/add"));?>
	<fieldset>
 		<legend><?php echo __('Kommentar hinzufügen'); ?></legend>
	<?php
		echo $this->Form->hidden('proposal_id');
		echo $this->Form->hidden('decision_id');
		echo $this->Form->input('comment');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
