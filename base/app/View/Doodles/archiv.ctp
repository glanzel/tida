<div class="decisions index">
<br>

	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd"><?php echo $this->Paginator->sort('bearbeitet');?></th>
			<th ><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('Kategorie');?></th>
			<th><?php echo $this->Paginator->sort('Sichtbarkeit');?></th>
			<th class="firsttd"><?php echo $this->Paginator->sort('Termin');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($doodles as $doodle):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td class="firsttd">
		<?= date('d.m.Y', strtotime($doodle['Doodle']['bearbeitet_am'])) ?>&nbsp;</td>
	
		<td>
		<?= $this->Html->link($doodle['Doodle']['title'], array('action' => 'view', $doodle['Doodle']['id'])); ?>&nbsp;</td>
		<td><?= $doodle['KptnTodocat']['title'] ?>&nbsp;</td>
		<td><?= $doodle['Admingroup']['name'] ?>&nbsp;</td>
		<td><?= date('d.m.Y', strtotime($doodle['Appointment']['von'])) ?>&nbsp;</td>
		
	</tr>
<?php endforeach; ?>
	</table>

	<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous'), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next').' >>', array(), null, array('class'=>'disabled'));?>
</div>
</div>
