<div class="kptnTodos defaultform">
<?php echo $this->Form->create('Doodle', array( 'url' => '/doodles/save'));?>
	<fieldset>
 		<legend><?php echo __('Doodle erstellen');?></legend>
	<?php
		echo $this->Form->label('Typ');
		echo $this->Form->select('kategorie', $kptnTodocats);
		echo $this->Form->input('title', array('label' => 'Title'));
		echo $this->Form->input('desc', array('label' => 'Beschreibung'));
		echo $this->Form->input('dauer', array('label' => 'Dauer in h')); 
		//echo $this->Form->select('typ', array('reparatur','ek','bau','orga','vorschlag','weiteres'));
		echo $this->Form->label('sichtbarkeit');
		echo $this->Form->select('sichtbarkeit', $admingroups);
		echo "<br>";
		echo $this->Form->label('editierbarkeit');
		echo $this->Form->select('editierbarkeit', $admingroups);

		echo "<br><br>";

		echo $this->Form->label('Anzahl vorschläge');
		echo $this->Form->select('anzahl', array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20),array('onChange' => 'showDoodle(this.value)'));
		echo "<br><br>";
		
		echo $datum;
		
		$divArr = array('div' => array('id' => "festesdatum"),"timeFormat" => '24');
		//if(isset($datum))$divArr['value'] = date('Y-m-d H:i', strtotime($datum)+43200);
		$divArr['value'] = date('Y-m-d 12:00');

		for($i=1;$i<20;$i++){
			echo "<div id='terminvor{$i}' class='datetime' style='display:none'>";
			echo $this->Form->label("vorschlag $i");
			echo $this->Form->dateTime("von{$i}", 'DMY', '24', $divArr);
			echo "</div>";
		}

	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
