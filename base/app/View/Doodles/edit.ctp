<div class="kptnTodos defaultform">
<?php echo $this->Form->create('Doodle', array( 'url' => '/doodles/save'));?>
	<fieldset>
 		<legend><?php echo __('Doodle bearbeiten');?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->label('Typ');
		echo $this->Form->select('kategorie', $kptnTodocats);
		echo $this->Form->input('title', array('label' => 'Title'));
		echo $this->Form->input('desc', array('label' => 'Beschreibung'));
		echo $this->Form->input('dauer', array('label' => 'Dauer in h')); 
		//echo $this->Form->select('typ', array('reparatur','ek','bau','orga','vorschlag','weiteres'));
		echo $this->Form->label('sichtbarkeit');
		echo $this->Form->select('sichtbarkeit', $admingroups);
		echo "<br>";
		echo $this->Form->label('editierbarkeit');
		echo $this->Form->select('editierbarkeit', $admingroups);
		
		echo "<br><br>";

	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
