<div class="entries index">
	<table cellpadding="0" cellspacing="0" class="crewlist">
	<tr>
			<th><?php echo $this->Paginator->sort('datum');?></th>
			<th><?php echo $this->Paginator->sort('user');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('speech');?></th>
			<th><?php echo $this->Paginator->sort('value');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	
	foreach ($Eversions as $Eversion){
	?>
	<tr>
		<td><?php echo date('d.m.y H:i', strtotime($Eversion['WikiVersion']['created'])); ?>&nbsp;</td>
		<td><?php echo $Eversion['User']['name']; ?>&nbsp;</td>
		<td><?php echo $Eversion['WikiVersion']['name']; ?>&nbsp;</td>
		<td><?php echo $Eversion['WikiVersion']['speech']; ?>&nbsp;</td>
		<td><?php echo $Eversion['WikiVersion']['value']; ?>&nbsp;</td>
		<td class="actions">
			<?php  /*echo $this->Html->link(__('View', true), "/$speech/eversion/view/".$Eversion['WikiVersion']['name']); */ ?>
			<?php 
			//echo $this->Html->link(__('Restore', true), "/$speech/entries/restore/".$Eversion['WikiVersion']['id']); 
			?>

			<?php 
			echo $this->Form->create('WikiVersion', array('url' => array('controller' => "WikiEntries", 'action' => 'restore')));
			echo $this->Form->hidden('id', array('value' => $Eversion['WikiVersion']['id'])); 
			echo $this->Form->end(array('name' => 'Restore', 'label' => 'Restore', 'div' => false)); 
			?>

		</td>
	</tr>
	<? } ?>
	</table>
	<p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' , array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next( ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
