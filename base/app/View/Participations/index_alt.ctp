<div class="participations index crewindex">
	<table class="crewtable" cellpadding="0" cellspacing="0">
	<tr class="firsttr">
			
		<th class=firsttd ><?php echo $this->Paginator->sort('user_id');?></th>
			
		<th  ><?php echo $this->Paginator->sort('year');?></th>
			
		<th  ><?php echo $this->Paginator->sort('mitmachen');?></th>
			
		<th  ><?php echo $this->Paginator->sort('verkehrsmittel');?></th>
			
		<th  ><?php echo $this->Paginator->sort('mitfahrgelegenheit');?></th>
			
		<th  ><?php echo $this->Paginator->sort('ort');?></th>
			
		<th  ><?php echo $this->Paginator->sort('anreise');?></th>
			
		<th  ><?php echo $this->Paginator->sort('abreise');?></th>
			
		<th  ><?php echo $this->Paginator->sort('zusage');?></th>
			
		<th  ><?php echo $this->Paginator->sort('desc');?></th>
			
		<th  ><?php echo $this->Paginator->sort('mitangemeldet');?></th>
			
		<th  ><?php echo $this->Paginator->sort('created');?></th>
			
		<th  ><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($participations as $participation): ?>
	<tr>
		<td  class=firsttd >
			<?php echo $this->Html->link($participation['User']['forename'], array('controller' => 'users', 'action' => 'view', $participation['User']['id'])); ?>
		</td>
		<td  ><?php echo h($participation['Participation']['year']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['mitmachen']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['verkehrsmittel']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['mitfahrgelegenheit']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['ort']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['anreise']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['abreise']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['zusage']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['desc']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['mitangemeldet']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['created']); ?>&nbsp;</td>
		<td  ><?php echo h($participation['Participation']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $participation['Participation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $participation['Participation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $participation['Participation']['id']), null, __('Are you sure you want to delete # %s?', $participation['Participation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>

	<div class="paging">
	<div class="symbols">

	<?php
		echo $this->Paginator->prev('<< ', array(), null, array('class' => 'prev disabled'));
		echo '|';
		echo $this->Paginator->numbers(array('separator' => ''));
		echo '|';
		echo $this->Paginator->next(' >>', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	<div class="howmuch">
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Zeige Datensätze %start% - %end% (%count% insgesamt)')
	));
	?>	</div>
	</div>
	
</div>

