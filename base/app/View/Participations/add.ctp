<div class="users form">
<?php echo $this->Form->create(null, array('url' => '/Participations/add'));?>
	<fieldset>
 		<legend><b>bitte lege deine teilnahme für <?= $year?> an</b></b></legend>
	<!--  
	
		<div id="yesorno">
			<?php
				$options = array('0' => ' Ich bin mir noch nicht sicher ', ZUSAGE => ' Ich möchte verbindlich zusagen ');
				echo $this->Form->radio("zusage" , $options, array('label' => false));
			?>
		</div>
	-->
<br>
<div style="width:450px; float:left">
	<?php

		echo $this->Form->hidden('year');
		echo $this->Form->hidden('id');
		echo $this->Form->hidden('user_id');
?>
<br>
<b> organistatorisches </b><br>
<?
		echo $this->Form->input('uebernachtung');
		echo $this->Form->input('mitangemeldet', array('style' => 'width:308px'));
		echo 'Was willst du beitragen ?'; 
		echo $this->Form->input('mitmachen', array('label' => false,'style' => 'width:308px', 'placeholder' => 'Ich könnte einen Swing Workshop anbieten, 2h am Einlass stehen und beim kochen helfen.'));
		echo $this->Form->label('Anmerkungen'); echo "<br>";
		echo $this->Form->input('desc', array('label' => false, 'style' => 'width:308px'));
	?>
<b> an und abfahrt </b><br>
		<?
		echo $this->Form->input('verkehrsmittel');
		echo $this->Form->input('mitfahrgelegenheit', array('label' => 'mitfahren', 'options' => array('', 'suche', 'biete')));
		echo $this->Form->label('anreise'); echo "<br>";
		echo $this->Form->input('anreise',array('timeFormat'=>'24', 'dateFormat' => 'DM', 'label' => false, 'selected' => Configure::read('klingtgut.start')));
		echo $this->Form->year('anreise', array('style' => 'display:none', 'default' => $year));
        echo $this->Form->label('abreise'); echo "<br>";
		echo $this->Form->input('abreise',array('timeFormat'=>'24', 'dateFormat' => 'DM', 'label' => false, 'selected' => Configure::read('klingtgut.ende')));
		echo $this->Form->year('abreise', array('style' => 'display:none', 'default' => $year));

        ?>
	
</div>
	<div style="float:right;  width:350px">
	<br><br>
1. Wenn du Personen mitbringst (z.B.: deine Familie) trag die Anzahl unter „Mitangemeldet“ ein. Personen die eigenständig anreisen, sollten sich separat anmelden.
<br><br>
2. Schreib uns unter Mitmachen auf was/ob du etwas anbieten / beim aufbauen, abbauen, kochen helfen / auftreten willst<br><br>
3. Bitte sage uns wenn möglich, wann du kommst und wann du wieder abreist, durch deine Auswahl unter „mitfahren“ können andere dich zwecks Mitfahrgelegenheit kontaktieren.
<br><br>
4. Wenn du uns unter „übernachten“ mitteilst, wie du übernachten willst (z.B. eigenes Zelt, Bus, Kasematten), erleichtert uns das die Unterkunftsplanung.
<br><br>
5. Wenn Du alles ausgefüllt hast, bitte über „submit“ das Ganze abschicken…

<br><br>

</div>	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
