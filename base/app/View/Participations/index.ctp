<?php ?>
<b><?= $year ?></b><br>
<div class="users index">
<?= $this->Form->create('Participations'); ?>

<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->Html->link('created', array('controller' => 'Participations', 'action'=>'index',  'Participation.created' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('name', array('controller' => 'Participations', 'action'=>'index',  'User.forename' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('kontakt', array('controller' => 'Participations', 'action'=>'index',  'kontakt' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('anfahrt', array('controller' => 'Participations', 'action'=>'index',  'anreise' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('abfahrt', array('controller' => 'Participations', 'action'=>'index',  'abreise' ),array('escape' => false),null,false); ?></th>
	<th>mitmachen</th>
	<th><?php echo $this->Html->link('zu', array('controller' => 'Participations', 'action'=>'index',  'zusage' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('bez', array('controller' => 'Participations', 'action'=>'index',  'bazhalt' ),array('escape' => false),null,false); ?></th>
	<th>desc</th>
	<th><?php echo $this->Html->link('+', array('controller' => 'Participations', 'action'=>'index',  'mitangemeldet' ),array('escape' => false),null,false); ?></th>
	<th></th>
	
</tr>
<?php

$alle = $zusagen = $mitangemeldet = $mitangemeldet_alle = $i = 0;
$n_1fruestueck = $n_2fruestueck = $n_1abendessen = $n_2abendessen = 0;
$n_haben_defaultdaten = 0;
$n_bezahlt = 0;
foreach ($participations as $key => $participant):

	$participant['Participation']['anreise'] =  date('Y', strtotime(Configure::read("klingtgut.start"))) . date('-m-d H:i', strtotime($participant['Participation']['anreise']));
	$participant['Participation']['abreise'] =  date('Y', strtotime(Configure::read("klingtgut.ende"))) . date('-m-d H:i', strtotime($participant['Participation']['abreise'])); //Hack, um aktuelles Jahr zu setzten, kann mit neuer leerer Datenbank wieder rausgenommen werden
	
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
	if($participant['Participation']['zusage'] < 2){
		$alle++;
		$mitangemeldet_alle = $mitangemeldet_alle + $participant['Participation']['mitangemeldet'];
	}

	if($participant['Participation']['zusage'] == 2){
		$zusagen++;
		$mitangemeldet = $mitangemeldet + $participant['Participation']['mitangemeldet'];
		
		if($participant['Participation']['anreise'] == Configure::read("klingtgut.start") && $participant['Participation']['abreise'] == Configure::read("klingtgut.ende")) {
		  $n_haben_defaultdaten++;
		  if(isset($participant['Participation']['mitangemeldet'])) $n_haben_defaultdaten = $n_haben_defaultdaten + $participant['Participation']['mitangemeldet'];
		}
		
		foreach ($essen as $name => $valueArray){
			if(strtotime($participant['Participation']['anreise']) < $valueArray['zeit'] && strtotime($participant['Participation']['abreise']) > $valueArray['zeit']) {
				@$essen[$name]['anzahl']++;
				if(isset($participant['Participation']['mitangemeldet'])){ 
					$essen[$name]['mitangemeldet_anzahl'] = @$essen[$name]['mitangemeldet_anzahl'] + $participant['Participation']['mitangemeldet'];
				}
			}
		}
		
		if($participant['Participation']['bezahlt'] > 0) {
		    $n_bezahlt ++;
            if(isset($participant['Participation']['mitangemeldet'])) {
              $n_bezahlt += $participant['Participation']['mitangemeldet'];
            }
          }
	}
	
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo date('d.m.y', strtotime( $participant['Participation']['created'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($participant['User']['forename'], array('action' => 'view', $participant['User']['id'])); ?>
			<?php echo $participant['User']['surname']; ?>
		</td>

<!--		<td>
			<?php echo $participant['User']['email']; ?>
		</td>
-->
		<td>
			<?php echo $participant['User']['kontakt']; ?>
		</td>
		<td>
			<?php if(($participant['Participation']['anreise']) && $participant['Participation']['anreise'] <> '0000-00-00 00:00:00') echo date('d.m. H:i', strtotime($participant['Participation']['anreise'])); ?>
		</td>
		<td>
			<?php if(($participant['Participation']['abreise'])) echo date('d.m. H:i', strtotime($participant['Participation']['abreise'])); ?>
		</td>

				<td>
			<?php echo $this->Html->link( substr($participant['Participation']['mitmachen'],0,30),array('action' => 'view'),
				array('data-tooltip' => $participant['Participation']['mitmachen']));	 ?>
		</td>
		<td>
			<?php 
			if($participant['Participation']['zusage'] == 1) echo $this->Html->image("hourglass.gif", array("border" => 0, 'title' => 'Warteliste')); 
			else if($participant['Participation']['zusage'] == ZUSAGE) echo $this->Html->image("checkmark_green.gif", array("border" => 0, 'title' => 'Zusage'));
			else if($participant['Participation']['zusage'] == ABSAGE) echo $this->Html->image("x.gif", array("border" => 0, 'title' => 'Zusage'));
			?>
		</td>
		<td> 
			<?php 
			echo $this->Form->hidden("id", array('value' => $participant['Participation']['id'], 'name' => "data[$key][id]"));
			echo $this->Form->text("bezahlt", array('checked' => $participant['Participation']['bezahlt'], 'name' => "data[$key][bezahlt]", 'label' => false, 'style' => 'width:25px', 'value' => $participant['Participation']['bezahlt'])); 
			?>
		</td>
		<td> 
			<?php echo $participant['Participation']['desc']; ?>
		</td>
		<td>
			<?php echo $participant['Participation']['mitangemeldet']; ?>
		</td>
		<td>
			<?php echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0, 'title' => 'User bearbeiten')), array('controller' => 'Users', 'action'=>'edit', $participant['Participation']['user_id']),array('escape' => false),null,false); ?>			
			<?php echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0, 'title' => 'Teilnahme bearbeiten')), array('controller' => 'Participations', 'action'=>'edit', $participant['Participation']['user_id']),array('escape' => false),null,false); ?>
			<?php echo $this->Form->postLink($this->Html->image("x.gif", array("border" => 0, 'title' => 'Teilnahme löschen')), array('controller' => 'Participations', 'action' => 'delete', $participant['Participation']['id']), array('escape' => false, 'inline' => false, 'confirm' => __('Willst du die Teilnahme von %s wirklich löschen?', $participant['User']['forename']))); ?>

		</td>
		

	</tr>
<?php endforeach; ?>
</table>
<?= $this->Form->end('gezahlt speichern'); ?>
<?= $this->fetch('postLink'); ?>

Ohne zusagen: <?= $alle ?> <br>
Ohne mitangemeldet: <?= $mitangemeldet_alle ?> <br>
<br>
Zusagen: <?= $zusagen ?><br>
Mitangemeldet: <?=$mitangemeldet ?><br>
davon
bezahlt: <?= $n_bezahlt ?><br>
<br>
Datum nicht geändert: <?= $n_haben_defaultdaten ?><br>
<br>
<?
 foreach($essen as $name => $valueArray){
	echo "$name : ";
	echo $valueArray['anzahl'];
	echo " + ";
	echo $valueArray['mitangemeldet_anzahl'];
	echo "<br>";
 }
 ?>

</div>

