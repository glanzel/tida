<div class="participations view">
<h3><?php  echo __('Teilnahme von '); echo $this->Html->link($participation['User']['forename'], array('controller' => 'users', 'action' => 'view', $participation['User']['id'])); ?></h3>

<?php $dabei = $participation['Participation'];  ?>

<h3>	<?= $zusagenArr[$participation['Participation']['zusage']]; ?> 
<? 
$year_id = Configure::read('klingtgut.year_id');
if($dabei['zusage'] == 2 ){
	if($dabei['bezahlt'] == 0){ 
		echo "<span style='color:red'> aber du hast noch nicht bezahlt</span>";
		echo "<span style='font-size:10px'> (nicht tagesgenau)</span>";
		echo "<br>";
		echo $this->Html->link('-> so geht das bezahlen', array('controller' => 'WikiEntries', 'action' => 'view', "bezahlen{$year_id}"));
		
	}
	if($dabei['bezahlt'] == 1){ 
		echo "<span style='color:green'>und erfolgreich Geld überwiesen.</span>";
		echo "<br>Du bist sicher dabei.";
	}
}
$mitfahrgelegenheit = array('', 'suche', 'biete');
?></h3>
	weitere Daten:

		<dt><?php echo __('Jahr'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mitmachen'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['mitmachen']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Verkehrsmittel'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['verkehrsmittel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mitfahrgelegenheit'); ?></dt>
		<dd>
			<?php echo $mitfahrgelegenheit[$participation['Participation']['mitfahrgelegenheit']]; ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ort'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['ort']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Anreise'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['anreise']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Abreise'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['abreise']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Anmerkung'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['desc']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mitangemeldet'); ?></dt>
		<dd>
			<?php echo h($participation['Participation']['mitangemeldet']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	
	<b><?php echo $this->Html->link(__('Teilnahme bearbeiten / absagen'), array('action' => 'edit', $participation['Participation']['user_id'])); ?></b>
	
</div>
