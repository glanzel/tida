<div class="users form">
<?php echo $this->Form->create(null, array('url' => '/Participations/edit/'));?>
	<fieldset>
 		<legend>ich habe interesse...</legend>
	
<div id="yesorno">
	<?php
		$options = array( WARTELISTE => ' Warteliste ', ZUSAGE => ' Zusage ', ABSAGE => ' Ich kann leider doch nicht');
		echo $this->Form->radio("zusage" , $options, array('label' => false));
	?>
</div><br>
<div style="width:450px; float:left">
	<?php

		echo $this->Form->hidden('id');
		echo $this->Form->hidden('user_id');
?>
<b> organistatorisches </b><br>
<?
		echo $this->Form->input('uebernachtung');
		echo $this->Form->input('mitangemeldet', array('style' => 'width:308px'));
		echo $this->Form->label('Mitmachen'); echo "<br>";
		echo $this->Form->input('mitmachen', array('label' => false,'style' => 'width:308px'));
		echo $this->Form->label('Anmerkungen'); echo "<br>";
		echo $this->Form->input('desc', array('label' => false, 'style' => 'width:308px'));
		echo "<br>";echo "<br>";
		echo $this->Form->label('bezahlt:'); 
		echo $this->request->data['Participation']['bezahlt'] ? "ja" : "nein" ; echo "<br>";
		
	?>
</div>
	<div style="float:right;  width:350px">
<b> an und abfahrt </b><br>
		<?
		echo $this->Form->input('verkehrsmittel');
		echo $this->Form->input('mitfahrgelegenheit', array('label' => 'mitfahren', 'options' => array('', 'suche', 'biete')));
		echo $this->Form->input('ort');
		echo $this->Form->label('anreise'); echo "<br>";
		echo $this->Form->input('anreise',array('timeFormat'=>'24', 'dateFormat' => 'DM', 'label' => false));
		echo $this->Form->year('anreise', array('style' => 'display:none', 'default' => $year));
		echo $this->Form->label('abreise'); echo "<br>";
		echo $this->Form->input('abreise',array('timeFormat'=>'24', 'dateFormat' => 'DM', 'label' => false));
		echo $this->Form->year('abreise', array('style' => 'display:none', 'default' => $year));
?>
	<br><br>
		1. Wenn du Personen mitbringen willst (z.B. deine Familie), trag die Anzahl unter mitangemeldet ein.
		Personen die eigenständig anreisen sollten sich seperat anmelden.<br><br>
		2. Schreib uns unter Mitmachen auf was/ob du etwas anbieten / beim aufbauen, abbauen, kochen helfen / auftreten willst.<br><br>
        3. Bitte sag wenn möglich, wann du kommst und wann du wieder fährst. Durch Auswahl von <i>mitfahren</i> können andere dich zwecks Mitfahrgelegenheit kontaktieren.<br><br>
		4. Wenn du uns schreibst, wie du übernachten willst (logistik), erleichtert uns das die Unterkunfstplanung.
	</div>
</fieldset>
<?php echo $this->Form->end('Speichern');?>
</div>
