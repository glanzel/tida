<? if($wysiwyg){ ?> 
<script type="text/javascript" src='//cdn.tinymce.com/4/tinymce.min.js'></script>
  <script type="text/javascript">
  tinymce.init({
    selector: 'textarea',
    height: 500,
  });
  </script>
 <? } else { ?>
	<!--
	<script type="text/javascript" src='//cdnjs.cloudflare.com/ajax/libs/codemirror/5.15.2/codemirror.min.js'></script>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.15.2/codemirror.min.css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.15.2/mode/htmlmixed/htmlmixed.min.js"></script>
	-->
<? } ?>
<div class="entries form">
<?php echo $this->Form->create('WikiEntry');?>
	<fieldset class=first>
 		<legend><span style="font-weight:500;">editiere Eintrag : <b style="font-size:1.1em;"><?= $this->data['WikiEntry']['name'] ?></b> </span></legend>
	
		<div class="input">
		<div style="float:right">
			<? if($wysiwyg){ 
				echo $this->Html->link('-> source', array('action' => 'edit', $id, 0));
			}else{
				echo $this->Html->link('-> wysiwiyg', array('action' => 'edit', $id, 1));
			}?>
		</div>
		<label><?= __('last edited by')?></label>  
		<div><?= $this->data['User']['name']; ?></div>
		</div>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->hidden('name');
		//echo $this->Form->input('speech');
	?>
	<div class="theditbox" >
	<?
		echo $this->Form->input('value', array('label' => false, 'id' => 'theditbox', 'style' => 'width:99%;height:500px'));
	?>

	</div>
	</fieldset>
	<div id="testbox">testbox</div>
  <script type="text/javascript">
	  var myCodeMirror = CodeMirror.fromTextArea(theditbox);
  </script>

	<fieldset>
	<legend><span><?=$this->Html->link('älteren Versionen anzeigen', "/WikiVersions/index/".$this->data['WikiEntry']['name']) ?></span></legend>

	<div class="end">
		<div class="submit">
			<span class="start">&nbsp;</span>
			<span>
				<?=$this->Form->submit(__d('entries', 'save', true), array('div' => false, 'class' => 'icon save'))?>
			</span>
			<span class="end">&nbsp;</span>
		</div>
	</div>
	</fieldset>
</div>
