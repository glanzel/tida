<div class="programm">


<table class="n_programm">
<colgroup> <col width="100" /> <col width="95" /> <col width="*" /> </colgroup>
<tbody>
<?php
	foreach ($appointments as $appointment):
	?>
	<tr>
	<td><b>
	<?= $wochentag[date('w', strtotime($appointment['Appointment']['von']))] ?>
	</b><br/>
	<?= date('d.m.y', strtotime($appointment['Appointment']['von'])) ?><br>
	</td>
	<td>
	<?= date('H:i', strtotime($appointment['Appointment']['von'])) ?> Uhr<br>
	</td>
	<td>
	<b><?= $appointment['Appointment']['title'] ?></b>
	<? if(! empty($appointment['Appointment']['beschreibung'])) echo  $appointment['Appointment']['beschreibung']; ?>
	</td>
	</tr>
	
<?php endforeach; ?>
</tbody>
</table>
</div>
