<div class="decisions index">
	[<?php echo $this->Html->link(__('Neuen Doodle'), array('controller' => 'Appointment', 'action'=>'addDoodle')); ?>]
	<br><br>
	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd"><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('Kategorie');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($appointments as $appointment):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td class="firsttd">
		<?= $this->Html->link($appointment['Appointment']['title'], array('action' => 'viewDoodle', $appointment['Appointment']['id'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($appointment['Appointment']['kategorie'], array('action' => 'view')); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>

	<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous'), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next').' >>', array(), null, array('class'=>'disabled'));?>
</div>
</div>
