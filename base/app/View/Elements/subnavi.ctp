<div id="subnav">
	<? if(isset($currentSub)){ ?>
	<ul id="subcat">
		<li>
			<?= $currentSub ?>
		</li>
	</ul>
	<? } ?>
	<ul id="actions">
	<? foreach($naviactions as $action){ 
		if(isset($action['pre'])) echo ('<li>'.$action['pre'].'</li>');
		else{
	?>
		<li class="main_navi" >
			<? $title = $action['title']; unset($action['title']); ?>
			<?
				if(isset($action['url'])) echo $this->Html->link($title, $action['url']);
				else echo $this->Html->link($title, $action);
			?>
		</li>
	<? }} ?>
	</ul>
</div>
