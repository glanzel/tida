<div class="votes index">
	<h2><?php echo __('Votes');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('decisions_id');?></th>
			<th><?php echo $this->Paginator->sort('user_id');?></th>
			<th><?php echo $this->Paginator->sort('proposal_id');?></th>
			<th><?php echo $this->Paginator->sort('votecat_id');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($votes as $vote):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $vote['Vote']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($vote['Decisions']['name'], array('controller' => 'decisions', 'action' => 'view', $vote['Decisions']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vote['User']['name'], array('controller' => 'users', 'action' => 'view', $vote['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vote['Proposal']['title'], array('controller' => 'proposals', 'action' => 'view', $vote['Proposal']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vote['Votecat']['name'], array('controller' => 'votecats', 'action' => 'view', $vote['Votecat']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $vote['Vote']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vote['Vote']['id'])); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $vote['Vote']['id']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?'), $vote['Vote']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%')
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous'), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next') . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Vote'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Decisions'), array('controller' => 'decisions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Decisions'), array('controller' => 'decisions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proposals'), array('controller' => 'proposals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proposal'), array('controller' => 'proposals', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Votecats'), array('controller' => 'votecats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Votecat'), array('controller' => 'votecats', 'action' => 'add')); ?> </li>
	</ul>
</div>