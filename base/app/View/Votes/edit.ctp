<div class="votes form">
<?php echo $this->Form->create('Vote');?>
	<fieldset>
 		<legend><?php echo __('Edit Vote'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('decisions_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('proposal_id');
		echo $this->Form->input('votecat_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $this->Form->value('Vote.id')), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?'), $this->Form->value('Vote.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Votes'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Decisions'), array('controller' => 'decisions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Decisions'), array('controller' => 'decisions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proposals'), array('controller' => 'proposals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proposal'), array('controller' => 'proposals', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Votecats'), array('controller' => 'votecats', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Votecat'), array('controller' => 'votecats', 'action' => 'add')); ?> </li>
	</ul>
</div>