<div>

Fort Gorgast<br>
15328 Küstriner Vorland

<p>
<?= $this->Html->link('Mitfahrgelegenheit', '/Participations/mitfahrt'); ?>

<div>
<b>Mit dem Rad</b><br>
Der Europaradweg R1 führt nur wenige Kilometer entfernt von Gorgast vorbei:<br>
<a href="http://www.r1-radweginfo.de/r1-radweg-deutschland-brandenburg-berlin.php">www.r1-radweginfo.de/r1-radweg-deutschland-brandenburg-berlin.php</a>
</div><br>
<div>
<b>Mit der Bahn</b><br>
Der Rb26 fährt alle 2h von S-Bhf Lichtenberg nach Gorgast. Der letzte fährt 19:37.
Der Bahnhof Gorgast liegt direkt an der Banhtrasse Ecke Bahnhofstraße
</div><br>
<div>

<b>Mit dem Auto</b><br>
Frankfurter Allee immer geradeaus und kurz vor Polen links rein.
</div><br>

<div style="width:900px;height:600px" id="basicMap"></div>
    <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
    <script>
      function init() {
        map = new OpenLayers.Map("basicMap");
        var mapnik = new OpenLayers.Layer.OSM();
        map.addLayer(mapnik);
	var lonLat = new OpenLayers.LonLat(14.545,52.56).transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            new OpenLayers.Projection("EPSG:900913") // to Spherical Mercator Projection
          );
   	var markers = new OpenLayers.Layer.Markers( "Markers" );
    	map.addLayer(markers);
        markers.addMarker(new OpenLayers.Marker(lonLat));

        map.setCenter(lonLat , 15 ); // set Zoom level
      }
      init();
    </script>


</div>
