<?php ?>
<div class="users index">
<p>

</p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->Html->link('created', array('controller' => 'Users', 'action'=>'index', 'created' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('name', array('controller' => 'Users', 'action'=>'index', 'name' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('kontakt', array('controller' => 'Users', 'action'=>'index', 'kontakt' ),array('escape' => false),null,false); ?></th>
	<th><?php echo $this->Html->link('email', array('controller' => 'Users', 'action'=>'index', 'email' ),array('escape' => false),null,false); ?></th>
	<th></th>
	
</tr>
<?php

foreach ($users as $user):

	
	$class = null;
	
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo date('d.m.y', strtotime( $user['User']['created'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($user['User']['forename'], array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $user['User']['surname']; ?>
		</td>

		<td>
			<?php echo $user['User']['kontakt']; ?>
		</td>
		<td>
			<?php echo $user['User']['email']; ?>
		</td>

		<td>
			<?php echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0, 'title' => 'bearbeiten')), array('controller' => 'Users', 'action'=>'edit', $user['User']['id']),array('escape' => false),null,false); ?>
		</td>
		

	</tr>
<?php endforeach; ?>
</table>
</div>

