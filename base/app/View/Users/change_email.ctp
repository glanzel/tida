<div class="users form">
<h3> Account aktivieren und/oder Email Adresse ändern</h3>

Falls du deine Emailadresse ändern willst / musst oder die Bestätigungsemail
zum aktivieren deines Kontos nochmals benötigst gebe hier die entsprechnde Emaildresse ein
und klicke auf Submit.<br><br>

<?php echo $this->Form->create('User', array('url' => "changeEmail"));?>
	<fieldset>
 	<?php
 		echo "<br>";
		echo $this->Form->input('id');
		echo $this->Form->input('email');
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>

<? if(isset($result)) echo $result;?>


</div>
