
<div style="text-align:left; font-size:1.2em;">
Was geht im Goldammer ?

<table class="n_programm" style="border-spacing: 0;
	width:100%;
	color: #404040;
	font-family: sans-serif,Arial;
    line-height: 1.8;">
<tbody>
<?php
	foreach ($appointments as $appointment):
	?>
	<tr style="	vertical-align:baseline;border-top: 1px solid #E6E6E6; padding: 1rem 1rem 0.8rem 0;">
	<td><b>
	<?= $wochentag[date('w', strtotime($appointment['Appointment']['von']))] ?>
	</b><br/>
	<?= date('d.m', strtotime($appointment['Appointment']['von'])) ?><br>
	</td>
	<td>
	<?= date('H:i', strtotime($appointment['Appointment']['von'])) ?> Uhr<br>
	</td>
	<td>
	<b><?= $appointment['Appointment']['title'] ?></b>
	<? if(! empty($appointment['Appointment']['beschreibung'])) echo  $appointment['Appointment']['beschreibung']; ?>
	</td>
	</tr>
	
<?php endforeach; ?>
</tbody>
</table>
</div>
