
<div class="users form">
<?php echo $this->Form->create(null, array('url' => '/Users/add'));?>
	<fieldset>
 		<legend>ich habe interesse...</legend>
	
<div id="yesorno">
	<?php

		$options = array('0' => ' Ich bin mir noch nicht sicher ','1' => ' Ich möchte verbindlich zusagen ');
		echo $this->Form->radio("zusage" , $options, array('label' => false));
	?>
</div><br>
<div style="width:450px; float:left">
<u><b> persönliche daten </b></u><br>
	<?php

		echo $this->Form->hidden('id');
		echo $this->Form->input('email');
		echo $this->Form->input('password'); echo ("(optional)");
		echo $this->Form->input('forename', array('label' => 'Vorname'));
		echo $this->Form->input('surname', array('label' => 'Nachname'));
?>
<br>
<u><b> organistatorisches </b></u><br>
<?
		echo $this->Form->input('uebernachtung');
		echo $this->Form->input('mitangemeldet', array('style' => 'width:308px'));
		echo $this->Form->input('kontakt',array('label' => 'Wen kennst du ?'));
		echo 'Was willst du beitragen ?'; 
		echo $this->Form->input('mitmachen', array('label' => false,'style' => 'width:308px', 'placeholder' => 'Ich könnte einen Swing Workshop anbieten und beim kochen helfen.'));
		echo $this->Form->label('Anmerkungen'); echo "<br>";
		echo $this->Form->input('desc', array('label' => false, 'style' => 'width:308px'));
	?>
</div>
	<div style="float:right;  width:350px">
<u><b> an und abfahrt </b></u><br>
		<?
		echo $this->Form->input('verkehrsmittel');
		echo $this->Form->input('mitfahrgelegenheit', array('label' => 'mitfahren', 'options' => array('', 'suche', 'biete')));
		echo $this->Form->label('anreise'); echo "<br>";
		echo $this->Form->input('anreise',array('timeFormat'=>'24', 'dateFormat' => 'DM', 'label' => false, 'selected' => '2016-07-08 17:00:00'));
		echo $this->Form->label('abreise'); echo "<br>";
		echo $this->Form->input('abreise',array('timeFormat'=>'24', 'dateFormat' => 'DM', 'label' => false, 'selected' => '2016-07-10 16:00:00'));
?>
	<br><br>
1. Wenn du Personen mitbringst (z.B.: deine Familie) trag die Anzahl unter „Mitangemeldet“ ein. Personen die eigenständig anreisen, sollten sich separat anmelden.
<br><br>
2. Schreibe uns unter 'Wen kennst du?' auf welchen anderen Teilnehmenden (oder Organisatoren) du kennst oder woher du von der Veranstaltung erfahren hast (...damit wir wissen das du echt bist).
<br><br>
3. Schreib uns unter „Was willst du beitragen ?“ auf, was/ob du etwas anbieten willst; beim aufbauen, abbauen, kochen helfen oder auftreten willst.
<br><br>
4. Bitte sage uns wenn möglich, wann du kommst und wann du wieder abreist, durch deine Auswahl unter „mitfahren“ können andere dich zwecks Mitfahrgelegenheit kontaktieren.
<br><br>
5. Wenn du uns unter „übernachten“ mitteilst wie du übernachten willst (z.B. eigenes Zelt, Bus, Kasematten), erleichtert uns das die Unterkunftsplanung.
<br><br>
6. Wenn Du alles ausgefüllt hast, bitte über „submit“ das Ganze abschicken…..
<br><br>

</div>	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
