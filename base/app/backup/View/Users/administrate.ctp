<?php ?>
<div class="users index">
<p>
</p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $this->Paginator->sort('name');?></th>
	<? if($cox) {
		foreach($groups as $group){
			echo "<th class='administrate_groups'>";
			echo $group['Admingroup']['name'];
			echo "</th>";
		}
		?>
		
	<? } ?>	
</tr>
<?php echo $this->Form->create('User', array('action' => 'administrate'));?>

<?php

$i = 0;
foreach ($users as $user):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $user['User']['name']; ?>
		</td>
	<? if($cox) {
		foreach($groups as $group){
			$ag_id = $group['Admingroup']['id'];
			echo "<td >";
			$options =  array();
			foreach($user['Admingroup'] as $usergroup) if($group['Admingroup']['id'] == $usergroup['id']){ 
					$options["checked"] = "checked";
					break;
			}
			if(($ag_id == 7 || $ag_id == 8 || $ag_id == 9 || $ag_id == 10) && !$superuser) $options["disabled"] = "disabled";
			
			echo $this->Form->checkbox("ids.".$user['User']['id'].".".$group['Admingroup']['id'], $options); 
			echo "</td>";
		}
		?>
		
	<? } ?>	

	</tr>
<?php endforeach; ?>
</table>
<?php echo $this->Form->end('Speichere Änderungen');?>
</div>
<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous'), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next').' >>', array(), null, array('class'=>'disabled'));?>
</div>

