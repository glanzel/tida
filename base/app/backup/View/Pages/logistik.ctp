<div style="width:70%">
<b>Übernachten:</b><br>

- Ihr könnt auf dem Gelände zelten (im Wald oder auf der Wiese) oder auch in einer der Hallen (Kasematten) übernachten (Isomatte und Schlafsack mitbringen)<br>

- Wer mit seinem Bus auf dem Gelände parken will, sollte früh anreisen. Nur eine kleine Anzahl passt auf das Gelände. Ansonsten parkt alles mit 4 Rädern vor dem Gelände.<br>
<br><br>
  

<b>Essen & Trinken:</b>

- Wir organisieren Frühstück (Samstag und Sonntag) und ein warmes Essen (vegetarisch) am Freitag- und Samstagabend. Alles Weitere für Zwischendurch bitte selbst mitbringen.
<br>
- Für Getränke (Bier, Saft, Wasser) wird gesorgt. Kühle Longdrinks werden zu günstigen Preisen verkauft.
<br>
- Stockbrot über der Feuertonne ist am Start.
<br>
- Bringt euer Gedeck mit!!!
<br><br>
<b>Kosten:</b>

- Das Fest soll kostenmäßig auf Null rauskommen. Unwahrscheinliche Überschüsse werden gespendet.
<br>
- Wir rechnen mit ca. 35-45€ pro Person für das ganze Wochenende. Inklusive wären darin Essen, Getränke (Bier, Saft, Wasser), Übernachtung & Unkostenbeitrag für das super Programm. Das Geld sammeln wir von Euch vor Ort ein bzw. ihr überweist es im Voraus (Details werden Euch noch rechtzeitig bekannt gegeben). Wer nichts trinkt oder nur einen Tag kommt, kann auch weniger bezahlen.
<br>
<br> 

