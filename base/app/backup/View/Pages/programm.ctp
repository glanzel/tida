<div>
 
<table id="b52" border="0"> <colgroup> <col width="300" /> <col width="550" /> </colgroup>
<tbody>

<tr>
<td class="firsttd" ><br><b>Bands</b><br><br></td>
<td></td>
</tr>

<tr>
<td class="firsttd"><?php echo $this->Html->image("02_dieaufloesung.jpg", array("border" => 0, 'title' => 'bild')); ?></td>
<td><b>Die Aufl&ouml;sung (live)</b> // [Kraut/Postrock]<br><br>Die Auflösung zeigt Euch drei Taugenichtse, hinter deren Musik keine klaren Zwecke stecken außer jenem, sich Vergnügen und Kurzweil zu verschaffen.
Elektrisch, improvisiert, oft laut. Noise-Rock, Kraut-Funk oder Drone?
Weder Anti noch Easy. Ohne Konzept, ohne Scham.
Ming, Norm N und Jo führen die Dissonanz zur Konsonanz und die Konsonanz zur Auflösung.
Es lebe der freie Mensch, der Hedonismus und die Dekomposition!<br>mehr Infos -> <a href="https://dieaufloesung.wordpress.com/">Webseite</a></td>
</tr>

<tr>
<td class="firsttd"><?php echo $this->Html->image("01_ruedi.jpg", array("border" => 0, 'title' => 'bild')); ?></td>
<td><b>die R&uuml;di Show (live)</b> // [Chansons der 20'er Jahre]<br><br>Clara Maess, Matthias Mengert, Charlotte Krafft und Lisa Andersohn singen Werke von Schubert bis Spoliansky, von Kreisler bis Chopin, am Klavier: Nils Corte.<br>mehr Infos -> <a href="https://www.youtube.com/watch?v=KjfNM2pDBBE">Youtube</a>, <a href="https://www.youtube.com/watch?v=O4bDJsqekwc">Youtube</a></td>
</tr>

<tr>
<td class="firsttd"><?php echo $this->Html->image("06_hans.jpg", array("border" => 0, 'title' => 'bild')); ?></td>
<td><b>Hans S&oslash;lo (live)</b> // [Neo Classic/Psychodelic Folk]<br><br>Der Singer Songwriter aus Berlin bringt es mit seinen feinsinnigen Texten auf den Punkt. <br>mehr Infos -> <a href="http://soundcloud.com/hans-solo ">Soundcloud</a></td>
</tr>

<tr>
<td class="firsttd"><br><b>DJ's</b><br><br></td>
<td></td>
</tr>

<tr>
<td class="firsttd">
<!-- <?php echo $this->Html->image("05_hughda.jpg", array("border" => 0, 'title' => 'bild')); ?>
-->
</td>
<td><b>Eros Mozzarella </b> // [Alternative Dorf Disco Delux]<br><br>
</td>
</tr>

<tr>
<td class="firsttd">
<!-- <?php echo $this->Html->image("05_hughda.jpg", array("border" => 0, 'title' => 'bild')); ?>
-->
</td>
<td><b>Zeller & Glanzen </b>// Trude, Ruth & Goldammer // [Deep House]<br><br>
</td>
</tr>

<tr>
<td class="firsttd">
<!-- <?php echo $this->Html->image("05_hughda.jpg", array("border" => 0, 'title' => 'bild')); ?>
-->
</td>
<td><b>Audiotourist </b>// [Eklektit]<br><br>
</td>
</tr>

<tr>
<td class="firsttd">
<!-- <?php echo $this->Html->image("05_hughda.jpg", array("border" => 0, 'title' => 'bild')); ?>
-->
</td>
<td><b>E&H </b> //  [disco & riot & funk] <br><br>
</td>
</tr>

<tr>
<td class="firsttd">
<!-- <?php echo $this->Html->image("05_hughda.jpg", array("border" => 0, 'title' => 'bild')); ?>
-->
</td>
<td><b>Jost </b> //  [swing] <br><br>
</td>
</tr>

<tr>
<td class="firsttd">
<?php echo $this->Html->image("08_jazz.jpg", array("border" => 0, 'title' => 'bild')); ?>
</td>
<td><b>Cup of Jazz </b> // Salon Obskur // [Jazzylectro]<br><br>

</td>
</tr>

<tr>
<td class="firsttd">
<?php echo $this->Html->image("07_xilian.jpg", array("border" => 0, 'title' => 'bild')); ?>
</td>
<td><b>Xilian </b> // st&ouml;rsignal // [house/techno]<br><br>
....puristic, rhythmic, danceable. sometimes old
school, sometimes dark and heavy.
</td>
</tr>

<tr>
<td class="firsttd"><?php echo $this->Html->image("03_hagen.png", array("border" => 0, 'title' => 'bild')); ?></td>
<td><b>H&Auml;GEN DAZ </b>// sisyphos // [HipHop/DnB/Dubstep]<br><br>
Besser als Saetchmo von Echochamber können wir es auch nicht beschreiben...<br>
slow burn boom with sugar on the top.
the main source for that thrust was my special guest.
the amazing hägen daz just killed it right away.
with indica wobbles and space flight bass.
one of the best sets played in my show this year,i tell you.<br>mehr Infos -> <a href="https://soundcloud.com/hagen-daz">Soundcloud</a></td>
</tr>



<tr>
<td class="firsttd"><?php echo $this->Html->image("05_hughda.jpg", array("border" => 0, 'title' => 'bild')); ?></td>
<td><b>hughda.jones </b>// Trude, Ruth & Goldammer // [Jungle/DnB]<br><br>
Aufgewachsen in den Strassen South Central Weddings, zog es den gebürtigen Berliner DJ schon Anfang der 90er Jahre in die Ost Berliner Clubs, wo nächtens die ehrlichen Breakbeats aus London herüberwummerten. Während alle Welt Jura und BWL studieren wollte, trieb er sich in Plattenläden rum, immer auf der Suche nach den heißesten Tunes. Jungle & D’n'B sind seit dem sein Ding und von Nancys Boots bis Stings Sunglasses, dieser Typ zieht sich alles an, hauptsache es läßt sich mit 180 bpm auf den Plattentellern drehen.
</td>
</tr>

</tbody>
</table>




</div>
