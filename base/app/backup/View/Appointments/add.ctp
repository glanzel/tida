<div class="kptnTodos defaultform">
<?php echo $this->Form->create('Appointment', array('action' => 'save'));?>
	<fieldset>
 		<legend><?php echo __('Termin erstellen');?></legend>
	<?php
		$divArr = array('div' => array('id' => "festesdatum"),"timeFormat" => '24','selected' => (strtotime($datum)+43200));
		echo $this->Form->input('von', $divArr);
		echo $this->Form->input('dauer', array('label' => 'Dauer in h')); 
		echo $this->Form->label('1x pro Woche'); 
		echo $this->Form->checkbox('wiederholen');
		echo "<br>";
		echo $this->Form->label('Typ');
		echo $this->Form->select('kategorie', $kptnTodocats);
		//echo $this->Form->select('typ', array('reparatur','ek','bau','orga','vorschlag','weiteres'));
		echo $this->Form->input('title', array('label' => 'Title'));
		echo $this->Form->input('beschreibung');
		echo $this->Form->label('sichtbarkeit');
		echo $this->Form->select('sichtbarkeit', $admingroups);
		echo "<br>";
		echo $this->Form->label('selbst teilnehmen');
		echo $this->Form->checkbox('teilnehmen', array('checked' => true));
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
