<? setlocale(LC_ALL, 'de_DE.utf8'); ?>

	<?
		foreach($typen as $id => $typ){
			echo $this->Html->link($typ, array('action' => 'indexall', "typ" => $typ));
			echo " | ";
		}
		echo $this->Html->link('alle', array('action' => 'indexall'));
		
	?><br><br>

<div class="decisions index">
	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd"><?php echo $this->Paginator->sort('StartDatum');?></th>
			<th><?php echo $this->Paginator->sort('Typ');?></th>
			<th ><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('Kategorie');?></th>
			<th><?php echo $this->Paginator->sort('Sichtbarkeit');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($appointments as $appointment):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td class="firsttd">
		<?= strftime("%A, %d.%m.%y", strtotime($appointment['Appointment']['von'])) ?>&nbsp;</td>
		<td><?= $appointment['Appointment']['wiederholen'] ?>&nbsp;</td>
		<td><?= $this->Html->link($appointment['Appointment']['title'], array('action' => 'view', $appointment['Appointment']['id'])); ?>&nbsp;</td>
		<td><?= $appointment['KptnTodocat']['title'] ?>&nbsp;</td>
		<td><?= $appointment['Admingroup']['name'] ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>

	<div class="paging">
	<?php echo $this->Paginator->prev('<< '.__('previous'), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $this->Paginator->numbers();?>
	<?php echo $this->Paginator->next(__('next').' >>', array(), null, array('class'=>'disabled'));?>
</div>
</div>
