<? setlocale(LC_ALL, 'de_DE.utf8'); ?>
<div class="decisions index">
	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd">StartDatum</th>
			<th>Typ</th>
			<th >title</th>
			<th>Kategorie</th>
			<th>Sichtbarkeit</th>
	</tr>
	<?php
	$i = 0;
	foreach ($appointments as $appointment):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td class="firsttd">
		<?= strftime("%A, %d.%m.%y", strtotime($appointment['Appointment']['von'])) ?>&nbsp;</td>
		<td><?= $appointment['Appointment']['wiederholen'] ?>&nbsp;</td>
		<td><?= $this->Html->link($appointment['Appointment']['title'], array('action' => 'view', $appointment['Appointment']['id'])); ?>&nbsp;</td>
		<td><?= $appointment['KptnTodocat']['title'] ?>&nbsp;</td>
		<td><?= $appointment['Admingroup']['name'] ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>

</div>
