<div class="kptnTodos defaultform">
<?php echo $this->Form->create('Appointment', array('action' => 'save'));?>
	<fieldset>
 		<legend><?php echo __('Termin bearbeiten');?></legend>
	<?php
		echo $this->Form->hidden('id');
		$divArr = array('div' => array('id' => "festesdatum"),"timeFormat" => '24');
		echo $this->Form->input('von', $divArr);
		echo $this->Form->input('dauer', array('label' => 'Dauer in h')); 
		echo $this->Form->label('1x pro Woche'); 
		echo $this->Form->checkbox('wiederholen');
		echo "<br>";
		echo $this->Form->label('Typ');
		echo $this->Form->select('kategorie', $kptnTodocats);
		echo $this->Form->input('title', array("rows" => 2));
		echo $this->Form->input('beschreibung');
		echo ('<br>');
		echo $this->Form->label('gruppe');
		echo $this->Form->select('sichtbarkeit', $admingroups);
	?>
	</fieldset>
<?php echo $this->Form->end('Submit');?>
</div>
