<div class="kptnTodos view">
		<span>[<?php echo $this->Html->link(__('In Schichtplan anzeigen'), array('controller' => 'KptnDeshifts', 'action'=>'indexWeekOfYear', date('W', strtotime($kptnTodo['Appointment']['von'])),date('Y', strtotime($kptnTodo['Appointment']['von'])))); ?>] </span>
		<span>[<?php echo $this->Html->link(__('Edit Appointment'), array('action'=>'edit', $kptnTodo['Appointment']['id'])); ?>] </span>
		<span>[<?php echo $this->Html->link(__('Delete Appointment'), array('action'=>'delete', $kptnTodo['Appointment']['id']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?'), $kptnTodo['Appointment']['id'])); ?>] </span>
		<br><br>

<div style="float:right">
		<?= $this->Form->create('Appointment', array('action' => 'addUser', 'url' => array( $kptnTodo['Appointment']['id'], $user_id))); ?>
		<?= $this->Form->end(' Am Termin Teilnehmen '); ?>

		<?= $this->Form->create('Appointment', array('action' => 'removeUser', 'url' => array( $kptnTodo['Appointment']['id'], $user_id))); ?>
		<?= $this->Form->end('Diesen Termin absagen'); ?>
		<br>
		<?= $this->Form->create('Appointment', array('action' => 'addUserByName', 'url' => array( $kptnTodo['Appointment']['id']))); ?>
		<?= $this->Form->input('name', array('div' => false, 'label' => false)); ?>
		<?= $this->Form->submit('für den Termin eintragen '); ?>
		<?= $this->Form->end(); ?>
</div>

<div style="border-right: solid 2px black; width:500px">
		<div>
		<?php echo date('d.m.Y H:i', strtotime( $kptnTodo['Appointment']['von'] )); ?>
		(<?php echo $kptnTodo['Appointment']['dauer']." h"; ?>)
		</div>
		<div>
		<b><?php echo $kptnTodo['Appointment']['title']; ?></b>
		</div>
		<div>
			<?php echo $kptnTodo['Appointment']['beschreibung']; ?>
		</div>
		<br>
		<div style="float:left; width:130px;"><i><?php echo __('Typ'); ?></i></div>
		<div><?php echo $kptnTodo['KptnTodocat']['title']; ?>
		</div>
		<div style="float:left; width:130px;"><i><?php echo __('teilnehmend'); ?></i></div>
			<? foreach($kptnTodo['User'] as $key => $user){ ?>
			<?php if($key!=0) echo(', ');echo $user['name']; ?>
			<? } ?>
			&nbsp;
		</div><br>
		<div style="font-size:80%"><?php echo __('Geändert Am'); ?></div>
		<div>	<?php echo date('d.m.Y', strtotime( $kptnTodo['Appointment']['geandert_am'] )); ?>
			&nbsp;
			<?php echo __('von'); ?>
			<?php echo $kptnTodo['Appointment']['geandert_von']; ?>
			&nbsp;
		</div>
		<br>
	
</div>

<?php if (!empty($decisions)) {?>
<div >
	<dl>
	<div style="background-color:#eee; width: 149px; padding-left:20px; border-right: solid 2px black"><b>Entscheidungen </b> </div>
<div style="background-color:#eee; border-top:solid 1px black" class="decisions index">
	<table cellpadding="0" cellspacing="0" class="todotable">
	<tr class="firsttr">
			<th class="firsttd">stichtag</th>
			<th>title</th>
			<th>votes</th>
			<th>Typ</th>
			<th>Autor</th>
	</tr>
	<?php
		$i = 0;
		foreach ($decisions as $decision){
		$class = null;
		if($decision['Decision']['sumProposal'] > count($decision['Vote'])) $class = "class=openProposal";
	?>
	<tr <?=$class?>>
		<td class="firsttd"><?php echo date('d.m.Y', strtotime($decision['Decision']['stichtag'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['Decision']['title'], array('controller' => 'Decisions','action' => 'view', $decision['Decision']['id'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['Decision']['voteQuote'], array('action' => 'result', $decision['Decision']['id'])); ?>&nbsp;</td>
		<td><?= $this->Html->link($decision['KptnTodocat']['title'], array('action' => 'index', 'typ' => $decision['KptnTodocat']['title'])); ?>&nbsp;</td>
		<td><?php echo $decision['User']['name']; ?>&nbsp;</td>
	</tr>
		<? } ?>
	</table>
</div>
<? } ?>
-> <?php echo $this->Html->link(__('Diskussionsthemen hinzufügen'), array('controller' => 'Decisions', 'action'=>"multiadd/".$kptnTodo['Appointment']['id'])); ?>



