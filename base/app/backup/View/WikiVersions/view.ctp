<div class="eversions view">
<h2><?php  __('Eversion');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $eversion['WikiVersion']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $eversion['WikiVersion']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Speech'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $eversion['WikiVersion']['speech']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Value'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $eversion['WikiVersion']['value']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $eversion['WikiVersion']['created']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Eversion', true), array('action' => 'edit', $eversion['WikiVersion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Eversion', true), array('action' => 'delete', $eversion['WikiVersion']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $eversion['WikiVersion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Eversions', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Eversion', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
