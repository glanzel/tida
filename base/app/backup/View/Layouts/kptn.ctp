<?php  header("Content-Type: text/html; charset=utf-8"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<title> das klingt doch gut ... | <?=$title_for_layout?> </title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<?=$this->Html->css('kptndesign'); ?>

	<? //$this->Html->css('sheet/jquery.sheet.css'); ?>
	<? //$this->Html->css('sheet/jquery-ui/theme/jquery-ui.css'); ?>
	<? //$this->Html->script('sheet/jquery-1.4.3.min.js');?>
	<? //$this->Html->script('sheet/jquery.sheet.min.js');?>
	<?= $this->Html->script('kptn.js');?>

</head>
<body class="mainPage">
<!-- <body class="mainPage" onkeyup="alert('hehe')"> -->

<? setlocale(LC_ALL, 'de_DE.utf8'); ?>
<center>
<!--
<div style='width:550px; height:132px; background-image: url("../tida/img/lampions_sw_farbe_klein.jpg" ); background-color: grey; '>

	<div style="float:right;">
	<div style="font-size:33px; padding-top:10px; color: red;"> klingt gut zwo</div>
	<b> dann tanz halt im juli</b><br>
	<span style="font-size:16px; color: lightblue"> 7.7 - 10.7 2016<span>	
	</div>
</div>
-->

<?= $this->Session->flash() ?>
	<table id="haupttabelle">
	<!-- start header -->
		<tr>
			<td> 
				<div style="float:right; margin-top:40px;">  
					<? echo $this->Html->image("tida_icons.png", array('width' => 130,)); ?>
				</div>
				<div >
					<div style="float:left; margin-right:15px">
						<? echo $this->Html->image("logo2.png", array('width' => 80, 'id' => 'tidalogo')); ?>
					</div>
					<div>
						<div style="font-size:33px; padding-top:10px;"> klingt<span style="font-weight:600;color:#aa8800">gut</span> </div>
						<div style="font-size:18px"> 8.7 bis 10.7.2016</div>
					</div>
				</div>
				
			</td>
		</tr>
		<tr><td class="menutd" colspan="2"><div id="menu"></div></td></tr> <!--linie -->
		<tr><td id=“footertd“ colspan="2" >
		<div >
			<div style="font-size:110%; ">
				<div style="float:right">
					<? if($username){ ?>
					<?= $this->Html->link("ich", '/Users/edit'); ?> |
					<?= $this->Html->link("logout", '/Users/logout'); ?>
					<? } else { ?>
					<?= $this->Html->link("teilnehmen", '/Users/add'); ?> |
					<?= $this->Html->link("login", '/Users/login'); ?>
					<? } ?>
				</div>
				<div>
					<?= $this->Html->link("home", '/'); ?> | 
					<?= $this->Html->link("infos", '/pages/info_angemeldet'); ?> |
					<?= $this->Html->link("logistik", '/pages/logistik'); ?> |
					<?= $this->Html->link("fotos", '/pages/color'); ?> |
					<?= $this->Html->link("zeitplan", '/WikiEntries/view/zeitplan'); ?> | 
	
					<? if($username){ ?>
					<?= $this->Html->link("anfahrt", '/pages/anfahrtmit'); ?> |
					<?= $this->Html->link("bezahlen", '/pages/bezahlen'); ?> |
					<? } else { ?>
					<?= $this->Html->link("anfahrt", '/pages/anfahrt'); ?>  |
					<?= $this->Html->link("bezahlen", '/pages/anfahrt'); ?>  |
					<? } ?>
					<? if($cox){ ?> ||
						
					<?= $this->Html->link("mail", '/Mails/sendwebmail'); ?>  |
					<?= $this->Html->link("gäste", '/Users/'); ?> |
					<? } ?>
				</div>
			</div>
		</div> <!--end of siteinfo-->
		<hr class=contenthr />		
		</td></tr>

	<!-- end header -->

	<!-- start content -->
		<tr><td colspan="2" class="content">
			<div id=“hauptteil“>
			<?=$content_for_layout?>
			<?=$this->Session->flash('auth');?>
			</div>
		</td></tr>

	<!-- end content -->

	</table>			

</center>

</body>

</html>


