<br>
<?= "Typ: ".$doodle['KptnTodocat']['title']." | Gruppe: ".$doodle['Admingroup']['name']; ?>
<hr>
<b><?php echo $doodle['Doodle']['title'];  ?> </b>
<?php if(in_array($doodle['Editierbar']['id'], $groups)) echo $this->Html->link($this->Html->image("edit.gif", array("border" => 0)), array('controller' => 'Doodles', 'action'=>'edit', $doodle['Doodle']['id']),array('escape' => false),null,false); ?>
<br>
<?php echo $doodle['Doodle']['desc'];  ?> 

<br>
<?	echo $this->Form->create('Doodle', array('action' => 'saveVote')); ?>

<table class="result"><tr>
<td></td>
<?  $greenValue = "style='border-top: solid 8px #5F9410;'    "; //'background: #6FC420'";
	foreach($votes as $uservotes){
		foreach($uservotes as $key => $vote){ 
			$green = $vote['Appointment']['id'] == $doodle['Doodle']['appointment_id'] ? $greenValue : ''; 
			?>
			
			<td <?=$green ?>><?= date('D d.m H:i', strtotime($vote['Appointment']['von'])); ?></td>
			 
		<?
		$summen[$key] = 0;	
		}
			if($doodle['Doodle']['appointment_id'] != null) {
				echo "<td>";
				echo " ".$this->Html->link($this->Html->image("x.gif", array('title' => 'Auswahl zurücksetzen',"border" => 0)), array('action'=>'resetResult', $doodle['Doodle']['id']),array('escape' => false),"Wirklich Termin zurücksetzen?",false);
				echo "</td>";
				
			}   

		break;  
	}
	?>
</tr>

<? 
	foreach($votes as $forename => $uservotes){ ?>
	<tr <? if($myforename == $forename) echo "id='my_doodle_row'" ?> > 
	<td class="col_title"><?= $forename ?> </td>
	<? 
		foreach($uservotes as $key => $vote){ 
		 	$green = $vote['Appointment']['id'] == $doodle['Doodle']['appointment_id'] ? $greenValue : ''; 
			$dabei = '';
			if( $vote['au']['teilnahme'] != NULL){
				$dabei = $teilnahme[$vote['au']['teilnahme']];
				$summen[$key] += $vote['au']['teilnahme'];
			} 
			
			$class = "";
	  		echo "<td >";
		  	if($myforename == $forename){
		  		$class = "class=value";
				echo "<span id='doodle_votes' class='vote' style='display:none' >";
		  		echo $this->Form->radio($vote['Appointment']['id'], array( 2 => 'Ja', 1 => '(Ja)', 0 => 'Nein'), array('legend' => false, 'value' => $vote['au']['teilnahme']));
		  		echo "</span>";
		  	}
			echo "<span $class> $dabei </span>"; 

		  	echo "&nbsp;</td>";
	 	} 
		if($myforename == $forename){
			echo "<td>";
			echo "<span class='value'> ";
			echo "<a href='javascript:editDoodle()' >".$this->Html->image("edit.gif", array("border" => 0, "title" => "editieren"))."</a>"; 
			echo "</span>";	
			echo "<span class='vote' style='display:none'>";
			echo $this->Form->submit("diskette.gif"); //, array('action'=>'saveVotes',$doodle['Doodle']['id'] ),array('escape' => false),null,false); 
			echo "</span></td>";
		}else{
			echo "<td>&nbsp;</td>";
		}

	 ?>


	</tr>
<? } ?>

<td>pkt</td>
<? foreach($summen as $summe){ 
	?>
	<td><?= $summe; ?> </td> 
<?	}  ?>
</tr>
<td></td>
<? 
	if(in_array($doodle['Editierbar']['id'], $groups)){
		foreach($votes as $uservotes){
			foreach($uservotes as $key => $vote){
				$choose_gif = $vote['Appointment']['id'] == $doodle['Doodle']['appointment_id'] ? 'checkmark_green.gif' : 'checkmark_grey.gif'; 
				echo "<td>";
				echo $this->Html->link($this->Html->image($choose_gif, array('title' => 'Diesen Vorschlag auswählen',"border" => 0)), array('action'=>'saveResult', $doodle['Doodle']['id'],$vote['Appointment']['id']),array('escape' => false),"Wirklich diesen Tag auswählen? Das sollte nur gemacht werden wenn genug leute abgestimmt haben",false);
				echo " ".$this->Html->link($this->Html->image("x.gif", array('title' => 'löschen',"border" => 0)), array('action'=>'deleteEvent', $vote['Appointment']['id'], $doodle['Doodle']['id']),array('escape' => false),"Wirklich löschen ?",false);
				echo "</td>"; 
			}
			break;  
		} 
	}
?>
</tr>

</table>	
<?	echo $this->Form->end(); ?>

<?
	echo $this->Form->create('Doodle', array( 'action' => 'saveEvents'));
	echo $this->Form->input('id');

	echo '<br>';
	echo $this->Form->select('anzahl', array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20),array('onChange' => 'showDoodle(this.value)'));
	echo ' neue Vorschläge hinzufügen';
	echo "<br><br>";
	
	$divArr = array('div' => array('id' => "festesdatum"),"timeFormat" => '24','selected' => (strtotime($datum)+43200));

	for($i=1;$i<20;$i++){
		if(isset($this->request->data['Appointment'][$i]['von'])) $divArr['value'] = $this->request->data['Appointment'][$i]['von'];
		else $divArr['value'] = date('Y-m-d 12:00');
		//pr($divArr);
		echo "<div id='terminvor{$i}' class='datetime' style='display:none'>";
		echo $this->Form->label("vorschlag $i");
		echo $this->Form->dateTime("von{$i}", 'DMY', '24', $divArr);
		echo "</div>";
	}
	echo '<div id="doodle_view_button" style="display:none">';
	echo $this->Form->end('Speichern');
	echo '</div>';
	

?>

</div>
