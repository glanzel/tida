<div class="decisions view">
	[<?php echo $this->Html->link(__('Abstimmen'), array('controller' => 'Decisions', 'action'=>"view/".$decision['Decision']['id'])); ?>]

<h4><?php echo $decision['Decision']['title']; ?> </h4>

<?php echo $this->Form->create('Decision', array('action' => "multivote/".$decision['Decision']['id']));?>

<table class="result"><tr>
<td></td>
<? foreach($voteSummen as $vote){ ?>
	<td><?= $vote['Proposal']['title']; ?></td> 
<?	}  ?>
</tr>
<? foreach($votes as $username => $uservotes){ ?>
	<tr><td class="col_title"><?= $username ?> </td>
	<? foreach($uservotes as $vote){ 
		$selected = isset($vote['Votecat']['id']) ? $vote['Votecat']['id'] : null;
		?>
		<td class="value"><?= $this->Form->select('votecats', $votecats, array('value' => $selected, 'name' => "data[Multivote][".$vote['User']['id']."][".$vote['Proposal']['id']."]")); ?>&nbsp;</td> 
	<? } ?>
	</tr>
<? } ?>

</tr></table>	
<?php echo $this->Form->end('Submit');?>

</div>
