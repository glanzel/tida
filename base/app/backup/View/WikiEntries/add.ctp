<div class="entries form">
<?php echo $this->Form->create('WikiEntry', array('url' => array('action' => 'add')));?>
	<fieldset class=first>
 		<legend><span><?php __('Add Entry'); ?></span></legend>
 		
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('speech');
	?>
	<div class="theditbox">
	<?
		echo $this->Form->input('value', array('id' => 'theditbox'));
	?>
	</div>
	<div class="end">
		<div class="submit">
			<span class="start">&nbsp;</span>
			<span>
				<?=$this->Form->submit(__d('entries', 'save', true), array('div' => false, 'class' => 'icon save'))?>
			</span>
			<span class="end">&nbsp;</span>
		</div>
	</div>
</fieldset>
</div>
