<div class="entries index">
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Entry', true), "/$speech/entries/edit/add"); ?></li>
	</ul>
</div>
	<table cellpadding="0" cellspacing="0" class="crewlist">
	<tr>
			<th>name</th>
			<th>speech</th>
			<th>value</th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($entries as $entry):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $entry['Eversion']['name']; ?>&nbsp;</td>
		<td><?php echo $entry['Eversion']['speech']; ?>&nbsp;</td>
		<td><?php echo $entry['Eversion']['value']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), "/$speech/entries/view/".$entry['Eversion']['name']); ?>
			<?php echo $this->Html->link(__('Edit', true), "/$speech/entries/edit/".$entry['Eversion']['id']); ?>
			<?php echo $this->Html->link(__('Delete', true), "/$speech/entries/delete/".$entry['Eversion']['id'], null, sprintf(__('Are you sure you want to delete # %s?', true), $entry['Eversion']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' , array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next( ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
