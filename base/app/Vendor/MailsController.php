<?php
App::uses('CakeEmail', 'Network/Email');

class MailsController extends AppController {

	var $name = 'Mails';
	var $helpers = array('Html', 'Form');
	//var $components = array('SwiftMailer'); 

	var $yakBaseUrl = "https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/";

	function beforeFilter(){
		$this->Auth->allow('apinbox_f796tbogvg_recieve', 'yakmail_f796tbogvg_notification', 'getNewYakMails','getYakMail', 'mailgun_f796tbogvg_push');
		parent::beforeFilter();
	}
	
	function index(){
		if(array_key_exists('typ', $this->passedArgs)) $options['KptnTodocat.title'] = $this->passedArgs['typ'];
		else $options['kptn_todocat_id !='] = '100';
		
		$options['sichtbarkeit'] = $this->Admingroup->getGroups($this->Auth->user('id'));
		
		$this->Mail->order = 'datum desc';
		$mails = $this->paginate($options);

		$typen = $kptnTodocats = $this->KptnTodo->KptnTodocat->find('list');
		$this->set(compact('mails','kptnTodocats', 'typen'));
	}
	

	function view($id){
		$mail = $this->Mail->read(null,$id);
		$this->set(compact('mail'));
	}
	
	
	


	function getNewYakMails(){
		//$response = http_get("{$yakBasUrl}get/new/email/");
		$response = file_get_contents("https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/get/new/email/?domain=kptn.simpleyak.com");
		$emails = json_decode($response, true);
		foreach($emails['Emails'] as $email){
			$emailId = $email['EmailID'];
			$this->processYakMail($email);
			// Mail Löschen Attachment wird mitgelöscht
			$this->file_post_contents("https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/delete/email/", array('EmailID' => $emailId));
		}
	}
	
	function deleteAllYakMails(){
		//$response = http_get("{$yakBasUrl}get/new/email/");
		$response = file_get_contents("https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/get/all/email/?domain=kptn.simpleyak.com");
		$emails = json_decode($response, true);
		foreach($emails['Emails'] as $email){
			$emailId = $email['EmailID'];
			// Mail Löschen Attachment wird mitgelöscht
			$this->file_post_contents("https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/delete/email/", array('EmailID' => $emailId));
		}
	}

	function getYakMail($emailId){
		//$response = http_get("{$yakBasUrl}get/email/");
		$response = file_get_contents("https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/get/email/?EmailID=$emailId");
		$email = json_decode($response, true);
		$this->processYakMail($email);
		// Mail wieder Löschen Attachment wird mitgelöscht
		$this->file_post_contents("https://api.emailyak.com/v1/f3gmwvuo2kklef8/json/delete/email/", array('EmailID' => $emailId));
		
	}

	function file_post_contents($url, $postArray){	
		$jsondata = json_encode($postArray);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/json',
		        'content' => $jsondata
		    )
		);
		
		$context  = stream_context_create($opts);
		return file_get_contents($url, false, $context);

	}
			
	function processYakMail($email) {
			$data['Mail']['sender'] 	= $email['FromAddress']; // orginaler sender 
			$data['Mail']['receiver'] 	= $email['ToAddressList']; // empfangende gruppe 
			$data['Mail']['from'] 	= $email['FromAddress']; 
			$data['Mail']['replyTo'] 	= $email['ToAddressList']; // antworten An Gruppe
			$data['Mail']['subject'] 	= $email['Subject'];
			$data['Mail']['datum'] 	= date('y-m-d H:i', strtotime( $email['Received']));
			$groupName = substr($email['ToAddressList'],0,strpos($email['ToAddressList'], '@'));
			//echo("Group = $groupName <br>");
			
			$cakeEmail = new CakeEmail('default');
			
		 	if(isset($email['Attachments'])){
				foreach($email['Attachments'] as $attachment){
			 		$filename = substr($attachment,strrpos($attachment,'/')+1);
					$decoded_filename = urldecode($filename);
					//$filecontent = file_get_contents($attachment);
					$attas[] = $decoded_filename;		
			 		
			 		//echo($decoded_filename);
			 		//echo ($filecontent);
			 		//file_put_contents()
			 		
			 		//TODO
			 		//$this->SwiftMailer->setAttachment($decoded_filename, $filecontent, $this->getMimeType($decoded_filename));
				}
				$data['Mail']['attachment'] = implode(',',$attas);
			} 
			
			$group = $this->Admingroup->findByName($groupName);
			if(empty($group)){
				return; //Dies Mailadresse exsistiert nicht 
			}
			
			$toArr  = array();
			foreach($group['User'] as $user){
				$toArr[] = $user['email'];
			}
			$data['Mail']['to'] 	= $toArr;
			$data['Mail']['sichtbarkeit'] = $group['Admingroup']['id'];
			
			if($email['HtmlBody'] != ''){ 
				$data['Mail']['content'] = $email['HtmlBody'];
				$data['Mail']['type'] = 'text/html';
				$data['Mail']['cetype'] = 'html';
				$data['Mail']['lb'] = '<br>';
			}else if(isset($email['TextBody'])){ 
				$data['Mail']['content'] = $email['TextBody'];
				$data['Mail']['type'] = 'text/plain';
				$data['Mail']['cetype'] = 'text';
				$data['Mail']['lb'] = "\n";
			} 

			$this->Mail->create();
			$this->Mail->save($data);
			$id = $this->Mail->id;

		 	if(isset($email['Attachments'])){
				foreach($email['Attachments'] as $attachment){
			 		$filename = substr($attachment,strrpos($attachment,'/')+1);
					$decoded_filename = urldecode($filename);
					$filecontent = file_get_contents($attachment);
					$mkdirstr = WWW_ROOT."media/$id/";
					mkdir($mkdirstr);
					$path = WWW_ROOT."media/$id/$decoded_filename";
					$handler = fopen($path,'w');
					//pr($handler);
					$result = fwrite($handler, $filecontent);
					//echo($result);
					
					$cakeEmail->attachments(array($decoded_filename => $path));
					
				}
			} 
			$this->sendMail($data, $attachment);
			
			

			//if($key == 1){
				//pr($email);
				//pr($data);
			//}
	}
	
	/*
	 * $group - Array of Admingroups  
	 * mailArr - Asso Array for Mail
	 * must contain -> from, subject, content 
	 */
		
	function processMail($group, $data, $attachments = null){

			foreach($group['User'] as $user){
				$toArr[] = $user['email'];
			}
			$data['Mail']['to'] 	= $toArr;
			$data['Mail']['sichtbarkeit'] = $group['Admingroup']['id'];

			// TODO: nur schreiben wenn nötig
			$data['Mail']['type'] = 'text/plain';
			$data['Mail']['cetype'] = 'text';
			$data['Mail']['lb'] = "\n";

			$this->Mail->create();
			$this->Mail->save($data);
			$id = $this->Mail->id;
			

		 	if($attachments != null){
		 		//debug('rename attachment');
		 		$mkdirstr = WWW_ROOT."media/$id/";
				mkdir($mkdirstr);
				foreach($attachments as $attachment){
					$new_path = $mkdirstr.$attachment['file']['name'];
					rename($attachment['file']['path'], $new_path);
					$attachArr[] = $new_path;
				}
			} 
			$this->sendMail($data, $attachArr);
		
	}
	
	function mailgun_f796tbogvg_push(){ // email push
		//Configure::write('debug', 2);

		$this->layout = 'print';
		$this->render('leer');

/*		$dump = print_r($_REQUEST,true);
		echo ($dump);
		$data['Mail'] = $_REQUEST;
		$data['Mail']['content'] = $dump;
		//debug($data);
		$this->Mail->create();
		$this->Mail->save($data);
*/
	$this->processGunMail($_REQUEST);
	}

	function processGunMail($email){


			//bei mehreren empfänger kptn adresse herausfinden
			$toArr = explode(',',$email['sender']);
			foreach($toArr as $tmpTo){
				if(strpos($tmpTo, '@kptn.de')) $to = $tmpTo;
			}
			

			$data['Mail']['sender'] 	= $email['sender']; // orginaler sender 
			$data['Mail']['receiver'] 	= $to; // empfangende gruppe 
			$data['Mail']['from'] 	= $email['From']; 
			$data['Mail']['replyTo'] 	= $email['To']; // antworten An Gruppe
			$data['Mail']['subject'] 	= $email['subject'];
			$data['Mail']['datum'] 	= date('y-m-d H:i', strtotime( $email['Received']));
			$groupName = substr($email['ToAddressList'],0,strpos($email['ToAddressList'], '@'));
			//echo("Group = $groupName <br>");
			
			$cakeEmail = new CakeEmail('default');
			$attachment = null;
			
/*
 * 
		 	if(isset($email['Attachments'])){
				foreach($email['Attachments'] as $attachment){
			 		$filename = substr($attachment,strrpos($attachment,'/')+1);
					$decoded_filename = urldecode($filename);
					//$filecontent = file_get_contents($attachment);
					$attas[] = $decoded_filename;		
			 		
			 		//echo($decoded_filename);
			 		//echo ($filecontent);
			 		//file_put_contents()
			 		
			 		//TODO
			 		//$this->SwiftMailer->setAttachment($decoded_filename, $filecontent, $this->getMimeType($decoded_filename));
				}
				$data['Mail']['attachment'] = implode(',',$attas);
			} 
*/			
			$group = $this->Admingroup->findByName($groupName);
			if(empty($group)){
				return; //Dies Mailadresse exsistiert nicht 
			}
			
			$toArr  = array();
			foreach($group['User'] as $user){
				$toArr[] = $user['email'];
			}
			$data['Mail']['to'] 	= $toArr;
			$data['Mail']['sichtbarkeit'] = $group['Admingroup']['id'];
			
			if(isset($email['body-html'])){ 
				$data['Mail']['content'] = $email['body-html'];
				$data['Mail']['type'] = 'text/html';
				$data['Mail']['cetype'] = 'html';
				$data['Mail']['lb'] = '<br>';
			}else if(isset($email['body-plain'])){ 
				$data['Mail']['content'] = $email['body-plain'];
				$data['Mail']['type'] = 'text/plain';
				$data['Mail']['cetype'] = 'text';
				$data['Mail']['lb'] = "\n";
			} 

			$this->Mail->create();
			$this->Mail->save($data);
			$id = $this->Mail->id;
/*
		 	if(isset($email['Attachments'])){
				foreach($email['Attachments'] as $attachment){
			 		$filename = substr($attachment,strrpos($attachment,'/')+1);
					$decoded_filename = urldecode($filename);
					$filecontent = file_get_contents($attachment);
					$mkdirstr = WWW_ROOT."media/$id/";
					mkdir($mkdirstr);
					$path = WWW_ROOT."media/$id/$decoded_filename";
					$handler = fopen($path,'w');
					//pr($handler);
					$result = fwrite($handler, $filecontent);
					//echo($result);
					
					$cakeEmail->attachments(array($decoded_filename => $path));
					
				}
			} 
*/			

			$this->sendMail($data, $attachment);
			
			

			//if($key == 1){
				//pr($email);
				//pr($data);
			//}

	}

	
	function saveMail($from, $to){
	
	}

	function yakmail_f796tbogvg_notification(){ //not email push only notification 
		$this->layout = 'print';
		
		$emailId = $_REQUEST['EmailID'];
		Configure::write('debug', '2');
		//debug($_REQUEST);
		
		if(empty($emailId)){
			$dump = print_r($_REQUEST,true);
			echo ($dump);
			$data['Mail'] = $_REQUEST;
			$data['Mail']['beschreibung'] = $dump;
			$data['Mail']['content'] = $dump;
			//debug($data);
			$this->Mail->create();
			$this->Mail->save($data);
		}else{
			$this->getYakMail($emailId);	
		}
		
		//if(isset($_REQUEST['html'])) $_REQUEST['content'] = $_REQUEST['html']; 
		//else if(isset($_REQUEST['plain'])) $_REQUEST['content'] = $_REQUEST['plain']; 

		//pr($data);
		
		//echo("data: ");
		//pr($this->request->data);
		 Configure::write('debug', '0');		
	}

	

	function apinbox_f796tbogvg_recieve(){
		$this->layout = 'print';
		
		$dump = print_r($_REQUEST,true);
		$_REQUEST['dump'] = $dump;
		
		if(isset($_REQUEST['html'])) $_REQUEST['content'] = $_REQUEST['html']; 
		else if(isset($_REQUEST['plain'])) $_REQUEST['content'] = $_REQUEST['plain']; 

		$data['Mail'] = $_REQUEST;
		//pr($data);
		
		$this->Mail->create();
		$this->Mail->save($data);
		
		//echo("data: ");
		//pr($this->request->data);		
	}
	
	function testafr(){
		
	} 	
	
	function sendwebmail(){
		if(!empty($this->request->data)){
			$data = $this->request->data;
			$data['Mail']['from'] = $data['Mail']['sender'] = $data['Mail']['replyTo'] = $this->Auth->user('email');
			$group = $this->Admingroup->read(null,$data['Mail']['to']); 
			$data['Mail']['datum'] = date('y-m-d H:i:s');
			$data['Mail']['receiver'] = $group['Admingroup']['name']."@kptn.de";
			
			if(! empty($data['Document']['file']['name'])){
				//pr($data);
				//$file['file']['content'] = $data['Document']['file']['tmp_name'];
				$decoded_filename = urldecode($data['Document']['file']['name']);
				$data['Mail']['attachment'] = $file['file']['name'] = $decoded_filename;
				$mkdirstr = WWW_ROOT."media/attachments/";
				mkdir($mkdirstr);
				$path = "$mkdirstr/$decoded_filename";
				move_uploaded_file($data['Document']['file']['tmp_name'], $path);
				$file['file']['path'] = $path;
				//pr($file);

				$this->processMail($group,$data, array($file));
						
			}else{
				//echo ('send without attachment');
				$this->processMail($group,$data);
			}
			$this->redirect("/pages/yes"); 
		}
		$this->set('groups', $this->Admingroup->find('list'));
	}



	// $email must be a CakeEmail Object;	
	function sendMail($mailArray, $attachments = null){
		pr($mailArray);
		
		$email = new CakeEmail('default');
				
		$email->emailFormat($mailArray['Mail']['cetype']);
		$email->sender($mailArray['Mail']['sender']);
		$email->from(array($mailArray['Mail']['from']));
		$email->replyTo($mailArray['Mail']['replyTo']);
		$email->bcc($mailArray['Mail']['to']);
		$email->subject($mailArray['Mail']['subject']);
		
		
		if(!empty($attachments)) $email->attachments($attachments);
		
		try{
		$email->send($mailArray['Mail']['content']);
		}catch(Exception $e){
			debug ('Fehler beim defaultversand<br> versende per smtp<br> alles ok.');
	
			$cemail = new CakeEmail('smtp');	
			$cemail->emailFormat($mailArray['Mail']['cetype']);
			$cemail->sender($mailArray['Mail']['sender']);
			$cemail->from(array($mailArray['Mail']['from']));
			$cemail->replyTo($mailArray['Mail']['replyTo']);
			$cemail->bcc($mailArray['Mail']['to']);
			$cemail->subject($mailArray['Mail']['subject']);
			//Nur nötig beim Versand per smtp
			//$mailArray['Mail']['content'] .= $mailArray['Mail']['lb'].$mailArray['Mail']['lb'].$mailArray['Mail']['sender'];
			$cemail->send($mailArray['Mail']['content']);
			
			
			
		}
		
	}
	
	function oldsendMail($mailArray){
		$this->SwiftMailer->to	= $mailArray['Mail']['to'];
		$this->SwiftMailer->from = $mailArray['Mail']['from'];
		$this->set('content', $mailArray['Mail']['content'].'/n/n'.$mailArray['Mail']['sender']);
		
	
		if (!$this->SwiftMailer->send('mail', $mailArray['Mail']['subject'], $mailArray['Mail']['type'])) {
			 $this->log('Error sending email .', LOG_ERROR);
		} 
	}

	function saveCat(){
		foreach($this->request->data['Mail'] as $mail_id => $kptncat){
			$data = $this->Mail->read(null,$mail_id);
			$this->Mail->create();
			$data['Mail']['kptn_todocat_id'] = $kptncat;
			$this->Mail->save($data);
		}
		$this->redirect(array('action' => 'index'));
		//$this->redirect(array('action' => "view/$id"));
	}
	
	function papierkorb($id){
		$data = $this->Mail->read(null,$id);
		$this->Mail->create();
		$data['Mail']['kptn_todocat_id'] = 100;
		$this->Mail->save($data);
		$this->redirect(array('action' => 'index'));
		//$this->redirect(array('action' => "view/$id"));
	}
	//deprecated	
	
	function imapOpen(){
		$server = "{imap.gmail.com:993/imap/ssl}INBOX";
		$user = "teilees.de@gmail.com";
		$passwd = "es2teile";
		 
		$mbox = imap_open($server,$user,$passwd) or die("Could not open Mailbox - try again later!");
		//pr($mbox);
		//imap_close($mbox);
		//return;
		
		$headers = imap_headers($mbox);

		if ($headers == false) {
		    echo "Call failed<br />\n";
		} else {
		    foreach ($headers as $key => $val) {
		    	echo "$key  ";
		        echo $val . "<br />\n";
		        if($key==6){
			        $struct = imap_fetchstructure ($mbox, $key+1);
		        	//$part_array = $this->create_part_array($struct);
		        	pr($struct);
			        $body = imap_fetchbody ($mbox, $key+1, 1);
		        	pr($body);
		        	
		        }
		    }
		}
		
		/*
		$message_count = imap_num_msg($mbox);
		 
		for ($i = 1; $i <= $message_count; ++$i) {
		    echo imap_header($mbox, $i) . " (" . date("Y-m-d H:i:s", strtotime($header->MailDate)) . ")<br />";
		}
		imap_close($mbox);
		*/
	}
	//deprecated	

	function create_part_array($structure, $prefix="") {
	    //print_r($structure);
	    if (sizeof($structure->parts) > 0) {    // There some sub parts
	        foreach ($structure->parts as $count => $part) {
	            $this->add_part_to_array($part, $prefix.($count+1), $part_array);
	        }
	    }else{    // Email does not have a seperate mime attachment for text
	        $part_array[] = array('part_number' => $prefix.'1', 'part_object' => $obj);
	    }
	   return $part_array;
	}

	//deprecated	
	
	// Sub function for create_part_array(). Only called by create_part_array() and itself.
	function add_part_to_array($obj, $partno, & $part_array) {
	    $part_array[] = array('part_number' => $partno, 'part_object' => $obj);
	    if ($obj->type == 2) { // Check to see if the part is an attached email message, as in the RFC-822 type
	        //print_r($obj);
	        if (sizeof($obj->parts) > 0) {    // Check to see if the email has parts
	            foreach ($obj->parts as $count => $part) {
	                // Iterate here again to compensate for the broken way that imap_fetchbody() handles attachments
	                if (sizeof($part->parts) > 0) {
	                    foreach ($part->parts as $count2 => $part2) {
	                        add_part_to_array($part2, $partno.".".($count2+1), $part_array);
	                    }
	                }else{    // Attached email does not have a seperate mime attachment for text
	                    $part_array[] = array('part_number' => $partno.'.'.($count+1), 'part_object' => $obj);
	                }
	            }
	        }else{    // Not sure if this is possible
	            $part_array[] = array('part_number' => $prefix.'.1', 'part_object' => $obj);
	        }
	    }else{    // If there are more sub-parts, expand them out.
	        if (sizeof($obj->parts) > 0) {
	            foreach ($obj->parts as $count => $p) {
	                add_part_to_array($p, $partno.".".($count+1), $part_array);
	            }
	        }
	    }
	}

	
	//deprecated	
	function popMail(){
		//require_once 'Base/src/ezc_bootstrap.php';
		//autoload(ezcMailImapTransport);
	    $imap = new ezcMailImapTransport( "imap.gmail.com" );
	    $imap->authenticate( "teilees.de", "es2teile" );
	    $imap->selectMailbox( 'Inbox' );
	    $rawMails = $imap->fetchAll();
	    $parser = new ezcMailParser();
	    $retMails = $parser->parseMail( $rawMails );
	    foreach ( $retMails as $retMail ){
		    echo('<pre>');
		    print_r($retMail);
		    echo('</pre>');
	    }
	}
	
	
	
    function getMimeType($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
    
 
    function cakeEmailDeliverTest($email){
		$this->view = 'getYakMail';
	    $erg = CakeEmail::deliver($email, 'Test delivery', "A delivery by CakeEmail::deliver from server: ".env('SERVER_NAME'), array('from' => 'test@kpt.de'));
		pr($erg);
    }
    
    function cakeEmailTest($email){
		$this->view = 'getYakMail';
		$cakeEmail = new CakeEmail('default');	
		$data['Mail']['sender'] 	= 'test@kptn.de'; // orginaler sender 
		$data['Mail']['receiver'] 	= $email; // empfangende gruppe 
		$data['Mail']['from'] 	= 'test@kptn.de'; 
		$data['Mail']['subject'] 	= 'das ist eine TEstmail '.date('H:i:s');
		$data['Mail']['to'] 	= $email;
		$data['Mail']['content'] = "Dies ist einen Testemail gesendet vom server: ".env('SERVER_NAME');
		$data['Mail']['type'] = 'text/html';
		$data['Mail']['cetype'] = 'html';
		$data['Mail']['lb'] = '<br>';
		try{
			$this->sendMail($data);
		}catch(Exception $e){
			echo "<pr>";
			echo $e;
			echo "</pr>";
		}
	}
	
	function phpMailTest($to){
		$this->view = 'getYakMail';

		echo ('legee daten an<br>');
		$Name = "Tester"; //senders name
		$email = "test@kptn.de"; //senders e-mail adress
		$recipient = $to; //recipient
		$mail_body = "Test: und zwar der php mail funktion vom server: ".env('SERVER_NAME'); //mail body
		$subject = "php:mail Testmail"; //subject
		$header = "From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields
		echo ('schreib init_set<br>');
		ini_set('sendmail_from', 'test@kptn.de'); //Suggested by "Some Guy"

		echo ('try send<br>');
		try{		
			$erg = mail($recipient, $subject, $mail_body, $header); //mail command :)
			echo "erg ist $erg <br>";
		}catch(Exception $e){
			echo $e;
		}
	}
	

}
?>
