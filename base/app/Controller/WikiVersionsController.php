<?php
class WikiVersionsController extends AppController {

	var $name = 'WikiVersions';
	var $paginate = array('WikiVersion' => array('order' => array('created' => 'DESC')));

	function index($name = 'start') {
		$this->WikiVersion->recursive = 0;
		$entryV = $this->paginate('WikiVersion', array('WikiVersion.name' => $name));
		//debug($entryV);
		$this->set('Eversions', $entryV);
	}
	
	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid WikiVersion', true));
			$this->sredirect(array('action' => 'index'));
		}
		$this->set('eversion', $this->WikiVersion->read(null, $id));
	}
	

}
?>