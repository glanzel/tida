<?php
class AppointmentsController extends AppController {


	var $name = 'Appointments';
	var $components = array('Auth', 'Session', 'Cookie');
	var $helpers = array('Html', 'Form');
	var $paginate = array(
		'limit' => 25,
		'order' => array(
			'von' => 'desc'
		),
	);
	
	var $uses = array('Appointment');
	
	function beforeFilter(){
		$this->Auth->allow('programm','viewProgramm');
		parent::beforeFilter();
	}
	
	function programm($layout = 'leer'){
		$this->layout = $layout;
		$programm = $this->Appointment->find('all', array('order' => array('von' => 'ASC'),'limit' => 8, 'conditions' => array('KptnTodocat.id' => 17, 'von > NOW()')));
		$this->set('appointments', $programm);
		$this->set('wochentag', array('Sonntag', 'Montag','Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'));
		//pr($programm);
	}

	
	function viewProgramm($id = null) {
		$this->layout = 'leer';
		$termin = $this->Appointment->read(null, $id);
		
		$this->set('appointment', $termin);
		$this->set('user_id', $this->Auth->user('id'));
		$this->set('wochentag', array('So', 'Mo','Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'));
	}

	
	
}
?>
