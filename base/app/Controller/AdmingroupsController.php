<?php
App::uses('AppController', 'Controller');
/**
 * Admingroups Controller
 *
 * @property Admingroup $Admingroup
 */
class AdmingroupsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Admingroup->recursive = 0;
		$this->set('admingroups', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Admingroup->id = $id;
		if (!$this->Admingroup->exists()) {
			throw new NotFoundException(__('Invalid admingroup'));
		}
		$this->set('admingroup', $this->Admingroup->read(null, $id));
		$this->set('userlist', $this->Admingroup->User->find('list', array('fields' => array('id','username'))));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Admingroup->create();
			if ($this->Admingroup->save($this->request->data)) {
				$this->Session->setFlash(__('The admingroup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admingroup could not be saved. Please, try again.'));
			}
		}
		$users = $this->Admingroup->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Admingroup->id = $id;
		if (!$this->Admingroup->exists()) {
			throw new NotFoundException(__('Invalid admingroup'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Admingroup->save($this->request->data)) {
				$this->Session->setFlash(__('The admingroup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admingroup could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Admingroup->read(null, $id);
		}
		$users = $this->Admingroup->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Admingroup->id = $id;
		if (!$this->Admingroup->exists()) {
			throw new NotFoundException(__('Invalid admingroup'));
		}
		if ($this->Admingroup->delete()) {
			$this->Session->setFlash(__('Admingroup deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Admingroup was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
