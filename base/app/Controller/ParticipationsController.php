<?php
App::uses('AppController', 'Controller');
/**
 * Participations Controller
 *
 * @property Participation $Participation
 */
class ParticipationsController extends AppController {

	function beforeFilter(){
		$this->Auth->allow('add');
		parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index($sort = 'Participation.created') {
		if(! empty($this->request->data)){
			//debug($this->request->data);
			foreach($this->request->data as $data){
				$teilnahme = $this->Participation->read(null, $data['id']);
				$teilnahme['Participation']['bezahlt'] = $data['bezahlt'];
				//$teilnahme['Participation']['buchungstag'] = $data['buchungstag'];
				//debug($teilnahme);
				$erg = $this->Participation->save($teilnahme);
				if($data['bezahlt'] == 1){ 
					//debug($teilnahme);
					//debug($erg);
				}
			}
		}
		$year = Configure::read("klingtgut.year_id");
		$this->Participation->recursive = 0;
		$options['Participation.year'] = $year;
		$participations = $this->Participation->find('all', array('conditions' => $options, 'order' => array($sort => 'ASC')));
		$this->set('participations', $participations);
		
		$start = strtotime(Configure::read('klingtgut.start'));
		$tagzwei = $start+24*60*60;
		$tagdrei = $tagzwei+24*60*60;
		$erstes_abendessen = strtotime(date("Y-m-d",$start)."21:00");
		$erstes_fruestueck = strtotime(date("Y-m-d",$tagzwei)."11:00");
		$zweites_abendessen =  strtotime(date("Y-m-d",$tagzwei)."21:00");
		$zweites_fruestueck = strtotime(date("Y-m-d",$tagdrei)."11:00");
		
		$essen = Configure::read('Essen');
		$this->set(compact('year', 'essen', 'erstes_abendessen', 'erstes_fruestueck', 'zweites_abendessen', 'zweites_fruestueck'));
		
		
	}
	
	function exportAdresses($zusage = null, $bezahlt = null){
		$year = Configure::read("klingtgut.year_id");
		$options['Participation.year'] = $year;
		if(! is_null($zusage)) $options['zusage'] = $zusage;
		if(! is_null($bezahlt) && $bezahlt == 0){ 
			$options['OR'][]['bezahlt'] = 0; 
			$options['OR'][] = 'bezahlt IS NULL';
			}
		if(! is_null($bezahlt) && $bezahlt > 0) $options['bezahlt >'] = 0;
		$participations = $this->Participation->find('all', array('conditions' => $options, 'order' => 'Participation.created ASC'));
		$this->set('participations', $participations);
		//debug($participations);
		
		$this->set('groups', array(0 => 'unsicher', WARTELISTE => 'warteliste', ZUSAGE => 'sicher', 99 => 'mich'));
		//$this->redirect("/pages/yes"); // nicht weiterleiten weil sonst die ausgabe kaputt ist
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($user_id = null) {
		$year = date('Y', strtotime(Configure::read("klingtgut.start")));
		$dabei = $this->Participation->find('first', array('conditions' => array('user_id' => $user_id, 'year' => $year)));
		if(empty($dabei)) $this->redirect(array('action' => 'add'));
		$this->set('participation', $dabei);
		$this->set('zusagenArr', array(ABSAGE => 'Du hast abgesagt.', WARTELISTE => 'Du stehst auf der Warteliste', ZUSAGE => 'Du hast zugesagt'));
		
	}

/**
 * add method
 *
 * @return void
 */
	public function add($user_id = null) {
		if(empty($user_id)) $user_id = $this->Auth->user('id');
		$year = date('Y', strtotime(Configure::read("klingtgut.start")));
		$this->set('year', $year);
		if ($this->request->is('post')) {
			$this->request->data['Participation']['zusage'] = ZUSAGE;
			$this->checkFreiePlaetze($this->request->data);
			
			if(isset($this->request->data['Participation']['anreise'])){ 
				$this->request->data['Participation']['anreise']['year'] = $year;
			}
			if(isset($this->request->data['Participation']['abreise'])){ 
				$this->request->data['Participation']['abreise']['year'] = $year;
			}
			$this->Participation->create();
			if ($this->Participation->save($this->request->data)) {
				$this->Session->setFlash(__('The participation has been saved'));
				if($this->request->data['Participation']['zusage'] == WARTELISTE) {
				    $this->redirect('/pages/warteliste');
                }
				$this->redirect(array('controller' => 'WikiEntries', 'action' => 'view', 'bezahlen'));
			} else {
				$this->Session->setFlash(__('The participation could not be saved. Please, try again.'));
			}
		}
		else {
			$this->request->data["Participation"]["user_id"] = $user_id;
			$this->request->data["Participation"]["year"] = Configure::read("klingtgut.year_id");
		}
        $this->set(compact('year'));
    }

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	
	function edit($user_id = null) {
		$year = Configure::read("klingtgut.year_id");
		if (!empty($this->request->data)) { // daten wurden bearbeitet
            if($this->request->data['Participation']['zusage'] == ZUSAGE && $this->zusage != ZUSAGE) {
                //auf Zusage geändert
                $this->checkFreiePlaetze($this->request->data);
            }
			if ($this->Participation->save($this->request->data)) {
				$this->Session->setFlash(__('The Participation has been saved'));
				if($this->request->data['Participation']['zusage'] == WARTELISTE) {
				    $this->redirect('/pages/warteliste');
                }
				$this->redirect('/pages/yes');
			} else {
				$this->Session->setFlash(__('The Participation could not be saved. Please, try again.'));
			}
		} else {  //edit zum angucken aufrufen
			$this->request->data = $this->Participation->find('first', array('conditions' => array("user_id" => $user_id, "year" => $year)));
			if(empty($this->request->data)){
				$this->Session->setFlash(__('No Participation for this year found. Redirect to add.'));
				$this->redirect('add');
			}
		}
		$this->set(compact('year'));
	}
	
	function checkFreiePlaetze(& $data){
	    if ($data['Participation']['zusage'] != ZUSAGE) {
	        return;
        }
		$year = Configure::read("klingtgut.year_id");
		$anzahlPlaetze = Configure::read("klingtgut.anzahlPlaetze");
		$teilnehmers = $this->Participation->find('all', array('conditions' => array('zusage' => ZUSAGE, 'year' => $year),'fields' => array('user_id', 'mitangemeldet'), 'recursive' => 0));
		$anzahl = 0;
		foreach($teilnehmers as $teilnehmer) {
                    $anzahl += 1;
                    $anzahl += $teilnehmer['Participation']['mitangemeldet'];
                }
        $anzahl += 1;
		$anzahl += $data['Participation']['mitangemeldet'];
		if($anzahl > $anzahlPlaetze){
			$this->Flash->set(__('Keine freien Plätze mehr, du bist erstmal nur auf der Warteliste.'));
			$data['Participation']['zusage'] = WARTELISTE;
		}
		//debug($anzahl);
	}

    function mitfahrt(){
 		$year = Configure::read("klingtgut.year_id");
        $this->pageTitle = "Mitfahrgelegenheiten";
        $options = array('mitfahrgelegenheit >' => 0, 'year' => $year);
        $users = $this->Participation->find('all', array('conditions' => $options, 'order' => 'forename asc'));
        $this->set('users', $users);
        $this->set('sucheoderbiete', array('', 'suche', 'biete'));

    }


    /**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Participation->id = $id;
		if (!$this->Participation->exists()) {
			throw new NotFoundException(__('Invalid participation'));
		}
		if ($this->Participation->delete()) {
			$this->Session->setFlash(__('Participation deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Participation was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
