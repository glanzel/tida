<?php
class ProposalsController extends AppController {

	var $name = 'Proposals';
	var $uses = array('Proposal','Admingroup');

	function index() {
		$this->Proposal->recursive = 0;
		$this->set('proposals', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid proposal'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Proposal->contain(array('User','DecisionComment','Votegroup' => array('Decision')));
		$proposal = $this->Proposal->read(null, $id);
		debug($proposal);
		$this->set('proposal', $proposal);
	}

	function create() {
		if (!empty($this->request->data)) {
			$this->Proposal->Votegroup->Decision->create();
			$this->request->data['Proposal']['erstellt'] = date('Y-m-d');
			$this->request->data['Proposal']['stichtag'] = date('Y-m-d', time() + 3600*24*$this->request->data['Proposal']['laufzeit']);
			$this->request->data['Proposal']['user_id'] = $this->Auth->user('id');

			$decision['Decision'] = $this->request->data['Proposal'];
			$decision['Decision']['title'] = $decision['Decision']['title']." ?";
			unset($decision['Decision']['beschreibung']); 
			
			//wenn nicht angegeben ist ->alle können dies sehen
			if(!isset($decision['Decision']['admingroup_id']))	$decision['Decision']['admingroup_id'] = 16; 
			//wenn nicht angegeben ist ->leitendeOrga können dies editieren 
			if(!isset($decision['Decision']['stimmberechtigt'])) $decision['Decision']['stimmberechtigt'] = 8;
		
			$this->Proposal->Votegroup->Decision->save($decision);
			$decision_id = $this->Proposal->Votegroup->Decision->id;
			$votegroup_id = $this->Proposal->Votegroup->Decision->addGroup($decision_id);
			
			$this->add($decision_id, $votegroup_id);		
			$this->redirect(array('controller' => 'Decisions', 'action' => 'index'));			
		}
		$kptnTodocats = $this->Proposal->Votegroup->Decision->KptnTodocat->find('list');
		$admingroups = $this->Admingroup->find('list');
		$this->set(compact('admingroups','kptnTodocats'));
	}

	function add($decision_id, $votegroup_id = null) {
		if (!empty($this->request->data)) {
			$this->Proposal->create();
			$this->request->data['Proposal']['erstellt'] = date('Y-m-d');
			$this->request->data['Proposal']['user_id'] = $this->Auth->user('id');
			if($votegroup_id) $this->request->data['Proposal']['votegroup_id'] = $votegroup_id;
			
			//pr($this->request->data);
			if ($this->Proposal->save($this->request->data)) {
				$this->Session->setFlash(__('The proposal has been saved'));


				//den stichtag nach hinten verschieben
				$data = $this->Decision->read(null,$decision_id);
				$dateInAWeek = date('Y-m-d', time()+3600*24*7);
				//echo(" $dateInAWeek ".($data['Decision']['stichtag'] < $dateInAWeek));
				if($data['Decision']['stichtag'] < $dateInAWeek ){
					$this->Decision->create();
					$data['Decision']['stichtag'] = $dateInAWeek;
					$data['Decision']['bedenkzeit'] = $data['Decision']['bedenkzeit']+1;
					$this->Decision->save($data);			
				}
				debug($this->request->data);
				$this->redirect(array('controller' => 'Decisions', 'action' => "view/$decision_id"));			

				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The proposal could not be saved. Please, try again.'));
			}
		}
		$this->set('decision_id', $decision_id);
		$votegroups = $this->Proposal->Votegroup->find('list', array('conditions' => array('decision_id' => $decision_id)));
		$this->set(compact('votegroups'));
		
	}

	function edit($id, $decision_id) {
		
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid proposal'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {

			// alle getätigten abstimmungen zu diesem proposal wieder löschen
			// damit niemand für etwas abgestimmt hat was nachträglich verändert wurde
			$prop = $this->Proposal->read(null,$id);
			//pr($prop);
			$this->log("User ".$this->Auth->user('name')." edited Proposal ".$id, 'edit');
			foreach($prop['Vote'] as $vote){
				//echo($vote['id']);
				$this->Proposal->Vote->delete($vote['id']);
			}
	
			if ($this->Proposal->save($this->request->data)) {
				$this->Session->setFlash(__('The proposal has been saved'));
				$this->log("User ".$this->Auth->user('name')." edited Proposal ".$id);
				$this->redirect(array('controller' => 'Decisions','action' => 'view', $decision_id));
				//debug($this->request->data);
			} else {
				$this->Session->setFlash(__('The proposal could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Proposal->read(null, $id);
		}
		$votegroups = $this->Proposal->Votegroup->find('list', array('conditions' => array('decision_id' => $decision_id)));
		$this->set(compact('votegroups'));
		$this->set('id', $id); 
		$this->set('decision_id', $decision_id); 
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for proposal'));
			$this->redirect(array('controller' => 'decisions', 'action'=>'view', $this->request->params['named']['backid']));
		}
		if ($this->Proposal->delete($id)) {
			$this->Session->setFlash(__('Proposal deleted'));
			//pr($this->request->params);
			$this->redirect(array('controller' => 'decisions', 'action'=>'view', $this->request->params['named']['backid']));
		}
		$this->Session->setFlash(__('Proposal was not deleted'));
		$this->redirect(array('controller' => 'decisions', 'action'=>'view', $this->request->params['named']['backid']));
	}
	
	function changeGroup($proposal_id, $decision_id){
		debug($this->request->data);
		$prop = $this->Proposal->read(null, $proposal_id);
		debug($prop);
		$this->Proposal->create();
		$prop['Proposal']['votegroup_id'] = $this->request->data['Proposal']['votegroup_id'];
		//$this->request->data['Proposal']['votegouid'] = $proposal_id;
		$this->Proposal->save($prop);
		
		debug($prop);
		$this->redirect(array('controller' => 'Decisions', 'action' => 'view', $decision_id));		
	}
	
	
}
?>