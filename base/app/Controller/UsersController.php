<?php
App::uses('CakeEmail', 'Network/Email');

// your code here

//require_once 'Base/src/ezc_bootstrap.php';
//require_once 'vendors/zeta/Mail/src/transports/imap/imap_transport.php';
//require_once 'vendors/zeta/Mail/src/transports/imap/imap_set.php';
//require_once 'vendors/zeta/Base/src/ezc_bootstrap.php';



//echo("hello<br>".get_include_path()."<br>");

class UsersController extends AppController {
	
	var $name = 'Users';
	var $helpers = array('Html', 'Form');
	//var $components = array('SwiftMailer'); 
	var $uses = array('User','AdmingroupsUser','Mail');
	var $protectedGroups = array('7','9','10');

	function beforeFilter(){
		//echo "before";
		//pr($this->Auth->user());
		$this->Auth->allow('wrongpw', 'add', 'activate','resetpassword','noaccess','notactivated','yes','registersuccess','no', 'logout','sl','willkommen','login');
		$this->Auth->autoRedirect = false;

		//pr($this->request);

/*		//last login
		if($this->request->action == "login"){
			$arr = array('User.username' => $this->request->data['User']['username']);
			//pr($arr);
			$user = $this->User->find('first', $arr);
			//pr($user);
			$user['User']['lastlogin'] = date("d.m.y");
			//pr($user);
			$this->User->save($user);
		}
*/		
		// workaround for not edit password if ''
		if(isset($this->request->data['User']['password']) && $this->request->data['User']['password'] == '') unset($this->request->data['User']['password']);
			//echo "hallo user";	
			parent::beforeFilter();
		}
		
	public function login($email = null) {
	    if ($this->request->is('post')) {
			$this->User->recursive = -1;
			$this->request->data["User"]["username"] = $this->request->data["User"]["email"];

		if ($this->Auth->login()) {
			$this->User->id = $this->Auth->user('id');
			$this->redirect(array('controller'=>'participations', 'action'=>'view', $this->User->id));
			
		} else {
		    $this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
				$this->redirect(array('action' => 'noaccess'));
		}
	    }
	    else {
		      $this->request->data["User"]["email"] = $email;
		  }
	}
	
	function unreadMails(){
		$lastlogin = $this->Auth->user('lastlogin');
		$this->Session->write('lastlogin', $lastlogin);
		$options['sichtbarkeit'] = $this->Admingroup->getGroups($this->Auth->user('id'));
		$options['datum >'] = $lastlogin;
		$this->Mail->order = 'datum desc';
		$mails = $this->paginate('Mail', $options);
		$this->Session->write('unreadMails', count($mails));
		$this->log("unread mails : ".count($mails));
	}	

	function doSync(){
		$this->Session->write('tmp.password', $this->request->data['User']['password']);
		$this->redirect(array('controller' => 'Backups', 'action' => 'sync'));
	}		
	
	function checkForBackup(){
		$link = 'http://'. env('SERVER_NAME').Router::url($this->Auth->redirect());
		$server = Configure::read('Backup.this.destination');
		echo ($server);
		echo('
			<script>
			var ls = localStorage.getItem("crew.kptn.doBackup");
			if(ls == "yes"){
				window.location.replace("'.$server.'/Backups/check/username:'.$this->Auth->user("username").'");
			}else{
				window.location.replace("'.$link.'");
			}
			</script>
		');

/*		
		$erg = 'no';
		try{
			echo('haha');
			//$this->redirect("$server/Backups/exist");
			//$erg = file_get_contents("$server/Backups/exist");
		}catch(Exception $e){}
		if(substr($erg,0,3) == 'yes'){
			$lastbackuptime = substr($erg, 4);
			$diff = strtotime(date('Y-m-d H:i')) - strtotime($lastbackuptime);
			$diff = $diff/ 3600; // Zeit seit letztem Backup in stunden
			if($diff > 72){ // wenn backup mehr als 3 Tage alt
				$this->redirect("$server/Backups/import/username:".$this->Auth->user('username'));
			}
		}
*/
	}


	function oldlogin() {
      // Check for incoming login request.
 		//echo('login started<br>');
      
      if ($this->request->data) {
   		//echo('request data is there <br>');	
   		//pr($this->request->data);
      	//pr($this->Auth->user());
      	// Use the AuthComponent's login action
      	//$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
   		//pr($this->request->data);
      	if ($erg = $this->Auth->login()) {
	   		//echo('Auth login is correct ');	

				//pr($this->request->data);
				// Retrieve user data
		   	$results = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username']), 'fields' => array('User.active')), null, false);
		     	// Check to see if the User's account isn't active
				if ($results['User']['active'] == 0) {
		             // Uh Oh!
		             $this->Session->setFlash('Your account has not been activated yet!');
		             //$this->Auth->logout();
		             //$this->redirect(array('action' => 'notactivated'));
		     
				// Cool, user is active, redirect post login
				} else {
					echo('angeblich alles ok');
					//pr($this->Auth->user());
					$this->redirect('/');
				}
			}				
			//$this->redirect(array('action' => 'noaccess'));
		}
		//echo ("erg: $erg <br>");
		
	}

/*
	function sl($code) {
		if($code == 'TZUI') $this->Auth->login();
		$this->redirect('/');
	}
*/


	function logout() {
		$this->Cookie->delete('_path');
		$this->Cookie->delete('slcheck');
		$this->Auth->logout();
		$this->redirect('/');
	}
	
	function index($sort = 'created') {
		if(! $this->isCox) $this->redirect('/pages/home');
		$this->pageTitle = "Benutzerliste";
		
		//standardwerte
		//if(array_key_exists('group', $this->passedArgs)) $options['AdmingroupsUser.id'] = $this->passedArgs['group'];
		//else $options['AdmingroupsUser.admingroup_id'] = 16;
		$conditions = array();
		$options = array('conditions' => $conditions, 'order' => array($sort => 'ASC'));
		
		//$this->User->bindModel(array('hasOne' => array('AdmingroupsUser')));
		//$users = $this->User->find('all', array('conditions' => $options, 'order' => 'created asc'));
		
		//Test Code für 2 DBs kann später gelöcht werden
		//$this->User->setDatabase('goldammer');
		//$users = $this->User->union('tida', 'goldammer', array());
		$users = $this->User->find('all', $conditions);
		
		
		//Test Code Ende
		
		
		
		//$users = $this->User->find('all', $options);
		//debug($users);
		$this->set('users', $users);
		
	}
	

	function administrate() {
		$this->groupAllow(array(7),'/pages/home');

		$this->pageTitle = "Benutzerliste";
		$groups = $this->User->Admingroup->find('all');
		$users = $this->paginate('User'); //, $options);
		//pr($users);
		$this->set('users', $users);
		$this->set('groups', $groups);
		
		if(!empty($this->request->data)){
			
				foreach($this->request->data['ids'] as $user_id => $user){
				foreach($user as $group_id => $status){

					$group = null;
					$group = $this->AdmingroupsUser->find('first',array('conditions' => array('user_id' => $user_id, 'admingroup_id' => $group_id)));
					$group['AdmingroupsUser']['user_id'] = $user_id;
					$group['AdmingroupsUser']['admingroup_id'] = $group_id;
		
					
					if($status){
						$this->AdmingroupsUser->create();
						$error = $this->AdmingroupsUser->save($group);
						
					} 
					else{
						if(isset($group['AdmingroupsUser']['id'])) $this->AdmingroupsUser->delete($group['AdmingroupsUser']['id']);
					} 
				}
			}
		}
		
	}

	function removeFromGroup($group_id, $user_id){
		$this->removeFromGroupImpl($group_id, $user_id);
		$this->redirect(array('action'=>'view', $user_id));
	}
	function removeUserFromGroup($group_id, $user_id){
		$this->removeFromGroupImpl($group_id, $user_id);
		//$this->redirect(array('controller'=>'admingroups', 'action'=>'view', $group_id));
	}

	function removeFromGroupImpl($group_id, $user_id){
		debug("removeFromGroupImpl start");
		if(in_array($group_id, $this->protectedGroups) && ! $this->isAdmin($this->Auth->user('username'))) return;
		debug("removeFromGroupImpl end");
		$groupUser = $this->AdmingroupsUser->find('first',array('conditions' => array('user_id' => $user_id, 'admingroup_id' => $group_id)));
		$this->AdmingroupsUser->delete($groupUser['AdmingroupsUser']['id']);
	}

	function addToGroup($user_id){
		$group_id = $this->request->data['Admingroup']['id'];
		debug($this->request->data);
		$this->addToGroupImpl($group_id, $user_id);	
		$this->redirect(array('action'=>'view', $user_id));
	}

	function addUserToGroup($group_id){
		$user_id = $this->request->data['User']['id'];
		//debug($this->request->data);
		$this->addToGroupImpl($group_id, $user_id);	
		$this->redirect(array('controller' => 'Admingroups', 'action'=>'view', $group_id));
	}

	function addToGroupImpl($group_id, $user_id){
		if(in_array($group_id, $this->protectedGroups) && ! $this->isAdmin($this->Auth->user('username'))) return;

		$au = array('admingroup_id' => $group_id, 'user_id' => $user_id);
		$this->AdmingroupsUser->create();
		$this->AdmingroupsUser->save($au);
	}


	function myview() {
		$this->view = "view";
		$this->viewImpl($this->Auth->user('id'));

	}

	function viewByName($name = null) {
		$this->layout = "lohnabrechnung";
		$this->view = "view";
		$user = $this->User->findByName($name);
		$this->viewImpl($user['User']['id']);
	}


	function view($id = null) {
		$this->viewImpl($id);
	}


	function viewImpl($id = null) {
		$this->pageTitle = "Benutzer";
		if (!$id) {
			$this->Session->setFlash(__('Invalid User.'));
			$this->redirect(array('action'=>'index'));
		}
		$user = $this->User->read(null, $id);
// 		debug($user);
		$this->set('user', $user);
/*
		$groups = $this->User->Admingroup->getGroups($id, true);
		$this->set('groups', $groups);
		$grouplist = $this->User->Admingroup->find('list');
		$this->set('grouplist', $grouplist);
		
		$contract = $this->Contracts->find('first', array('conditions' => '', array('order' => 'valid_till desc')));
		$this->set('contract', $contract);
*/		

	}

	function add($email = null) {
    	//echo($this->Auth->loggedIn());
		$this->pageTitle = "Benutzer hinzufügen";
		if (!empty($this->request->data)) {
			//debug($this->request->data);
			$this->User->create();
			//$this->Auth->hashPasswords($this->request->data);
			if(!empty($this->request->data['User']['password'])) $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
			$this->request->data["User"]["username"] = $this->request->data["User"]["email"];

			
			//pr($this->request->data);			
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The User has been saved'));
// 				debug($this->request->data);
// 		    debug($this->User->id);
				$this->sendActivationEmail($this->User->id);
				$this->redirect(array('controller' => 'Participations', 'action' => 'add', $this->User->id));
			} else {
				$this->request->data ['User']['password'] = "";
				//debug(__('The User could not be saved. Please, try again.'));
				$this->Session->setFlash(__('The User could not be saved. Please, try again.'));
			}
		}
		else {
		      $this->request->data["User"]["email"] = $email;
		}
	}
	
	function userFormDatenAufuellen($data){
		
	}


	function sendActivationEmail($user_id) {
		$user = $this->User->find('first',array('conditions' => array('User.id' => $user_id)));
		//debug($user);
		if ($user === false) {
				debug(__METHOD__." failed to retrieve User data for user.id: {$user_id}");
				return false;
		}
		$mailTitle = Configure::read("mail.register.title");
		$this->sendHashMail($user, "activate", 'register', $mailTitle);	
	}
		
	function sendHashMail($user, $action, $mailname, $mailsubject){

		// Set data for the "view" of the Email
        $this->User->id = $user['User']['id'];
        //echo $this->User->getActivationHash();
		
		$link = Router::url("/".$this->name."/$action/".$user['User']['id']."/".$this->User->getActivationHash(), false);
		//echo(" $link   - ");
		$link = 'http://'. env('SERVER_NAME').$link;

		$this->set('activate_url', $link );
		$this->set('username', $user['User']['username']);
		
		
		/**
		$this->SwiftMailer->to	= $user['User']['email'];	
		$this->SwiftMailer->from = 'noreply@' . env('SERVER_NAME');
		//pr($this->SwiftMailer);
		if (!$this->SwiftMailer->send($mailname, $mailsubject)) {
			 $this->log('Error sending email "adduser".', LOG_ERROR);
		} 
		**/
		
		$email = new CakeEmail('default');
		$email->viewVars(array('activate_url' => $link, 'username' => $user['User']['username']));
		$email->template($mailname)->emailFormat('html');
		$from = Configure::read("mail.from");
		$email->from($from);
		$email->to($user['User']['email']);
		$email->subject($mailsubject);
		$email->send();

	}

	function noaccess() {
		if(! empty($this->request->data)){
			$user = $this->User->find('first', array('conditions' => array('User.email' => $this->request->data['User']['email']), 'fields' => array('User.email', 'User.username','User.id')));
			if (empty($user)) {
				return false;			
			}
			//pr($user);
			$this->sendHashMail($user, "activate", 'register',"Klingtgut - Reset Passwort");	
			//$this->sendHashMail($user, "resetpassword", "reset", "Hier kannst du deine Passwort neu setzen");	
			$this->redirect(array('action' =>"yes"));
		}		
	}

	function activate($user_id = null, $in_hash = null) {
		$this->Auth->logout();
		$this->User->id = $user_id;
		if ($this->User->exists() && ($in_hash == $this->User->getActivationHash())){
			// Update the active flag in the database
			$this->User->saveField('active', 1);
			// Let the user know they can now log in!
			//$this->Session->setFlash('Your account has been activated, please log in');
			$this->request->data = $this->User->read(null, $user_id);
			$result = $this->Auth->login($this->request->data['User']);
			$this->redirect(array('action' =>"loggedin"));
		}
		// Activation failed, render ‘/views/user/activate.ctp’ which should tell the user.
	}

	function resetpassword($user_id = null, $in_hash = null) {
		$this->User->id = $user_id;
	
		//echo("exists : ".$this->User->exists()."   ,  ".$in_hash."  ==  ".$this->User->getActivationHash());
	
		if ($this->User->exists() && ($in_hash == $this->User->getActivationHash())){
			//echo(" Passwort wird verändert ");
			$user = $this->User->read(null, $user_id);
			$this->set('username', $user['User']['username']);
			$this->set('user_id', $user_id);
			$this->set('in_hash', $in_hash);
				if(! empty($this->request->data)){
					$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
					//pr($this->request->data);
					if($this->User->save($this->request->data)){
						$this->redirect(array('action' =>"yes"));
					}
				}	
		}else{
			//echo(" $in_hash == {$this->User->getActivationHash()}" );
			$this->redirect(array('action' => "no"));
		} 
		// Activation failed, render ‘/views/user/activate.ctp’ which should tell the user.
	}
	
	
	function changeSL() {
		$this->User->id = $this->Auth->user('id');
		if ($this->User->exists()){
			$user = $this->User->read(null, $this->User->id);
			$this->set('username', $user['User']['username']);
				if(! empty($this->request->data)){
					//$this->request->data['User']['id']
					$this->request->data['User']['sp'] = $this->Auth->password($this->request->data['User']['sp']);
					if($this->User->save($this->request->data)){
						$this->redirect(array('action' =>"yes"));
					}
				}	
		}else{
			//echo(" $in_hash == {$this->User->getActivationHash()}" );
			$this->redirect(array('action' => "no"));
		} 
		// Activation failed, render ‘/views/user/activate.ctp’ which should tell the user.
	}
	
	function test(){
		$this->view = 'yes';
		$this->User->create();
		$this->request->data["User"]["name"] = 'acht';

		pr($this->request->data);
		if ($this->User->save($this->request->data, false)) {
			$this->Session->setFlash(__('The User has been saved'));
			echo("yeahr");
		} else {
			echo("no");
			$this->request->data ['User']['password'] = "";
		}
		
	}


	function edit($id = null) {
		if (!empty($this->request->data)) {
			if(!empty($this->request->data['User']['password'])) $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);

			$this->request->data["User"]["username"] = $this->request->data["User"]["email"];
			$this->User->create();
			$this->request->data['User']['active'] = 1;
			if(isset($this->request->data['User']['anreise'])){ 
				$this->request->data['User']['anreise']['year'] = date('Y', strtotime(Configure::read("klingtgut.start")));
			}
			if(isset($this->request->data['User']['abreise'])){ 
				$this->request->data['User']['abreise']['year'] = date('Y', strtotime(Configure::read("klingtgut.start")));
			}

			//debug($this->request->data);
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The User has been saved'));
				$this->redirect('/pages/yes');
			} else {
				$this->Session->setFlash(__('The User could not be saved. Please, try again.'));
			}
			
		} else {
			$user['User'] = $this->Auth->user();
			if($this->isCox && !empty($id)){}
			else{ 
				$id = $user['User']['id']; 				
			}					
			$this->request->data = $this->User->read(null, $id);
			//debug($this->request->data);
			unset($this->request->data['User']['password']); // = NULL;
		} 
		$this->set('hash',$this->User->getActivationHash());
		
	}
	
	function willkommen(){
		if (!empty($this->request->data)) {
			$email = $this->request->data["User"]["email"];
			$arr = array('conditions' => array('User.email' => $email));
			$user = $this->User->find('first', $arr);
			if(!empty($user)) {
				$this->redirect("/Users/login/$email");
			}
			else {
				$this->redirect("/Users/add/$email");
			}

		}
	}
	
	function save(){
		pr($this->request->data);
		$this->User->id = $this->request->data['User']['id'];
		$this->User->create();
		$erg = $this->User->save($this->request->data);
		echo($erg);
		if ($erg) {
			echo('haha');
			echo('hehe');
			$this->redirect('/pages/yes');
		} else {
			echo('buhhh');
		}
		
	}
	
	
	function editName($id = null) {
		$this->checkPermission('admin');
		//pr($this->request->action);
		if (!empty($this->request->data)) {
			pr($this->request->data);
			$this->User->save($this->request->data);
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->User->read(null, $id);
			$this->set("username", $this->request->data['User']['username']);
		}
	}
	
	function changeEmail($result = null){
		$id = $this->Auth->user('id');
		if(!empty($result)) $this->set('result', $result);
		if(! empty($this->request->data)){
			$user = $this->User->read(null, $id);
			$user['User']['active'] = 0;
			$user['User']['email'] = $this->request->data['User']['email'];
			$erg = $this->User->save($user);
			//debug($erg);

			$this->sendActivationEmail($id);

            $this->set("result", "Die Emailadresse <i>".$this->request->data['User']['email']."</i> wurde gespeichert und es wurde eine Aktivierungsemail an die Adresse geschickt.<br>" .
                "Jetzt musst du nur noch dein emailadresse bestätigen.<br>".
                "Eine entsprechende email sollte in den nächsten minuten bei dir eintreffen..<br>".
                "Danach kannst du dich wieder normal einloggen.");
        }
		$this->request->data = $this->User->read(null, $id);
		
	}


	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for User'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action'=>'index'));
		}
	}

	// static pages included because of reusability of the programming design
	function notactivated(){
		$this->set('id', $this->Auth->user('id'));
	}

	function registersuccess(){}

	function yes(){}

	function loggedin(){}


	function no(){}
	
	function debug($level){
		$this->Session->write('debug', $level);
		$this->redirect($this->referer());
	}


}
?>
