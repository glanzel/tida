<?php
class DecisionsController extends AppController {

	var $name = 'Decisions';
	var $uses = array('Decision','Admingroup','WorkingHour', 'AppointmentsDecision');
	var $paginate = array(
		'limit' => 25,
		'order' => array(
			'stichtag' => 'desc'
		)
	);
	public static $snaviactions = array(
		array('title' => '+Diskussion',  'action' => "add"),
		array('title' => '+Vorschlag',  'controller' => 'Proposals', 'action' => "create"),
		array('title' => 'Vorschläge', 'action' => "index"),
		array('title' => 'Beschlüsse', 'action' => "archiv"),
		array('title' => 'erledigte Beschlüsse','action' => "archiv/fertig:1"));
	
	function beforeFilter(){
		$this->naviactions = DecisionsController::$snaviactions;
		parent::beforeFilter();
	}	
	
	function migrateToKern(){
		$decisions = $this->Decision->find('all');
		foreach($decisions as $decision){
			if($decision['Decision']['admingroup_id'] == 8) $decision['Decision']['admingroup_id'] = 10;
			else if($decision['Decision']['admingroup_id'] == 7) $decision['Decision']['admingroup_id'] = 9;
			if($decision['Decision']['stimmberechtigt'] == 8) $decision['Decision']['stimmberechtigt'] = 10;
			else if($decision['Decision']['stimmberechtigt'] == 7) $decision['Decision']['stimmberechtigt'] = 9;

			$this->Decision->create();
			$this->Decision->save($decision);
		}
	}	

	function migrateToVotegroup(){
		$decisions = $this->Decision->find('all');
		foreach($decisions as $decision){
			$this->Decision->Votegroup->create();
			$data['Votegroup']['id'] = $decision['Decision']['id'];
			$data['Votegroup']['decision_id'] = $decision['Decision']['id'];
			$data['Votegroup']['lfdNr'] = 1;
			$data['Votegroup']['choosen_proposal'] = $decision['Decision']['proposal_id'];
			$this->Decision->Votegroup->save($data);
		}
	}	

	function suche($action = 'index'){
		if(! empty( $this->request->data['Decision']['suche']))
			$this->redirect(array('action' => $action, 'suche' => $this->request->data['Decision']['suche']));
		else $this->redirect(array('action' => $action)); 
	}
	
	function solute($id, $value = 2) {
		$kptnTodo = $this->Decision->read(null,$id);
		$this->Decision->save(array('Decision' => array('id' => $id, 'solutions' => $value)));
		$this->redirect($this->Session->read('lastUrl'));
	}


	function index() {
		$this->Session->write('lastUrl', "/".$this->request->url);
		$options = array(
			'Decision.admingroup_id' => $this->Admingroup->getGroups($this->Auth->user('id')));
		
		
		if(! empty( $this->request->named['suche'])) $suche = $this->request->named['suche'];
		if(! empty( $suche)){
			$options['Decision.title like'] = '%'.$suche.'%';  //nur die ohne lösung anzeigen.
		}else {
			$options['Decision.solutions <'] = '2';  //nur die ohne lösung anzeigen.
		}
		
		//$options = array('Votegroup.choosen_proposal' => 'IS NULL');
		if(array_key_exists('typ', $this->passedArgs)) $options['KptnTodocat.title'] = $this->passedArgs['typ'];
		
		$this->pageTitle = "Offene Entscheidungen";
		//$this->Decision->recursive = 0;
		$this->Decision->bindModel(array('hasMany' => array('Vote' => array('conditions' => array('user_id' => $this->Auth->user('id'))))),false);
		$decisions = $this->paginate('Decision', $options);
		$this->Decision->unbindModel(array('hasMany' => array('Vote')),false);
		$decisions = $this->Decision->calcDecisionVotes($decisions);
		//$decisions = $this->paginate('Decision', $options);

		$this->set('decisions', $decisions);
		//pr($decisions);
		
	}
	
	function archiv($typ = 'alle') {
		$this->Session->write('lastUrl', "/".$this->request->url);
		
		$options = array('Decision.admingroup_id' => $this->Admingroup->getGroups($this->Auth->user('id')));

		if(! empty( $this->request->named['suche'])){
			$options['Decision.title like'] = '%'.$this->request->named['suche'].'%';  //alle anzeigen
		}else {
			$options['Decision.solutions >'] = '0';  //nur die mit lösung(en) anzeigen.
		}


		if(array_key_exists('typ', $this->passedArgs)) $options['KptnTodocat.title'] = $this->passedArgs['typ'];
		else $options['KptnTodocat.id !='] = '100';
		if(array_key_exists('fertig', $this->passedArgs)){
			if($this->passedArgs['fertig'] == 0) $options['Decision.umgesetzt_von'] = 0;
			else $options['Decision.umgesetzt_von !='] = 0;
		}
		//if($typ != 'alle') $options['KptnTodocat.title'] = $typ;
		//pr($options);
		$this->pageTitle = "Getroffenen Entscheidungen";
		$this->paginate['Decision'] = array('contain' => array('KptnTodocat', 'Votegroup' => array('Solution')), 
			'order' => array('stichtag' => 'desc'));

		//$this->Decision->contain(array('Votegroup' => array('Proposal')));
		$decisions =  $this->paginate('Decision',$options); //$this->Decision->archiv(); //
		$this->set('decisions', $this->paginate('Decision',$options));
		
		$kptnTodocats = $this->Decision->KptnTodocat->find('list');
		//$kptnTodocats[0] = 'alle';
		$this->set('typen',$kptnTodocats);
		
		//pr($kptnTodocats);
	}

	function view($id = null) {
		
		$this->Session->write('lastUrl', "/".$this->request->url);
		$this->pageTitle = "Diskussion / Abstimmung";

		if (!$id) $this->flash(__('Invalid decision'), array('action' => 'index'));
		$decision = $this->Decision->read(null, $id);
		//if($decision['Decision']['proposal_id'] != 0) $this->redirect(array("action" => "result/$id"));
		//pr($decision);
		
		$this->set('decision', $decision);
		//$this->Decision->Votegroup->recursive = 3;
		//$this->Decision->Votegroup->unbindModel(array('belongsTo' => array('Decision', 'Proposal')));
		//$proposals = $this->Decision->Votegroup->find('all', array('conditions' => array('decision_id' => $id)));		
		
		
		foreach($decision['Votegroup'] as $key => $group){
			$grouplist[$group['id']] = $group['lfdNr'];  
			
			$this->Decision->Votegroup->Proposal->recursive = 2;
			//$this->Decision->Votegroup->Proposal->DecisionComment->recursive = 2;
			//$this->Decision->Votegroup->Proposal->unbindModel(array('belongsTo' => array('Votegroup')));
			$this->Decision->Votegroup->Proposal->unbindModel(array('belongsTo' => array('Votegroup')));
			$this->Decision->Votegroup->Proposal->unbindModel(array('hasOne' => array('Votegroup')));
			$proposals[$group['lfdNr']]['proposals'] = $this->Decision->Votegroup->Proposal->find('all', array('conditions' => array('votegroup_id' => $group['id'])));		
			$proposals[$group['lfdNr']]['id'] = $group['id'];		
	
		}
		//pr($proposals);
		$this->set('grouplist', $grouplist);
		$this->set('votegroups', $proposals);
		//debug($decision['Votegroup']);
		
		$votecats = $this->Decision->Votegroup->Proposal->Vote->Votecat->find('list');		
		$this->set('votecats', $votecats);
		
		$opt['conditions']['user_id'] = $this->Auth->user('id');
		$opt['conditions']['decision_id'] = $id;
		$opt['fields'] = array('Vote.proposal_id', 'Vote.votecat_id');
		$votes = $this->Decision->Votegroup->Proposal->Vote->find('list', $opt);		
		$this->set('votes', $votes);
		
		$this->set('empty', empty($proposals[1]['proposals']));
		$this->set('stimmberechtigt', in_array($decision['Decision']['stimmberechtigt'],$this->Admingroup->getGroups($this->Auth->user('id'))));

		$options['von >'] =  date('Y-m-d H:i', strtotime(date('Y-m-d H:i'))-60*60*24*14);
		$options['kategorie <> 17 AND kategorie <>'] = 18;
		$applist = $this->Decision->Appointment->find('list', array('conditions' => $options, 'order' => 'von asc'));
		$this->set('applist', $applist);

	}
	function saveCat(){
		foreach($this->request->data['Decision'] as $mail_id => $kptncat){
			$data = $this->Decision->read(null,$mail_id);
			$this->Decision->create();
			$data['Decision']['kptnTodocat_id'] = $kptncat;
			$this->Decision->save($data);
		}
		$this->redirect($this->Session->read('lastUrl'));
		$this->redirect(array('action' => 'index'));
		//$this->redirect(array('action' => "view/$id"));
	}
	
	function addGroup($id){
		$this->Decision->addGroup($id);
		$this->redirect(array('action' => 'view', $id));		
	}

	function delGroup($id, $decision_id){
		$this->Decision->Votegroup->delete($id);
		$this->redirect(array('action' => 'view', $decision_id));		
	}

	function result($id = null, $gewichted = true) {
		$this->pageTitle = "Abstimmungsergebniss";
		if (!$id) {
			$this->flash(__('Invalid decision'), array('action' => 'index'));
		}
		$decision = $this->Decision->read(null, $id);

		// Nur Leitende sollen die Gewichtung sehen können ACHTUNG hardcodiert
		if($decision['Decision']['admingroup_id'] > 10 ) $gewichted = false;

		$this->set('decision', $decision);
		
		$admingroups = $this->Decision->Admingroup->find('first', array('conditions' => array('id' => $decision['Decision']['stimmberechtigt'])));		
		$users = $admingroups['User'];
		foreach($users as $user){ 
			$userlist[strtolower($user['name'])] = $user;
			$usernames[] = strtolower($user['name']);
		}
		
		$votes = $this->Decision->result($id, $users);
		//debug($votes);				
		// Optional Part Gewichtung 
		// TODO: if 
		if(Configure::read('Decision.gewichten') && $gewichted){
			$thearray = $this->WorkingHour->getSummenMatrix($usernames,$userlist);
			$userlist = $thearray['userlist'];
			$faktorTitel = $thearray['faktorTitel'];
			$alleAnteile = $thearray['alleAnteile'];
			
	
			foreach($userlist as & $user){
				$user['gewichtung'] = round($user['gesamtanteile']*100 / $alleAnteile);
			}
			
			
			$this->set('users', $userlist);
		}
		// Optional Part Gewichtung ende

		$this->set('votes', $votes);
		
		$resultSummen = $this->Decision->resultSumme($id);
		//debug($resultSummen);
		$this->set('voteSummen', $resultSummen);
	
		$sumVotes = $this->Decision->sumVotes($id);
		//$sumProposal = $this->Decision->sumProposals($id);
		//$sumVoter = $this->Decision->sumVoter($id);
		//$posVotes = $sumVoter * $sumProposal;
		$posVotes = $this->Decision->sumVoter($id) * $this->Decision->sumProposals($id);
		
	
		//debug($decision);
		//debug($resultSummen);

		//echo("resultStimmen: ".$resultSummen[0][0]['stimmen']." users ".count($users)." stichtag : ".$desicion['Decision']['stichtag']." stichtag : ".date('Y-m-d')." ist kleiner gleich ".($desicion['Decision']['stichtag'] <= date('Y-m-d')));

		// wenn alle abgestimmt haben oder der stichtag vorbei ist
		if( $posVotes  == $sumVotes || $decision['Decision']['stichtag'] <= date('Y-m-d')){
			//bestes ergebniss herausfinden
			$solutions = 0;
			foreach($decision['Votegroup'] as $key => $votegroup){
				
				$ausgewaehlt[0]['pkt'] = 0;
				$ausgewaehlt[0]['aufschiebend'] = 0;
				foreach($resultSummen as $resultSumme){
					// result die zu anderen votegroups gehören ignorieren 
					if($resultSumme['Proposal']['votegroup_id'] != $votegroup['id']) continue;
					if($resultSumme[0]['pkt'] == $ausgewaehlt[0]['pkt'])	$ausgewaehlt[0]['aufschiebend'] = 1;
					if($resultSumme[0]['pkt'] > $ausgewaehlt[0]['pkt'])     $ausgewaehlt = $resultSumme;
				}	
				//pr($ausgewaehlt);

				//wenn kein veto gegen das beste ergebnss besteht oder zwei besten ergebnisse existieren 
				if($ausgewaehlt[0]['aufschiebend'] < 1 && $ausgewaehlt[0]['pkt'] > 0){
					$solutions += 1;
					$this->Decision->Votegroup->create();	
					$data['Votegroup']['id'] = $votegroup['id'];
					$data['Votegroup']['choosen_proposal'] = $ausgewaehlt['Proposal']['id'];
					$this->Decision->Votegroup->save($data);	
					echo("ausgewählter vorschlag wurde automatisch gespeichert ! ");
				}else{
					echo("es wurde kein bester vorschlag gefunden. es muss neu gewählt werden");
				}
			}
			if($solutions == count($decision['Votegroup'])) $decision['Decision']['solutions'] = 2; // alle abstimmungen entschieden 
			else if($solutions > 0) $decision['Decision']['solutions'] = 1; // einige aber nicht alle Abstimmungen entschieden
			else  $decision['Decision']['solutions'] = 0; // keine Abstimmung entschieden
			$this->Decision->create();
			$this->Decision->save($decision);
			
			
		}
		
	}
	function multiadd($appointment_id = null){
		//debug($this->request->data);

		$options['conditions']['von >'] =  date('Y-m-d H:i', strtotime(date('Y-m-d H:i'))-60*60*24*14);
		//$options['fields'] = array('id', 'title', 'von');
		$applist = $this->Decision->Appointment->find('list', $options);
		$this->set('applist', $applist);

		$kptnTodocats = $this->Decision->KptnTodocat->find('list');
		$admingroups = $this->Admingroup->find('list');
		$this->set(compact('admingroups','kptnTodocats','appointment_id'));
		
		if (!empty($this->request->data)) {
		
			$all = $this->request->data['all'];
			unset($this->request->data['all']);
			foreach($this->request->data as $key => $todo){
				if($todo['Decision']['title'] == "") break;
				$todo['Decision']['appointment_id'] = $all['Decision']['appointment_id'];
				//debug($todo);
				$this->KptnTodo->create();
				$this->saveImpl($todo);
			}
			$this->redirect($this->Session->read('lastUrl'));
			$this->redirect(array('action'=>'index'));
		}
	
	}


	function multivote($id = null) {
		$this->pageTitle = "Abstimmen";
		if (!$id) $this->flash(__('Invalid decision'), array('action' => 'index'));
		$desicion = $this->Decision->read(null, $id);
		$this->set('decision', $desicion);
			
		$admingroups = $this->Decision->Admingroup->find('first', array('conditions' => array('id' => $desicion['Decision']['stimmberechtigt'])));		
		$users = $admingroups['User'];
		$this->set('users', $users);
		
		$votes = $this->Decision->result($id, $users);
		$this->set('votes', $votes);
		
		$resultSummen = $this->Decision->resultSumme($id);
		$this->set('voteSummen', $resultSummen);
	
		$votecats = $this->Decision->Votegroup->Proposal->Vote->Votecat->find('list');
		$this->set('votecats', $votecats);
	
		if(!empty($this->request->data)){
			
			foreach($this->request->data['Multivote'] as $user_id => $proposal_ids){
				foreach($proposal_ids as $proposal_id => $votecat_id){
					$this->Decision->Votegroup->Proposal->Vote->create();
	
					//gucken ob wahl bereits existert
					$cond['proposal_id'] = $proposal_id;
					$cond['user_id'] = $user_id;
					$this->Decision->Votegroup->Proposal->Vote->recursive = -1;
					$data = $this->Decision->Votegroup->Proposal->Vote->find('first', array('conditions' => $cond));
					
					// wenn diese wahl nicht neu ist belassen
					if($data['Vote']['votecat_id'] == $votecat_id) continue;
					
					// mit aktuellen werten überschreiben
					$data['Vote']['proposal_id'] = $proposal_id;					
					$data['Vote']['user_id'] = $user_id;					
					$data['Vote']['decision_id'] = $id;
					$data['Vote']['votecat_id'] = $votecat_id;
					$data['Vote']['erstellt'] = date("Y-m-d");
					$data['Vote']['erstellt_von'] = $this->Auth->user('name');
					//pr($data);

					$this->Decision->Votegroup->Proposal->Vote->save($data);
					
				}
			}
			//pr($this->request->data);
			$this->redirect(array("action" => "result/$id"));
		}
	
	}

	function add() {
		$this->pageTitle = "Diskussionsthema hinzufügen";
		$this->addImpl();
	}
	
	function addImpl() {
		$kptnTodocats = $this->Decision->KptnTodocat->find('list');
		$admingroups = $this->Admingroup->find('list');
		$this->set(compact('admingroups','kptnTodocats'));
	}

	function save(){
		if (!empty($this->request->data)) {
			//pr($this->request->data);
			if(! $this->saveImpl($this->request->data)) $this->flash(sprintf(__('Konnte nicht gespeichert werden')), array('action' => 'index'));
			//pr($this->request->data);
			$this->redirect(array('action' => 'index'));
		}
		
	}
	
	function saveImpl($data){
		$this->Decision->create();
		$data['Decision']['erstellt'] = date('Y-m-d');
		$data['Decision']['stichtag'] = date('Y-m-d', time() + 3600*24*$data['Decision']['laufzeit']);
		debug($data['Decision']['stichtag']);
		$data['Decision']['user_id'] = $this->Auth->user('id');
		
		//wenn nicht anderes angegeben ist ->alle können dies sehen
		if(!isset($data['Decision']['admingroup_id']) && !$this->isCox) 
			$data['Decision']['admingroup_id'] = 16; 
		else if(!isset($data['Decision']['admingroup_id']) && $this->isCox) 
			$data['Decision']['admingroup_id'] = 7; 

		//wenn nicht anderes angegeben ist ->leitendeOrga können dies editieren 
		if(!isset($data['Decision']['stimmberechtigt'])) $data['Decision']['stimmberechtigt'] = 7;
		
		if ($this->Decision->save($data)) {
			$this->Decision->addGroup($this->Decision->id);
			if(!empty($data['Decision']['appointment_id'])){
				$appointment_id = $data['Decision']['appointment_id'];
				$au = array('decision_id' => $this->Decision->id, 'appointment_id' => $appointment_id);
				$this->AppointmentsDecision->create();
				$this->AppointmentsDecision->save($au);
			}		
			return true;
		} else {
			return false;
		}
		
		
		
		
	}

	function edit($id = null) {
		$this->pageTitle = "Diskussionsthema bearbeiten";
		if (!$id && empty($this->request->data)) {
			$this->flash(sprintf(__('Invalid decision')), array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Decision->save($this->request->data)) {
				//pr($this->request->data);
				//$this->flash(__('The decision has been saved.'), array('action' => 'index'));
				$this->redirect($this->Session->read('lastUrl'));
				//$this->redirect(array('action' => 'view', $id));
			} else {
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Decision->read(null, $id);
		}
		$kptnTodocats = $this->Decision->KptnTodocat->find('list');
		$admingroups = $this->Admingroup->find('list');
		$this->set(compact('admingroups','kptnTodocats'));
		
	}
	function stichtagPlus($id,$tage){
		$data = $this->Decision->read(null,$id);
		$this->Decision->create();
		$data['Decision']['stichtag'] = date('Y-m-d', strtotime($data['Decision']['stichtag'])+3600*24*$tage);
		$data['Decision']['bedenkzeit'] = $data['Decision']['bedenkzeit']+1;
		$this->Decision->save($data);
		pr($data);
		$this->redirect(array('action' => "view/$id"));
	}
	
	function done($id) {
		$this->Decision->save(array('Decision' => array('id' => $id, 'umgesetzt_am' => date('y-m-d'), 'umgesetzt_von' => $this->Auth->user('name'))));
		$this->redirect(array('action'=>'archiv'));
	}
	
	function papierkorb($id){
		$data = $this->Decision->read(null,$id);
		$this->Decision->create();
		$data['Decision']['kptnTodocat_id'] = 100;
		$this->Decision->save($data);
		$this->redirect($this->Session->read('lastUrl'));
		//$this->redirect(array('action' => "view/$id"));
	}

	function delete($id = null) {
		if (!$id) {
			$this->flash(sprintf(__('Invalid decision')), array('action' => 'index'));
		}
		if ($this->Decision->delete($id)) {
			$this->flash(__('Decision deleted'), array('action' => 'index'));
		}
		$this->flash(__('Decision was not deleted'), array('action' => 'index'));
		$this->redirect(array('action' => 'index'));
	}
}
?>