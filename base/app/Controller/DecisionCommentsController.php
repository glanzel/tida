<?php
class DecisionCommentsController extends AppController {

	var $name = 'DecisionComments';


	function add($decision_id = null, $proposal_id = null) {
		if($decision_id){
			$this->request->data['DecisionComment']['decision_id'] = $decision_id;		
			$this->request->data['DecisionComment']['proposal_id'] = $proposal_id;		
		}else if (!empty($this->request->data)) {
			$this->request->data['DecisionComment']['user_id'] = $this->Auth->user('id');
			pr($this->request->data);
			
			$this->DecisionComment->create();
			if ($this->DecisionComment->save($this->request->data)) {
				$this->Session->setFlash(__('The decision comment has been saved'));
				$this->redirect(array('controller' =>'Decisions', 'action' => 'view', $this->request->data['DecisionComment']['decision_id']));
			} else {
				$this->Session->setFlash(__('The decision comment could not be saved. Please, try again.'));
			}
		}else{
			echo "Fehler, Aktion wurde ohne decison_id aufgerufen";
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid decision comment'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->DecisionComment->save($this->request->data)) {
				$this->Session->setFlash(__('The decision comment has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The decision comment could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->DecisionComment->read(null, $id);
		}
		$decisions = $this->DecisionComment->Decision->find('list');
		$proposals = $this->DecisionComment->Proposal->find('list');
		$users = $this->DecisionComment->User->find('list');
		$this->set(compact('decisions', 'proposals', 'users'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for decision comment'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->DecisionComment->delete($id)) {
			$this->Session->setFlash(__('Decision comment deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Decision comment was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
?>