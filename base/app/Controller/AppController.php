<?php

class AppController extends Controller {
	var $components = array('Auth', 'Session', 'Cookie', 'Flash');
	var $helpers = array('Session','Form', 'Html', 'Js', 'Time');
	var $uses = array('User','Admingroup','AdmingroupsUser','Decision','KptnTodo','Appointment', 'Doodle', 'Participation');
	public $isCox = false;
	public $isCrew = false;
	public $isMobile = false;
	var $naviactions = array();
	var $cpath;
	public $zusage = null;
	
	function beforeFilter(){
		$this->layout = 'kptn';
		$debug = $this->Session->read('debug');
		
		if(!empty($debug)){
			$debug_level = $this->Session->read('debug');
			Configure::write('debug', $debug_level);
		}else{
			$default_debug = Configure::read('default_debug');
			Configure::write('debug', $default_debug);
		} 

		//debug($this->Auth->user());
		
		
		if($this->Auth->user('id')){ 
			
			$this->set('username', $this->Auth->user('username'));
			$this->set('user_id', $this->Auth->user('id'));
			$this->set('shortname', $this->Auth->user('name'));
			$this->isCox = $this->isKptnCox($this->Auth->user('username'));
			$this->isCrew = $this->isCrew($this->Auth->user('username'));
			$this->set('cox', $this->isCox);
			$this->set('superuser', $this->isAdmin($this->Auth->user('username')));
			$this->set('crew', $this->isCrew);
			//debug($this->isCrew);
		
			$this->groups = $this->Session->read('mygroups');
			if(empty($this->groups)){
				//$this->groups = $this->Admingroup->getGroups($this->Auth->user('id'));
				$this->Session->write('mygroups', $this->groups);
			}
		
			$this->set('groups', $this->groups); 
			
			if($this->Auth->user('active') == 0){
				if(! ($this->request->params['action'] == 'logout' || $this->request->params['action'] == 'changeEmail' || $this->request->params['action'] == 'activate')){ 
					$this->redirect(array('controller' => 'Users', 'action' => 'changeEmail'));
				}
			}		
		}else{
			$this->set('cox', false);
			$this->set('username', null);
		}
		
			$teilnahme = $this->Participation->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'), 'year' => Configure::read("klingtgut.year_id"))));
			if(!empty($teilnahme)) $this->zusage = $teilnahme['Participation']['zusage'];
			$this->set('zusage', $this->zusage);
				
		//$this->groupAllow(array(16),'/pages/home');	
		$this->set('naviactions', $this->naviactions);
		
		
	}
	function isAuthorized() {
		return true;
	}
	
	
	function testPermission($controller, $action, $user_id){
		//$user_id = $this->Auth->user('id');
		//$controller = $this->request->params['controller'];
		//$action = $this->request->params['action'];
		$tuser = $this->User->find('first', array('conditions' => array('id' => $user_id)));
		echo($tuser['User']['name']."<br>");
		$group_arr = $this->Admingroup->getGroups($user_id);
		$allowed = $this->hasPermission($controller, $action, $group_arr);
		echo " Allowed: --$allowed-- <br>";
	}

	function checkPermission($notallowedredirect = '/pages/notallowed'){
		$user_id = $this->Auth->user('id');
		$controller = $this->name;
		$action = $this->request->params['action'];
		$action{0} = strtolower($action{0});
		$group_arr = $this->Admingroup->getGroups($user_id);
		$allowed = $this->hasPermission($controller, $action, $group_arr);	
		debug($controller.$action);
		if(! $allowed) $this->redirect($notallowedredirect);
	}

	function hasPermission($controller, $action, $groups_arr){
		$allowed = true;
		$acl = AppController::$acl;
		
		if(isset($acl[$controller])){
			//debug(" acl[$controller] <br>");
			if(isset($acl[$controller][$action])){
				debug(" acl[$controller][$action] <br>");
				if(is_array($acl[$controller][$action])){
					if(isset($acl[$controller][$action]['all'])){
						$allowed = in_array($acl[$controller][$action]['all'], $groups_arr);
					}
					if(isset($acl[$controller][$action]['my'])){
						if($this->checkIfMy()){
							$allowed = in_array($acl[$controller][$action]['my'], $groups_arr);
						}
					}				
					$allowed = $this->andGroups($allowed,$acl, $controller, $action, $groups_arr);
					
				}else{
					$allowed = in_array($acl[$controller][$action], $groups_arr);
				}
			}else if(isset($acl[$controller]['all'])){
				$allowed = in_array($acl[$controller]['all'], $groups_arr);
				$allowed = $this->andGroups($allowed,$acl, $controller, 'all', $groups_arr);
		
			}else $allow = true;
		}else if(isset($acl['all'])){
			//debug ("acl[all]<br> ");
			$allowed = in_array($acl['all'], $groups_arr);
		}else {} debug("not specified<br>");
		
		//debug($allowed);
		
		return $allowed;
		
	}
	
	function andGroups($allowed, $acl, $controller, $action, $groups_arr){
		if(isset($acl[$controller][$action]['and'])){
			foreach($acl[$controller][$action]['and'] as $group) {
				$allowed = in_array($group, $groups_arr);
				debug("Group $group is $allowed");
				if($allowed) return $allowed;
			}
		}
		return $allowed;		
	}
	
	function checkIfMy(){
		$posId = $this->request->pass[0];
		$modelClass = $this->modelClass;
		$row = $this->$modelClass->read(null, $posId);
		if(! empty($row)){
			if(isset($row[$modelClass]['user_id']) && $row[$modelClass]['user_id'] == $this->Auth->user('id')) return true;
			else if(isset($row[$modelClass]['name']) && $row[$modelClass]['name'] == $this->Auth->user('name')) return true;
			else return false;
		} else return false;

	}

	function groupAllow($groupids, $notallowedredirect){
		if(in_array($this->request->params['action'], $this->Auth->allowedActions)) return;
		$allow = false;
		if($this->Auth->user('id')){
				foreach($groupids as $groupid){
					if(in_array($groupid, $this->groups)) $allow = true;			
				}
			if(! $allow) $this->redirect($notallowedredirect);
		}
	}
	
	function isKptnCox($username){
		$isCox = $this->Session->read("{$username}cox");
		
		$cox = $this->AdmingroupsUser->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'), 'admingroup_id' => 7)));
		if(empty($cox))  $this->Session->write("{$username}cox", 0);
		else $this->Session->write("{$username}cox", 1);
		return !empty($cox);
		
		//if($username == "glanzel" || $username == "kptn" || $username == "celine" || $username == "patrick" || $username == "mel") return true;
		//return false;
	}

	function isCrew($username){
		$isCrew = $this->Session->read("{$username}crew");
		if(is_null($isCrew)) {
			//debug('isNull');
			$crew = $this->AdmingroupsUser->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'), 'admingroup_id' => 16)));
			if(empty($crew))  $this->Session->write("{$username}crew", 0);
			else $this->Session->write("{$username}crew", 1);
			$isCrew = !empty($crew);
		}
		//debug($isCrew);
		return $isCrew;
	}
	
	function isAdmin($username){
		if($username == "glanzel@gmx.de" || $username == "glanzel") return true;
		return false;
	}
	
	function isSuper(){
		if($this->Auth->user('name') == "glanzel@gmx.de") return true;
		return false;
	}
	
			
	
		
}
?>
