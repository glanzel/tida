<?php
App::uses('CakeEmail', 'Network/Email');

class MailsController extends AppController {
	
	var $name = 'Mails';
	var $helpers = array('Html', 'Form');
	//var $components = array('SwiftMailer');
	var $uses = array('Mail', 'User','Subscriber','Apointments', 'Participation'); 

	public $logmails = null;

	
	function index(){
		$this->Session->write('lastUrl', "/".$this->request->url);
		$this->set('lastlogin', $this->Session->read('lastlogin'));
		$this->Session->delete('unreadMails');
		$this->User->query("UPDATE `users` SET `lastLogin`= '".date('Y-m-d H:i:s')."' WHERE id=".$this->Auth->user('id'));
		 
		if(array_key_exists('typ', $this->passedArgs)) $options['KptnTodocat.title'] = $this->passedArgs['typ'];
		else $options['kptn_todocat_id !='] = '100';
		
		$options['sichtbarkeit'] = $this->Admingroup->getGroups($this->Auth->user('id'));
		
		$this->Mail->order = 'datum desc';
		$mails = $this->paginate($options);
		$typen = $kptnTodocats = $this->KptnTodo->KptnTodocat->find('list');
		$this->set(compact('mails','kptnTodocats', 'typen'));
	}

	function view($id){
		$mail = $this->Mail->read(null,$id);
		$this->set(compact('mail'));
	}

/******************* send Mail Section ********************/	
	
	
	//TODO: In Participations gucken wegen zusage
	function sendwebmail(){
		$year = Configure::read("klingtgut.year_id");
		if(!empty($this->request->data)){
			$zusage = $this->request->data['Mail']['to']; //bischn verwirrend benannt TODO: to in zusagen umbenennen ?
			if($zusage == MICH_SELBST) $group = array($this->Auth->user('id') => $this->Auth->user('email')); // zum testen
			else $group = $this->Participation->find('list', array('conditions' => array('zusage' => $zusage, 'year' => $year),'fields' => array('User.id', 'User.email'), 'recursive' => 0)); 
			$data['Mail']['from'] = $data['Mail']['sender'] = $data['Mail']['replyTo'] = $this->Auth->user('email');
			//debug($group);	
			$this->sendwebmailImpl($this->request->data, $group);
		}
		$this->set('groups', array(UNSICHER => 'unsicher', WARTELISTE => 'warteliste', ZUSAGE => 'sicher', MICH_SELBST => 'mich'));
		//$this->redirect("/pages/yes"); // nicht weiterleiten weil sonst die ausgabe kaputt ist
	}
			
	function sendwebmailImpl($data = null, $group = null){
		$this->logmails = "";
		
		$data['Mail']['datum'] = date('y-m-d H:i:s');
		$data['Mail']['receiver'] = 'noreply'."@".Configure::read('Mail.domain');
		if(Configure::read('Mail.useFixedSender')){
			$data['Mail']['from'] = $data['Mail']['sender'] = Configure::read('Mail.fixedSender');
		}			
		
		if(! empty($data['Document']['file']['name'])){
			//pr($data);
			//$file['file']['content'] = $data['Document']['file']['tmp_name'];
			$decoded_filename = urldecode($data['Document']['file']['name']);
			$data['Mail']['attachment'] = $file['file']['name'] = $decoded_filename;
			$mkdirstr = WWW_ROOT."media/attachments/";
			mkdir($mkdirstr);
			$path = "$mkdirstr/$decoded_filename";
			move_uploaded_file($data['Document']['file']['tmp_name'], $path);
			$file['file']['path'] = $path;

			$this->processMail($group,$data, array($file));
					
		}else{
			//echo ('send without attachment');
			$this->processMail($group,$data);
		}
		echo("Mails an ");
		echo $this->logmails;
		echo(" verschickt.");
	}

	/*
	 * liest die emailadresse aus und speichert eine instanz von Mail
	 * gibt dann die daten zum senden an sendMail weiter
	 */
	function processMail($users, $data, $attachments = null, $template = false, $viewVars = false, $layout = 'default'){
	
		//$data['Mail']['sichtbarkeit'] = $sichtbarkeit;
	
		// TODO: nur schreiben wenn nötig
		$data['Mail']['content'] = str_replace("\n", "<br>", $data['Mail']['content']);
		$data['Mail']['type'] = 'text/html';
		$data['Mail']['cetype'] = 'html';
		$data['Mail']['lb'] = "<br>";
	
		//$this->Mail->create();
		//$this->Mail->save($data);
		$id = $this->Mail->id;
			
		$attachArr = NULL;
		if($attachments != null){
			//debug('rename attachment');
			$mkdirstr = WWW_ROOT."media/$id/";
			mkdir($mkdirstr);
			foreach($attachments as $attachment){
				$new_path = $mkdirstr.$attachment['file']['name'];
				rename($attachment['file']['path'], $new_path);
				$attachArr[] = $new_path;
			}
		}
			
		$stammcontent = $data['Mail']['content'];
			
			
		// ein email an alle schicken
			
		$counter = 0;
		$user_chunks = array_chunk($users, 98);
	
		//foreach($user_chunks as $user){		//wegen der blöden google beschränkung auf 100 empfänger pro email
		foreach($users as $key => $email){
			$counter++;
			$toArr[] = $email;
		}
		$data['Mail']['to'] 	= 'gutklingts@gmail.com';
		$data['Mail']['bcc'] 	= $toArr;
		$data['Mail']['content'] = 	$stammcontent."<br>".$this->genericloginLink();
		$this->logmails = implode(', ', $toArr);
		$this->sendMail($data, $attachArr, $template, $viewVars, $layout);
		//}
		//debug($data);
			
		/*
		 //eine email pro user
		 foreach($users as $userid => $email){
		 $this->logmails .= $email.", ";
	
		 $data['Mail']['to'] = $email;
	
		 $registrationLink = $this->loginLink($userid);
		 $registrationLink = "<a href=".$registrationLink."> Hier kannst du deine Daten bearbeiten </a>";
		 //debug("$email $userid $registrationLink " );
	
		 //$registrationLink .= "Hier könnte ihr eure anmeldeDaten bearbeiten";
		 	
		 $data['Mail']['content'] = 	$stammcontent."<br>".$registrationLink;
	
		 //debug($data['Mail']['content']);
	
		 $this->sendMail($data, $attachArr, $template, $viewVars, $layout);
		 }
		 */
	}
	
	
	// $email must be a CakeEmail Object;	
	function sendMail($mailArray, $attachments = null, $template = false, $viewVars = false, $layout = 'default'){
		//pr($mailArray);
		
		$email = new CakeEmail('default');
				
		$email->emailFormat($mailArray['Mail']['cetype']);
		$email->sender($mailArray['Mail']['sender']);
		$email->from(array($mailArray['Mail']['from']));
		$email->replyTo($mailArray['Mail']['replyTo']);
		
		// Mails werden jetzt alle einzeln verschickt
		$email->bcc($mailArray['Mail']['bcc']);
		//$email->to(array('kptn.a.mueller@gmail.com' => 'undisclosed recipient'));
		$email->to($mailArray['Mail']['to']);
				
		//$this->log($mailArray['Mail']['subject'],'mail');
		
		$email->subject($mailArray['Mail']['subject']);
		//$this->log($email,'mail');
		
		//$this->log($mailArray,'mail');
		//$this->log($email,'mail');
		
		//$this->log($mailArray['Mail']['content'],'mail');
	
		
		if(!empty($attachments)) $email->attachments($attachments);
		
		try{
			if($template){ 
				$email->template($template, $layout)->emailFormat('html');
				$email->viewVars($viewVars);
				$email->send();
			} else	$email->send($mailArray['Mail']['content']);
		}catch(Exception $e){
			$this->log ('Fehler beim defaultversand<br> versende per smtp<br> alles ok.', 'mail');
	
			$cemail = new CakeEmail('smtp');	
			$cemail->emailFormat($mailArray['Mail']['cetype']);
			$cemail->sender($mailArray['Mail']['sender']);
			$cemail->from(array($mailArray['Mail']['from']));
			$cemail->replyTo($mailArray['Mail']['replyTo']);
			$cemail->bcc($mailArray['Mail']['to']);
			$cemail->subject($mailArray['Mail']['subject']);
			//Nur nötig beim Versand per smtp
			//$mailArray['Mail']['content'] .= $mailArray['Mail']['lb'].$mailArray['Mail']['lb'].$mailArray['Mail']['sender'];
			$cemail->send($mailArray['Mail']['content']);
			//$this->log($email,'mail');
			
		}
	}

/**************** Login Section ****************************/	
	
	function loginLink($userid = null){
		$theuser = $this->User->read(null, $userid);
		$user = $theuser['User'];
		$link = Router::url("/Users/activate/".$user['id']."/".$this->User->getActivationHash($user['email']), false);
		$link = 'http://'. env('SERVER_NAME').$link;
		//debug ( $link );
		//debug($this->User->getActivationHash($user['email']));
		return $link;
	}
	
	function genericloginLink(){
		$link = Router::url("/Users/login/", false);
		$link = 'http://'. env('SERVER_NAME').$link;
		$link = "<a href=".$link."> Hier kannst du dich einloggen</a>";
		return $link;
	}
	
	
	function showLoginLink($id = null){
		$user = $this->User->read(null, $id);
		$this->loginLink($user['User']);
	}
	
/***************** Categorization Section **********************************/	
	
	function saveCat(){
		foreach($this->request->data['Mail'] as $mail_id => $kptncat){
			$data = $this->Mail->read(null,$mail_id);
			$this->Mail->create();
			$data['Mail']['kptn_todocat_id'] = $kptncat;
			$this->Mail->save($data);
		}
		$this->redirect($this->Session->read('lastUrl'));
		$this->redirect(array('action' => 'index'));
		//$this->redirect(array('action' => "view/$id"));
	}
	
	function papierkorb($id){
		$data = $this->Mail->read(null,$id);
		$this->Mail->create();
		$data['Mail']['kptn_todocat_id'] = 100;
		$this->Mail->save($data);
		$this->redirect(array('action' => 'index'));
		//$this->redirect(array('action' => "view/$id"));
	}
	

/****************** probably unused stuff Section ********************************/        
	
    function getMimeType($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
    }
        

        function file_post_contents($url, $postArray){
        	$jsondata = json_encode($postArray);
        
        	$opts = array('http' =>
        			array(
        					'method'  => 'POST',
        					'header'  => 'Content-type: application/json',
        					'content' => $jsondata
        			)
        	);
        
        	$context  = stream_context_create($opts);
        	return file_get_contents($url, false, $context);
        
        }
        
        
        function teststripMail(){
        	echo $this->stripMail("myName <nam@domain.de> zuzu");
        }
        
        function stripMail($addressStr){
        	$arr = explode("<", $addressStr);
        	if(count($arr) == 1) return $arr[0];
        	else if(count($arr) == 2){
        		$arr2 = explode(">", $arr[1]);
        		return $arr2[0];
        	}
        	return null;
        }
        
        
        
        
/*   not used at the moment but maybe later     
        
        
        function sendProgrammNewsletter($von = null, $bis = null, $limit = null, $layout = 'leer'){
        	if($von == NULL || $von == 0) $von = date('Y-m-d 0:00');
        	if($bis == NULL || $bis == 0) $bis = date('Y-m-d', strtotime('2030-12-30'));
        	if($limit != null) $options['limit'] = $limit;
        	$options['order'] = array('von' => 'ASC');
        
        	$cond = array('KptnTodocat.id' => 17,  'von <' => $bis, 'von >' => $von);
        	$options['conditions'] = $cond;
        
        	$programm = $this->Appointment->find('all', $options);
        
        	$viewVars = array('appointments' => $programm, 'wochentag' => array('Sonntag', 'Montag','Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'));
        	//debug($programm);
        	$this->request->data['Mail']['subject'] = 'Das Programm';
        	$this->request->data['Mail']['content'] = 'Platzhalter in der email steht hier das Programm';
        	$this->sendnewsletter("mail@truderuth.de", 7, 'programm', $viewVars, 'programm');
        }
        
        function sendnewsletter($absender = "mail@truderuth.de", $sichtbarkeit = 7, $template = false, $viewVars = false, $layout = 'default'){
        	if(!empty($this->request->data)){
        		$data = $this->request->data;
        
        		$data['Mail']['kptn_todocat_id'] = 17; //immer vom typ programm
        		$data['Mail']['from'] = $data['Mail']['sender'] = $data['Mail']['replyTo'] = $absender;
        		$emaillist = $this->Subscriber->find('list', array('fields' => array('id', 'email')));
        			
        		$list = null;
        		foreach ($emaillist as $email){
        			$list[] = array('email' => $email);
        		}
        			
        		$data['Mail']['datum'] = date('y-m-d H:i:s');
        		$data['Mail']['receiver'] = 'list@truderuth.de';
        		debug($data);
        		$this->processMail($list,$sichtbarkeit, $data, null, $template, $viewVars, $layout);
        		$this->redirect("/pages/yes");
        	}
        	$this->set('groups', $this->Admingroup->find('list'));
        }
        
        
        
        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
*/    
 
/****************** Test Section ********************/        
        
    function cakeEmailDeliverTest($email){
		$this->view = 'getYakMail';
	    $erg = CakeEmail::deliver($email, 'Test delivery', "A delivery by CakeEmail::deliver from server: ".env('SERVER_NAME'), array('from' => 'test@kpt.de'));
		pr($erg);
    }
    
    function cakeEmailTest($email){
		$this->view = 'getYakMail';
		$cakeEmail = new CakeEmail('default');	
		$data['Mail']['sender'] 	= 'test@kptn.de'; // orginaler sender 
		$data['Mail']['receiver'] 	= $email; // empfangende gruppe 
		$data['Mail']['from'] 	= 'test@kptn.de'; 
		$data['Mail']['subject'] 	= 'das ist eine TEstmail '.date('H:i:s');
		$data['Mail']['to'] 	= $email;
		$data['Mail']['content'] = "Dies ist einen Testemail gesendet vom server: ".env('SERVER_NAME');
		$data['Mail']['type'] = 'text/html';
		$data['Mail']['cetype'] = 'html';
		$data['Mail']['lb'] = '<br>';
		try{
			$this->sendMail($data);
		}catch(Exception $e){
			echo "<pr>";
			echo $e;
			echo "</pr>";
		}
	}
	
	function phpMailTest($to){
		$this->view = 'getYakMail';

		echo ('legee daten an<br>');
		$Name = "Tester"; //senders name
		$email = "test@kptn.de"; //senders e-mail adress
		$recipient = $to; //recipient
		$mail_body = "Test: und zwar der php mail funktion vom server: ".env('SERVER_NAME'); //mail body
		$subject = "php:mail Testmail"; //subject
		$header = "From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields
		echo ('schreib init_set<br>');
		ini_set('sendmail_from', 'test@kptn.de'); //Suggested by "Some Guy"

		echo ('try send<br>');
		try{		
			$erg = mail($recipient, $subject, $mail_body, $header); //mail command :)
			echo "erg ist $erg <br>";
		}catch(Exception $e){
			echo $e;
		}
	}
	

}

?>
