<?php
/*
 * Created on 12.10.2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class HttpPostComponent extends Component {
 
	function file_json_contents($url, $postArray){	
		$jsondata = json_encode($postArray);
	
		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/json',
		        'content' => $jsondata
		    )
		);
		
		$context  = stream_context_create($opts);
		return file_get_contents($url, false, $context);
	
	}
	
	/**
	make an http POST request and return the response content and headers
	@param string $url    url of the requested script
	@param array $data    hash array of request variables
	@return returns a hash array with response content and headers in the following form:
	    array ('content'=>'<html></html>'
	        , 'headers'=>array ('HTTP/1.1 200 OK', 'Connection: close', ...)
	        )
	*/
	
	function http_test ($url){
	    return array ('content'=>file_get_contents ($url));
	}
	
	
	function http_post ($url, $data){
	    $data_url = http_build_query ($data);
	    $data_len = strlen ($data_url);
	
	    return array ('content'=>file_get_contents ($url, false, 
	    stream_context_create (array (
			'http'=>array (
				'method'=>'POST', 
				'header'=>"Connection: close\r\nContent-Length: $data_len\r\nContent-type: application/x-www-form-urlencoded", 	
				'content'=>$data_url
            ))))
        , 'headers'=>$http_response_header
        );
	}

	function http_post2 ($url, $data){
	    $data_url = http_build_query ($data);
	    $data_len = strlen ($data_url);	

		$context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . $data_len . "\r\n",
            'content' => $data_url
            )
        );
		$context = stream_context_create($context_options);
	    return array ('content'=>file_get_contents ($url, false,$context));
	}

	function http_post3 ($url, $data){
	    $data_url = http_build_query ($data);
	    $data_len = strlen ($data_url);
	
	    return array ('content'=>file_get_contents ($url, false, 
	    stream_context_create (array (
			'http'=>array (
				'method'=>'POST', 
				'header'=>"Connection: close\r\nContent-Length: $data_len\r\nContent-type: application/x-www-form-urlencoded", 	
				'content'=>$data_url
            ))))
        );
	}
	function http_post4 ($url, $body,$https_user= 'test', $https_password = 'test'){
			$opts = array('http' =>
		  array(
		    'method'  => 'POST',
		    'header'  => "Content-Type: text/xml\r\n".
		      "Authorization: Basic ".base64_encode("$https_user:$https_password")."\r\n",
		    'content' => $body,
		    'timeout' => 60
		  )
		);
		                       
		$context  = stream_context_create($opts);
		$result = file_get_contents($url, false, $context, -1, 40000);
	}
 
 }
?>