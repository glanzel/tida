<?php

class TimeComponent extends Component {

	var $trans = array("Sun" => "So", "Mon" => "Mo", "Tue" => "Di", "Wen" => "Mi", "Thu" => "Do", "Fry" => "Fr", "Sat" => "Sa");

	function toDecimal($time){
		$timeArr = explode(':', $time);
		$decTime = ($timeArr[0]) + ($timeArr[1]/60);
	   return $decTime;
	} 
	
	function getGermanDate($format, $timeStr){
		$day = date('D', $timeStr);
		$tag = $this->trans[$day];
		$datum = $tag.date($format, $timeStr);
	}
	
}
	
?>