<?php
class WikiEntriesController extends AppController {

	var $name = 'WikiEntries';
	var $uses = array('WikiEntry','WikiVersion');

	function beforeFilter(){
		$this->Auth->allow('view');
		parent::beforeFilter();
	}

	
	function index() {
		$this->WikiEntry->recursive = 0;
		$this->set('entries', $this->paginate());
	}

	function vindex($name = 'start') {
		$this->WikiEntry->recursive = 0;
		$entryV = $this->WikiVersion->find('all',array('conditions' => array('name' => $name)));
		//debug($entryV);
		$this->set('entries', $entryV);
	}


	function view($name = null) {
		//$this->set('show_boxes', false ); //traingsboxen werden nicht angezeigt
		
		//$this->view = 'view';
		if (!$name) {
			$this->Session->setFlash(__('Invalid entry', true));
			$this->redirect(array('action' => 'index'));
		}
		$entry = $this->WikiEntry->find('first', array('conditions' => array('WikiEntry.name' => $name)));
		if(empty($entry)) $this->redirect( "/wiki_entries/add/$name" );
		

		// Kostrukte der art [[training]] in links übersetzten
		$eparts =explode(']]', $entry['WikiEntry']['value']);
		$text = '';
		foreach($eparts as $epart){
			if(strrpos($epart, '[[') === false){
				$text .= $epart;
				continue;	
			} 
			$text .= substr($epart,0, strrpos($epart, '[['));
			$entryname = substr($epart, (strrpos($epart, '[[')+2) );
		
			$text .= "<a href=".Router::url(array('action' => 'view', $entryname)).">$entryname</a>"; //

		}
		$entry['WikiEntry']['value'] = $text;		
		//debug($text);
		$this->set('entry', $entry);
		$this->set('speech', $this->speech);

		
	}
	function test(){
			$this->render('view');
			App::import('Helper', 'Html'); // loadHelper('Html'); in CakePHP 1.1.x.x
        	$html = new HtmlHelper();
			
			$text .= $html->link('test', "/entries/view/test/" );
		
	}

	function add($name = '') {
		
		//echo("$name $this->speech ");
		//debug($this);
		
		if (!empty($this->request->data)) {
			$this->WikiEntry->create();
			$this->request->data['WikiEntry']['user_id'] = $this->Auth->user('id');
			if ($this->WikiEntry->save($this->request->data)) {
				$this->Session->setFlash(__('The entry has been saved', true));
				$this->redirect(array('action' => "view", $this->request->data['WikiEntry']['name'] ));
				debug($this->request->data);
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.', true));
			}
		}else{
			$this->request->data['WikiEntry']['name'] = $name;
			$this->request->data['WikiEntry']['speech'] = $this->speech;
			debug($this->request->data);
		}
		
	}
	
	
	function restore() {
		
		/* deprecated
		if (!$ever_id) {
			$this->Session->setFlash(__('Invalid eversion', true));
			$this->sredirect(array('controller' => 'entries', 'action' => 'index'));
		}
		*/
		
		if (empty($this->request->data) || ! isset($this->request->data['WikiVersion']['id'])) {
			$this->Session->setFlash(__('Invalid eversion', true));
			$this->sredirect(array('controller' => 'entries', 'action' => 'index'));
		}
		
		$ever_id = $this->request->data['WikiVersion']['id'];
		//debug($ever_id);
		
		
		$ever = $this->WikiVersion->read(null, $ever_id);
		$last_version = $this->WikiEntry->read(null, $ever['WikiVersion']['entry_id']);
		$this->backupEntry($last_version);				
		
		$ever['WikiVersion']['id'] = $ever['WikiVersion']['entry_id'];
		$entry['WikiEntry'] = $ever['WikiVersion'];
		$this->WikiEntry->save($entry);
		$this->redirect(array('action' => 'view', $entry['WikiEntry']['name']));
		
	}
	

	function edit($id = null, $wysiwyg = false) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid entry', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			$last_version = $this->WikiEntry->read(null,$this->request->data['WikiEntry']['id']);
			$this->request->data['WikiEntry']['user_id'] = $this->Auth->user('id');
			//debug($this->request->data);
			if ($this->WikiEntry->save($this->request->data)) {
				$this->Session->setFlash(__('The entry has been saved', true));
				$this->backupEntry($last_version);				
				//if($lastUrl = $this->Session->read('lastUrl')) $this->redirect($lastUrl); 
				//else 
				$this->redirect(array('action' => 'view', $this->request->data['WikiEntry']['name']));
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->WikiEntry->read(null, $id);
			//Configure::write('Entry.'.$id.'.'.$this->Auth->user('id'), date('Y-m-d H:i'));
		}
		$this->set('id', $id);
		$this->set('wysiwyg', $wysiwyg);
		//debug(Configure::read('Entry.'.$id));
	}
	function backupEntry($last_version){
		$entryV['WikiVersion'] =  $last_version['WikiEntry'];
		$entryV['WikiVersion']['entry_id'] = $entryV['WikiVersion']['id'];
		unset($entryV['WikiVersion']['id']);
		unset($entryV['WikiVersion']['created']);
		$this->WikiVersion->create();
		$this->WikiVersion->save($entryV);
	}

	function delete($id = null) {
		echo($this->Auth->user('id'));
/*
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for entry', true));
			$this->sredirect(array('action'=>'index'));
		}
		if ($this->WikiEntry->delete($id)) {
			$this->Session->setFlash(__('Entry deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Entry was not deleted', true));
		$this->sredirect(array('action' => 'index'));
*/	
	}
	
}
?>