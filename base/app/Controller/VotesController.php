<?php
class VotesController extends AppController {

	var $name = 'Votes';

	function index() {
		$this->Vote->recursive = 0;
		$this->set('votes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid vote'));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('vote', $this->Vote->read(null, $id));
	}
	
	function add($decision_id, $proposal_id, $votecat_id) {

			// gucken ob das bereits existiert
			$opt['proposal_id'] = $proposal_id;
			$opt['user_id'] = $this->Auth->user('id');
			$this->Vote->recursive = -1; // die anderen Models werden nicht gebraucht
			$data = $this->Vote->find('first', array('conditions' => $opt));
			pr($data);

			//bestehende werte überschreiben			
			$data['Vote']['proposal_id'] = $proposal_id;
			$data['Vote']['user_id'] = $this->Auth->user('id');;
			$data['Vote']['decision_id'] = $decision_id;
			$data['Vote']['votecat_id'] = $votecat_id;
			pr($data);

			// geht auch bei update
			$this->Vote->create();

			// insert or update
			if ($this->Vote->save($data)) {
				$this->Session->setFlash(__('The vote has been saved'));
			} else {
				$this->Session->setFlash(__('The vote could not be saved. Please, try again.'));
			}
			$this->redirect(array('controller' => 'Decisions', 'action' => "view/$decision_id"));
	}

	function edit($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('Invalid vote'));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Vote->save($this->request->data)) {
				$this->Session->setFlash(__('The vote has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The vote could not be saved. Please, try again.'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Vote->read(null, $id);
		}
		$decisions = $this->Vote->Decision->find('list');
		$users = $this->Vote->User->find('list');
		$proposals = $this->Vote->Proposal->find('list');
		$votecats = $this->Vote->Votecat->find('list');
		$this->set(compact('decisions', 'users', 'proposals', 'votecats'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for vote'));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Vote->delete($id)) {
			$this->Session->setFlash(__('Vote deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Vote was not deleted'));
		$this->redirect(array('controller' => 'Decision', 'action' => 'index', $this->request->params['named']['backid']));
	}
}
?>