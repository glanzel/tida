<?php
class Admingroup extends AppModel {
	var $name = 'Admingroup';
	var $displayField = 'name';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Decision' => array(
			'className' => 'Decision',
			'foreignKey' => 'admingroup_id',
			'dependent' => false,
		),
		'AdmingroupsUser' => array(
			'className' => 'AdmingroupsUser',
			'foreignKey' => 'admingroup_id',
			'dependent' => false,
		)
	);

	var $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'admingroups_users',
			'foreignKey' => 'admingroup_id',
			'associationForeignKey' => 'user_id',
			'unique' => true,
			'order' => 'name asc'
		)
	);
	
	function getGroups($user_id, $key = false){
		$sql = "SELECT Admingroup.id, Admingroup.name FROM admingroups Admingroup
				INNER JOIN admingroups_users au ON au.admingroup_id = Admingroup.id AND user_id = $user_id";
		$ergs = $this->query($sql);
			foreach($ergs as $row){
				if($key) $groups[$row['Admingroup']['name']] = $row['Admingroup']['id'];
				else $groups[] = $row['Admingroup']['id'];
			}
		return $groups;		
	}

	function getGroupUsers($id){
		$sql = "SELECT name, user_id FROM admingroups_users au
				INNER JOIN users User ON au.user_id = User.id 
				WHERE au.admingroup_id = $id";
		$ergs = $this->query($sql);
		foreach($ergs as $row){
			$users[$row['User']['name']] = $row['au']['user_id'];
		}
		return $users;		
	}

	


}
?>