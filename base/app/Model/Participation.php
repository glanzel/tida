<?php
App::uses('AppModel', 'Model');
/**
 * Participation Model
 *
 * @property User $User
 */
class Participation extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/*
	var $validate = array(
		'zusage' => array('rule' => 'numeric','required' => true, 'message' => 'Teilnahmeart auswählen')
	);
	*/
}
