<?php
class KptnTodo extends AppModel {

	var $name = 'KptnTodo';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'KptnTodocat' => array(
			'className' => 'KptnTodocat',
			'foreignKey' => 'kptn_todocat_id',
		),
		'Admingroup' => array(
			'className' => 'Admingroup',
			'foreignKey' => 'sichtbarkeit',
		)
	);
	
	function openTodos($user_id){
		$sqlSelect = "SELECT * FROM kptn_todos KptnTodo
				INNER JOIN admingroups_users au ON au.admingroup_id = KptnTodo.editierbarkeit AND au.user_id = $user_id
				WHERE KptnTodo.zugeordnet = '' AND fertig IS NULL";
		$ergs = $this->query($sqlSelect);
		//pr($ergs);	
		return $ergs;
	}
	function myNextTodos($user_id, $username, $anzahl){
		$sqlSelect = "SELECT * FROM kptn_todos KptnTodo
				INNER JOIN admingroups_users au ON au.admingroup_id = KptnTodo.editierbarkeit AND au.user_id = $user_id
				WHERE KptnTodo.zugeordnet like '%$username%' AND fertig IS NULL
				ORDER BY stichtag LIMIT $anzahl ";
		$ergs = $this->query($sqlSelect);
		//pr($ergs);
		//echo ($username);	
		return $ergs;
	}

}

?>