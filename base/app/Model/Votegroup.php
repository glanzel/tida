<?php
class Votegroup extends AppModel {
	var $name = 'Votegroup';
	var $displayField = 'lfdNr';
	var $order = array('lfdNr ASC');
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Decision' => array(
			'className' => 'Decision',
			'foreignKey' => 'decision_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Solution' => array(
			'className' => 'Proposal',
			'foreignKey' => 'choosen_proposal',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	var $hasMany = array(
		'Proposal' => array(
			'className' => 'Proposal',
			'foreignKey' => 'votegroup_id',
			'dependent' => false,
		)
	);
	
}
?>