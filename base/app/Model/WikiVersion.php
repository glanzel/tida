<?php
class WikiVersion extends AppModel {
	var $name = 'WikiVersion';
	var $displayField = 'name';
	
	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);
	
}
?>