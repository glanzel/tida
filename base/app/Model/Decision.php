<?php
class Decision extends AppModel {
	var $name = 'Decision';
	var $displayField = 'title';
	var $actsAs = array('Containable');
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Admingroup' => array(
			'className' => 'Admingroup',
			'foreignKey' => 'admingroup_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'KptnTodocat' => array(
			'className' => 'KptnTodocat',
			'foreignKey' => 'kptnTodocat_id',
		)
	);
	var $hasAndBelongsToMany = array(
		'Appointment' => array(
			'className' => 'Appointment',
			'joinTable' => 'appointments_decisions',
			'foreignKey' => 'decision_id',
			'associationForeignKey' => 'appointment_id',
			'unique' => true,
		)
	);

	var $hasMany = array(
		'Votegroup' => array(
			'className' => 'Votegroup',
			'foreignKey' => 'decision_id',
			'dependent' => false,
			'order' => 'lfdNr ASC'
		)
	);
	
	function calcDecisionVotes($decisions){
		foreach($decisions as & $decision){ 
			$decision['Decision']['sumVotes'] = $this->sumVotes($decision['Decision']['id']);
			$decision['Decision']['sumProposal'] = $this->sumProposals($decision['Decision']['id']);
			$decision['Decision']['sumVoter'] = $this->sumVoter($decision['Decision']['id']);
			//echo($decision['Decision']['title']."   props: ".$decision['Decision']['sumProposal']."   voter : ".$decision['Decision']['sumVoter']."<br>");
			$posVotes = $decision['Decision']['sumVoter'] * $decision['Decision']['sumProposal'];
			$decision['Decision']['voteQuote'] = $decision['Decision']['sumVotes']."/".$posVotes;
		}
		return $decisions;
	}
	
	
	
	function result($decision_id, $users){
		foreach($users as $user){
			$sqlSelect = "SELECT Proposal.id, Proposal.title, Vote.erstellt_von, Votecat.id, Votecat.name, Votecat.pkt, Votecat.aufschiebend 
				FROM proposals Proposal
				LEFT JOIN votes Vote ON Vote.proposal_id = Proposal.id AND Vote.user_id = '".$user['id']."' 
				LEFT JOIN votecats Votecat ON Votecat.id = Vote.votecat_id 
				INNER JOIN votegroups Votegroup ON Votegroup.id = Proposal.votegroup_id
				WHERE Votegroup.decision_id = '$decision_id'
				ORDER BY Votegroup.id, Proposal.title";
			$ergs[$user['name']] = $this->query($sqlSelect);
			foreach ($ergs[$user['name']] as $key => $value) $ergs[$user['name']][$key]['User']['id'] = $user['id'];
			//pr($ergs);	
		}
		return $ergs;
	}
	
	function resultSumme($decision_id){
			$sqlSelect = "SELECT Votegroup.lfdNr, Proposal.id, Proposal.title, Proposal.beschreibung, Proposal.votegroup_id, SUM(Votecat.pkt) as pkt, SUM(Votecat.aufschiebend) as aufschiebend, COUNT(Votecat.aufschiebend) as stimmen, DecisionComment.comment
				FROM proposals Proposal
				LEFT JOIN decision_comments DecisionComment ON DecisionComment.proposal_id = Proposal.id  
				LEFT JOIN votes Vote ON Vote.proposal_id = Proposal.id  
				LEFT JOIN votecats Votecat ON Votecat.id = Vote.votecat_id 
				INNER JOIN votegroups Votegroup ON Votegroup.id = Proposal.votegroup_id
				WHERE Votegroup.decision_id = '$decision_id'  GROUP BY Proposal.id
				ORDER BY Votegroup.id, Proposal.title";
			$ergs = $this->query($sqlSelect);	
		return $ergs;
	}
	
	function noVotes($user_id){
		$sqlSelect = "SELECT * FROM decisions Decision
				INNER JOIN votegroups Votegroup ON Votegroup.decision_id = Decision.id 
				INNER JOIN proposals Proposal ON Proposal.votegroup_id = Votegroup.id 
				LEFT JOIN votes Vote ON Vote.proposal_id = Proposal.id AND Vote.user_id = $user_id
				INNER JOIN admingroups_users au ON au.admingroup_id = Decision.stimmberechtigt AND au.user_id = $user_id
				WHERE Vote.decision_id IS NULL GROUP BY Decision.id";
		$ergs = $this->query($sqlSelect);
		//pr($ergs);	
		return $ergs;
	}
	
	function archiv(){
		$sqlSelect = "SELECT * FROM decisions Decision
				INNER JOIN votegroups Votegroup ON Votegroup.decision_id = Decision.id 
				WHERE Votegroup.choosen_proposal >  0 ";
		$ergs = $this->query($sqlSelect);
		//pr($ergs);	
		return $ergs;
	}
	
	function sumProposals($decision_id){
		$sqlSelect = "SELECT COUNT(*) as anzahl FROM proposals Proposal
				INNER JOIN votegroups Votegroup ON Proposal.votegroup_id = Votegroup.id 
				WHERE Votegroup.decision_id =  '$decision_id'";
		$ergs = $this->query($sqlSelect);
		return $ergs[0][0]['anzahl'];
	}

	function sumVotes($decision_id){
		$sqlSelect = "SELECT COUNT(*) as anzahl FROM votes Vote
				WHERE Vote.decision_id =  '$decision_id'";
		$ergs = $this->query($sqlSelect);
		return $ergs[0][0]['anzahl'];
	}

	function sumVoter($decision_id){
		$sqlSelect = "SELECT COUNT(*) as anzahl FROM admingroups_users au 
				LEFT JOIN decisions Decision ON Decision.stimmberechtigt = au.admingroup_id
				WHERE Decision.id =  '$decision_id'";
		$ergs = $this->query($sqlSelect);
		return $ergs[0][0]['anzahl'];
	}
	
	function nextVotegroupLfdNr($id){
		$sqlSelect = "SELECT max(lfdNr) as diese FROM `votegroups` where decision_id = '$id'";
		$ergs = $this->query($sqlSelect);
		$next = $ergs[0][0]['diese'] + 1;
		return $next;	
	}
	
	function addGroup($id){
		$this->Votegroup->create();
		$data['Votegroup']['decision_id'] = $id;
		$data['Votegroup']['lfdNr'] = $this->nextVotegroupLfdNr($id);
		$this->Votegroup->save($data);
		return $this->Votegroup->id;
	}
	
	
}
?>