<?php
class User extends AppModel {

	public $name = 'User';
	public $displayField = 'forename';
	//var $actsAs = 'ExtendAssociations'; 
	
	var $validate = array(
		'email' => array(
			'mrule' => array('rule' => 'email','required' => true, 'message' => 'Keine Email angegeben'), 
			'urule' =>  array('rule' => 'isUnique', 'message' => 'Adresse schon in benutzung')
		)
	);

	var $hasAndBelongsToMany = array(
			'Admingroup' => array('className' => 'Admingroup',
						'joinTable' => 'admingroups_users',
						'foreignKey' => 'user_id',
						'associationForeignKey' => 'admingroup_id',
						'unique' => true,
			)
			
	);
/*
	var $hasMany = array(
			'Shiftwish' => array('className' => 'Shiftwish',
						'foreignKey' => 'user_id'
			)
	);
*/
	
	
	
	function listByGroup($id){
		$sqlSelect = "SELECT u.id, u.name FROM users u 
		 INNER JOIN admingroups_users au ON au.user_id = u.id AND au.admingroup_id = $id"; 

		$ergs = $this->query($sqlSelect);
		foreach($ergs as $row){
			$names[$row['u']['id']] = $row['u']['name'];
		}
		return $names;		
	}
	
	function getActivationHash($email = null){
		if(empty( $email )) $email = $this->field('email');
		$hashSt = Configure::read('Security.salt') . $email;
		//echo($hashSt."<br>");
		$hashSt = Security::hash($hashSt);
		//echo($hashSt."<br>");
		return substr($hashSt, 0, 8);
	}
	
	function register($data){
		if (!empty($data)) {
			$this->create();
			$this->Auth->hashPasswords($data);			
			if ($this->User->save($this->data)) {
				return true;			
			} else {
				return false;		
			}
		}	
	}
	
	function finde(){
		return $this->query(" SELECT * FROM tida.users as User UNION ALL select * FROM goldammer.users as User ");
	}
	
	function union($dbname1, $dbname2, $options = array()){
		$db = $this->getDataSource();
		$stdArry =	array(
				'fields'     => array('*'),
				'table'      => $db->fullTableName($this),
				'alias'      => 'User',
				'limit'      => null,
				'offset'     => null,
				'conditions' => null,
				'order'      => null,
				'group'      => null
		);
		$stdArry = array_merge($stdArry, $options);
		$query1 = $db->buildStatement($stdArry,	$this);
		debug($query1);
		
		$this->setDatabase($dbname2);
		$db2 = $this->getDataSource();
		
		$query2 = $db2->buildStatement($stdArry, $this);
		$query = $query1." UNION ".$query2;
		debug($query);
		return $this->query($query);
	}

}
?>
