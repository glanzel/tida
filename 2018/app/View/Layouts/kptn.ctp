<?php  header("Content-Type: text/html; charset=utf-8"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<title> das klingt doch gut ... | <?=$title_for_layout?> </title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<?=$this->Html->css('tidadesign'); ?>

	<?= $this->Html->script('kptn.js');?>
	<?= $this->Html->meta('icon', $this->Html->url('/favicon.ico')); ?>

</head>
<body class="mainPage" >

<? setlocale(LC_ALL, 'de_DE.utf8'); ?>
<center>

<?= $this->Session->flash() ?>
		<div style="height:80px; width:100%; background-color:#0088aa; border-bottom: solid 2px black;">
			<div style="width:70%;">
				<div style="float:right; margin-top:5px; margin-right:-30px; height:40px;">  
					<? echo $this->Html->image("icons_hochkant.svg", array('height' => 150,)); ?>
				
				</div>
				<div >
					<div style="float:left; margin-right:15px; ">
						<?= $this->Html->link($this->Html->image("logo_eckig.svg", array('width' => 65, 'id' => 'tidalogo')), '/', array('escape' =>false)); ?> 
					</div>
					<div style="text-align:left;">
						<div style="font-size:33px; padding-top:6px;"> klingt<span style="font-weight:600;color:#3b293b">gut</span> </div>
						<div style="font-size:18px"> <?= date("j.n.", strtotime( Configure::read('klingtgut.start'))) ?> bis <?= date("j.n.Y", strtotime( Configure::read('klingtgut.ende'))) ?></div>
					</div>
				</div>
			</div>
		</div>
		<div id="menu" style="width:70%;margin-bottom:40px;">
			<div style="font-size:110%; ">
				<div style="float:right;margin-right:70px;margin-top:-24px;">
					<? if($username){ ?>
					<div class="polygon_link_2">
						<div class="plcontent">  
						<?= $this->Html->link("ich", '/Users/edit'); ?> |
						<?= $this->Html->link("teilnahme", "/Participations/view/$user_id");  ?> |		
						<?= $this->Html->link("logout", '/Users/logout'); ?>
						</div>
					<?=  $this->Html->image('link_background_white_230.svg', array('width' => '210px')); ?>
					</div>
					
					<? } else { ?>
					<div class="polygon_link_2">
					<div class="plcontent"><?= $this->Html->link("login", '/Users/login'); ?></div>
					<?=  $this->Html->image('link_background_white.svg', array('width' => '65px')); ?>
					</div>
					<div class="polygon_link_2">  
					<div class="plcontent"><?= $this->Html->link("teilnehmen", '/Users/add'); ?></div>
					<?=  $this->Html->image('link_background_white_120.svg', array('width' => '120px')); ?>
					</div>

					<? } ?>
				</div>
				<div>
					<div class="polygon_link first_link">  
					<div class="plcontent"><?= $this->Html->link("programm", '/WikiEntries/view/zeitplan'); ?> </div> 
					<?=  $this->Html->image('link_background_120.svg', array('width' => '100px')); ?>
					</div>

					<div class="polygon_link reverse">  
					<div class="plcontent"><?= $this->Html->link("fotos", '/pages/color'); ?></div> 
					<?=  $this->Html->image('link_background.svg', array('width' => '60px')); ?>
					</div>
	
					<? if($username){ ?>

					<div class="polygon_link ">  
					<div class="plcontent">
						<?= $this->Html->link("infos", '/WikiEntries/view/orga'); ?>  |
						<?= $this->Html->link("anfahrt", '/pages/anfahrtmit'); ?>  |
						<? 
							if($zusage > 1) echo $this->Html->link("bezahlen", '/WikiEntries/view/bezahlen');
							else echo $this->Html->link("bezahlen", '/Participations/edit/$user_id');
						?>
					</div> 
					<?=  $this->Html->image('link_background_260.svg', array('width' => '210px')); ?>
					</div>

					<? } else { ?>
					<div class="polygon_link ">  
					<div class="plcontent"><?= $this->Html->link("infos", '/WikiEntries/view/orga'); ?></div>
					<?=  $this->Html->image('link_background.svg', array('width' => '60px')); ?>
					</div>
					<? } ?>
					<? if($cox){ ?>
					<div class="polygon_link ">  
						<div class="plcontent">	
						<?= $this->Html->link("mail", '/Mails/sendwebmail'); ?>  |
						<?= $this->Html->link("gäste", '/Participations/'); ?>
						</div>
						<?=  $this->Html->image('link_background_120.svg', array('width' => '110px')); ?>
					</div>
					<? } ?>
				</div>
			</div>
		</div> <!--end of siteinfo-->
		<div id=“hauptteil“ style="width:67%;text-align:left;margin-right:3%">
			<?=$content_for_layout?>
			<?=$this->Session->flash('auth');?>
		</div>

	<!-- end content -->


</center>

</body>

</html>


