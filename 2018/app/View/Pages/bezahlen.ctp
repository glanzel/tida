
Aufgrund der geänderten Auflagen und damit wir mehr Planungsicherheit haben, müssen wir das Geld in diesem Jahr schon im Voraus einsammeln. Bitte bis 2 Wochen nach Anmeldung überweisen.
<br><br>
Early-Bird: Überweist ihr uns den Unkostenbeitrag im Voraus bis zum 01.06.2016, dann zahlt ihr pro Person für das ganze Wochenende 35* € (Erwachsene), bzw. 15 € (Kinder ab 6). Für einen Tag & Nacht sind es dann 20 €, bzw. 10 €.
<br><br> 
Nach dem 1.6. kostet das ganze Wochenende dann 40* € (Erwachsene) bzw. 15 € (Kinder). Für jeweils einen Tag & eine Nacht beträgt der Beitrag 25 € bzw. 10 €.
<br><br> 
*dieser Betrag ist knapp kalkuliert. Wer ausreichend Geld hat, kann gerne 5-10 € mehr geben. 
<br><br>
Wie funktioniert es?<br>
Ihr überweist den Betrag auf folgendes Konto:<br><br>
Comdirect<br>
Kontoinhaber: Moritz Mottschall<br>
IBAN: DE64200411440608393500<br>
BIC: COBADEHD044<br>
Verwendungszweck: klingt gut + Vor- und Nachname (der TeilnehmerInnen)<br>
<br><br> 
Wichtig: Überweist ihr gleich für mehrere Personen, dann bitte alle Namen im Betreff aufführen. Auch in diesem Jahr werden keine Tickets oder ähnliches im Vorfeld verschickt, sondern wir sitzen ganz oldschool mit einer Namensliste am Eingang. Wollt ihr eine Eingangsbestätigung eurer Überweisung gebt zusätzlich einfach eure Mailadresse an.

