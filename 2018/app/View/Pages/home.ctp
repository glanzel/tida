<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        @font-face {
            font-family: ubuntu;
            src: local("ubuntu"),
            url('./Ubuntu-R.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        a {
            font-family: ubuntu !important;
        }

        body {
            background-color: #0088aa;
            width: 100%;
            height: 100%;
            font-family: ubuntu;
            font-size: 20px;
            margin: 0;
        }

        .kategorie_bereich {
            position: absolute;
            white-space: nowrap;
        }

        .kategorie_bereich .kat_schrift_links {
            display: inline-block;
            margin: 4px 0 0 11px;
            padding: 0 0 7px 0;
            line-height: 25px;
            border-bottom: solid 2px black;
            width: 250px;
        }

        .kategorie_bereich .kat_schrift_rechts {
            display: inline-block;
            margin: 4px 11px 0 27px;
            padding: 0 0 7px 0;
            line-height: 25px;
            border-bottom: solid 2px black;
            width: 200px;
            text-align: right;
        }

        .kategorie_bereich .kat_titel {
            font-size: 180%;
            font-weight: 500;
        }

        .kategorie_bereich img {
            vertical-align: bottom;
        }

        #fb_link span {
            line-height: 40px;
            vertical-align: top;
            font-size: 110%;
        }

        #logo_title {
            top: 6px;
            font-size: 143%
        }

        #logo_text {
            position: absolute;
            left: calc(25% + 137px);
            top: 7px;
        }

        #logo_title span {
            color: #800080;
            font-weight: 500
        }

        .kg_text {
            position: absolute;
            color: white;
            font-size: 85%;
        }

    </style>
</head>
<body>


<div style="position:absolute;left:25%;top:0;">
	<?= $this->Html->image("logo_rand.svg") ?>
</div>
<div id="logo_text">
    <div id="logo_title">
        klingt<span>gut</span>
    </div>
    <div style="top:40px;font-size:90%">
        7.9 - 9.9.2018
    </div>
</div>
<div style="position:absolute;left:calc(25% + 55px);top:62px;font-size:120%">
    du bist das Festival
</div>
<div style="min-width: 1050px;position: relative">
   
	<div style="position:absolute;left:calc(50% - 200px);top:130px;">
		<?= $this->Html->image("panda_bunt_g.png", array('width' => '400px')) ?>
	</div >   

	<a href="<?= $this->Html->url(array("controller" => "WikiEntries", "action" => "view", "zeitplan")) ?>">
    <div class="kategorie_bereich" style="right: calc(50% + 200px);top:170px;">
		<?= $this->Html->image("feiern_logo.svg") ?>
        <div class="kat_schrift_links" style="width:220px">
            <span class="kat_titel">feiern
            
            </span>
            <br>programm
        </div>
    </div>
	</a>

	<a href="<?= $this->Html->url(array("controller" => "WikiEntries", "action" => "view", "orga")) ?>">
    <div class="kategorie_bereich" style="right: calc(50% + 140px);top:400px;">
		<?= $this->Html->image("wissen_logo.svg") ?>
        <div class="kat_schrift_links" style="">
            <span class="kat_titel">wissen</span>
            <br>orga/info
        </div>
    </div>
	</a>	

	<a href="<?= $this->Html->url(array("controller" => "Users", "action" => "login")) ?>">
    <div class="kategorie_bereich" style="left:calc(50% + 150px);top:30px;">
        <div style="float:left;width:27px;bottom: -32px;position: inherit;display: inline-block;">
		<?= $this->Html->image("strich.svg") ?>
        </div>
        <div class="kat_schrift_rechts" style="">
            <span class="kat_titel">dabei sein</span>
            <br>login/anmeldung
        </div>
		<?= $this->Html->image("zelt.svg") ?>
    </div>
    </a>

	<a href="<?= $this->Html->url("color") ?>">
    <div class="kategorie_bereich" style="left:calc(50% + 170px);top:300px;">
        <div class="kat_schrift_rechts" style="width:170px">
            <span class="kat_titel">sehen</span>
            <br>fotos
        </div>
		<?= $this->Html->image("fotoapparat.svg") ?>
    </div>
    </a>

	<a href="https://www.facebook.com/events/2021094924836725/">
    <div id="fb_link" style="position:absolute;left:65%;top:520px;">
        <span>folge uns auf</span>
		<?= $this->Html->image("fb_logo.svg") ?>
    </div>
	</a>

    <!-- text -->

    <p class="kg_text" style="top:240px; right: calc(50% + 280px);">
        Workshops<br>
        von allen für alle
    </p>
    <p class="kg_text" style="top:310px; right: calc(50% + 350px);">
        Musik und Kunst<br>
        von Freund*innen
    </p>
    <p class="kg_text" style="top:130px; left: calc(50% + 200px);">
        Zelten auf der Insel
    </p>

    <p class="kg_text" style="top:190px; left: calc(50% + 300px);">
        Bühnen und Tanzflächen <br>
        drinnen und draußen
    </p>
    <p class="kg_text" style="top:410px; left: calc(50% + 180px);">
        zusammen kochen und <br>
        gemeinsam essen
    </p>
</div>
</body>
</html>