<?php
class Appointment extends AppModel {

	public $virtualFields = array('fulltitle' => 'CONCAT(Appointment.title, " (", DATE_FORMAT (Appointment.von, "%d.%m.%y"), ")")','datum' => 'DATE_FORMAT (von, "%e.%m.%y")');

	public $displayField = 'fulltitle';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'KptnTodocat' => array(
			'className' => 'KptnTodocat',
			'foreignKey' => 'kategorie',
		),
		'Admingroup' => array(
			'className' => 'Admingroup',
			'foreignKey' => 'sichtbarkeit',
		)
	);
	
	var $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'appointments_users',
			'foreignKey' => 'appointment_id',
			'associationForeignKey' => 'user_id',
			'unique' => true,
		),
		'Decision' => array(
			'className' => 'Decision',
			'joinTable' => 'appointments_decisions',
			'foreignKey' => 'appointment_id',
			'associationForeignKey' => 'decision_id',
			'unique' => true,
		)
	);
	
	function createWiederholungsTermine($datum){
		$this->contain('User');
		$cond = array("Appointment.wiederholen" => 1, "WEEKDAY(Appointment.von) = WEEKDAY('$datum')" ,  "angelegt_bis < '$datum'" );
		$ergs = $this->find('all', array('conditions' => $cond));
		if(empty($ergs)) return;
		foreach($ergs as $row){
			// apspeichern bis wann wiederholungen angelegt wurden
			$row['Appointment']['angelegt_bis'] = $datum;
			$this->create();
			$this->save($row);
			
			//wiederholungen anlegen

			$row['Appointment']['wiederholen'] = 0;  // kind termin ist nur einmal

			// benutzer auf wiederholungen übertragen 			
			foreach($row['User'] as $key=>$user){
				unset($row['User'][$key]['AppointmentsUser']['id']); // damit die zuordnung nicht dem orginal termin weggenommen wird
			}
		
			unset($row['Appointment']['id']); //es soll ein neuer eintrag angelegt werden

			//datum des aktuellen termins setzen
			$row['Appointment']['von'] = $datum." ".date('H:i', strtotime($row['Appointment']['von']));
			$this->create();
			$this->save($row);
			//pr($row);
		}		
	}
	
	function termineByWochentag($datum, $user_id = 0){
		$sqlSelect = "SELECT Appointment.*, KptnTodocat.*, count(AppointmentsUser.id) as users   FROM appointments Appointment
				INNER JOIN admingroups_users agroup ON agroup.admingroup_id = Appointment.sichtbarkeit AND agroup.user_id = $user_id
				LEFT JOIN kptn_todocats KptnTodocat ON KptnTodocat.id = kategorie
				LEFT JOIN appointments_users AppointmentsUser ON AppointmentsUser.appointment_id = Appointment.id		 
				WHERE DATE(von) = '$datum' AND wiederholen = 0
				Group BY Appointment.id";
		$ergs = $this->query($sqlSelect);
		return $ergs;
	}
	
	function nextDoodleId(){
		$sqlSelect = "SELECT max(doodle_id) as diese FROM `appointments` app";
		$ergs = $this->query($sqlSelect);
		pr($ergs);
		$next = $ergs[0][0]['diese'] + 1;
		return $next;	
	}
	
	function openEvents($user_id, $vorlauf = 7){
		$vorlaufDatum = date('y-m-d', time()+(60*60*24* $vorlauf));
		$sqlSelect = "SELECT * FROM appointments Appointment
				INNER JOIN admingroups_users agroup ON agroup.admingroup_id = Appointment.sichtbarkeit AND agroup.user_id = $user_id
				WHERE wiederholen = 0 AND von >= NOW() AND von <= '$vorlaufDatum'
				AND Appointment.id NOT IN (SELECT appointment_id FROM appointments_users)";
		$ergs = $this->query($sqlSelect);
		//pr($ergs);	
		return $ergs;
	}
	
	function myNextEvents($user_id, $anzahl = 3){
		$sqlSelect = "SELECT KptnTodocat.title, Appointment.* FROM appointments Appointment
				LEFT JOIN kptn_todocats KptnTodocat ON KptnTodocat.id = Appointment.kategorie
				INNER JOIN appointments_users au ON au.appointment_id = Appointment.id AND au.user_id = $user_id
				WHERE wiederholen = 0 AND von >= NOW()
				ORDER BY von LIMIT $anzahl ";
				$ergs = $this->query($sqlSelect);
		//pr($ergs);	
		return $ergs;
	}
	
	
	

}
?>