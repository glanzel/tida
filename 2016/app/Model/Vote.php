<?php
class Vote extends AppModel {
	var $name = 'Vote';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proposal' => array(
			'className' => 'Proposal',
			'foreignKey' => 'proposal_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Votecat' => array(
			'className' => 'Votecat',
			'foreignKey' => 'votecat_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>