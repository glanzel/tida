<?php
/* SVN FILE: $Id$ */

class AppModel extends Model {
	 var $actsAs = array('Containable');
/*
    function __construct($id=false, $table=null, $ds=null){
        parent::__construct($id, $table, $ds);
        if(!defined('SET_NAMES_UTF8')){
            $this->query("SET NAMES 'UTF8'");
            define('SET_NAMES_UTF8', true);
        }
    }
*/
	 public function setDatabase($database, $datasource = 'default'){
	 	$nds = $datasource . '_' . $database;
	 	$db  = ConnectionManager::getDataSource($datasource);
	 
	 	$db->setConfig(array(
	 			'name'       => $nds,
	 			'database'   => $database,
	 			'persistent' => false
	 	));
	 
	 	if ( $ds = ConnectionManager::create($nds, $db->config) ) {
	 		$this->useDbConfig  = $nds;
	 		$this->cacheQueries = false;
	 		return true;
	 	}
	 
	 	return false;
	 }
	 
	 
}
?>