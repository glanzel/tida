<?php
class Doodle extends AppModel {
	var $name = 'Doodle';
	var $displayField = 'title';
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'KptnTodocat' => array(
			'className' => 'KptnTodocat',
			'foreignKey' => 'kategorie',
		),
		'Admingroup' => array(
			'className' => 'Admingroup',
			'foreignKey' => 'sichtbarkeit',
		),
		'Editierbar' => array(
			'className' => 'Admingroup',
			'foreignKey' => 'editierbarkeit',
		)
	);
	
	
	function getVote($doodle_id, $user_id){
		$sqlSelect = "SELECT Appointment.id, Appointment.von, au.teilnahme FROM doodles Doodle
			LEFT JOIN appointments Appointment ON Appointment.doodle_id = Doodle.id
			LEFT JOIN appointments_users au ON au.appointment_id = Appointment.id AND au.user_id = $user_id
			WHERE Doodle.id = '$doodle_id'";
			$ergs = $this->query($sqlSelect);
		return $ergs;
	}
	
	function openDoodle($user_id){
		$sqlSelect = "SELECT au.id, Doodle.* FROM doodles Doodle
				INNER JOIN admingroups_users agroup ON agroup.admingroup_id = Doodle.sichtbarkeit AND agroup.user_id = $user_id
				INNER JOIN appointments Appointment ON Appointment.doodle_id = Doodle.id
				LEFT JOIN appointments_users au ON au.appointment_id = Appointment.id AND au.user_id = $user_id
				WHERE au.id IS NULL GROUP BY Doodle.created";
		$ergs = $this->query($sqlSelect);
		//pr($ergs);	
		return $ergs;
	}
	
}
?>