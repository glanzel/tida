<?php
class Proposal extends AppModel {
	var $name = 'Proposal';
	var $displayField = 'title';
	var $actsAs = array('Containable');
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/*
	var $hasOne = array(
		'Decision' => array(
			'className' => 'Decision',
			'foreignKey' => 'proposal_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		
	);
	*/
	

	var $belongsTo = array(
		'Votegroup' => array(
			'className' => 'Votegroup',
			'foreignKey' => 'votegroup_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

	var $hasMany = array(
		'Vote' => array(
			'className' => 'Vote',
			'foreignKey' => 'proposal_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),		
		'DecisionComment' => array(
			'className' => 'DecisionComment',
			'foreignKey' => 'proposal_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
			//'joins' => array(array('table' => 'users', 'alias' => 'User', 'type' => 'left','conditions' => array('User.id = DecisionComment.user_id')))
		)
	);

}
?>