<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as 
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));


 // The settings below can be used to set additional paths to models, views and controllers.
  $app = ROOT.'/app';
  $baseapp = BASEROOT.'/app';
  App::build(array(
      'Plugin' => array("$app/plugins/", "$baseapp/plugins/"),
      'Model' =>  array("$app/Model/", "$baseapp/Model/"),
      'View/Layouts' => array("$app/View/Layouts/", "$baseapp/View/Layouts/"),
      'View/Helper' => array("$app/View/Helper/", "$baseapp/View/Helper/" ),
      'View' => array("$app/View/", "$baseapp/View/"),
      'Controller' => array("$baseapp/Controller/"),
      'Controller/Component' => array("$baseapp/Controller/Component/"),
      'Vendor' => array("$app/vendors" , "$baseapp/vendors/"),
      'locales' => array("$app/locale/", "$baseapp/locale/"),
  ));

/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

Configure::write('klingtgut.year_id', '2016');
Configure::write('klingtgut.start', '2016-07-08 17:00');
Configure::write('klingtgut.ende', '2016-07-10 16:00');

Configure::write('klingtgut.anzahlPlaetze', 2);

define('ZUSAGE', 2);
define('WARTELISTE', 1);
define('ABSAGE', 99);
