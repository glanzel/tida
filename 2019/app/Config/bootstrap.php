<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as 
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));


 // The settings below can be used to set additional paths to models, views and controllers.
  $app = ROOT.'/app';
  $baseapp = BASEROOT.'/app';
  App::build(array(
      'Plugin' => array("$app/plugins/", "$baseapp/plugins/"),
      'Model' =>  array("$app/Model/", "$baseapp/Model/"),
      'View/Layouts' => array("$app/View/Layouts/", "$baseapp/View/Layouts/"),
      'View/Helper' => array("$app/View/Helper/", "$baseapp/View/Helper/" ),
      'View' => array("$app/View/", "$baseapp/View/"),
      'Controller' => array("$app/Controller/", "$baseapp/Controller/"),
      'Controller/Component' => array("$baseapp/Controller/Component/"),
      'Vendor' => array("$app/vendors" , "$baseapp/vendors/"),
      'locales' => array("$app/locale/", "$baseapp/locale/"),
  ));

/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
Configure::write('klingtgut.year_id', '2018');
Configure::write('klingtgut.start', '2019-09-07 16:00');
Configure::write('klingtgut.ende', '2019-09-09 18:00');
Configure::write('klingtgut.anzahlPlaetze', 289);
Configure::write('mail.register.title', "Klingtgut - Anmeldebestätigung");
Configure::write('mail.from', array('noreply@epok.de' => 'epok'));

$start = strtotime(Configure::read('klingtgut.start'));
$tagzwei = $start+24*60*60;
$tagdrei = $tagzwei+24*60*60;

Configure::write('Essen.Abendessen1.zeit',  strtotime(date("Y-m-d",$start)."20:00"));
Configure::write('Essen.Fruehstueck1.zeit', strtotime(date("Y-m-d",$tagzwei)."11:00"));
Configure::write('Essen.Mittagessen1.zeit',  strtotime(date("Y-m-d",$tagzwei)."20:00"));
Configure::write('Essen.Abendessen2.zeit',  strtotime(date("Y-m-d",$tagzwei)."20:00"));
Configure::write('Essen.Fruehstueck2.zeit',  strtotime(date("Y-m-d",$tagdrei)."11:00"));


define('ZUSAGE', 2);
define('WARTELISTE', 1);
define('UNSICHER', 0);
define('ABSAGE', 99);
define('MICH_SELBST', 100);
