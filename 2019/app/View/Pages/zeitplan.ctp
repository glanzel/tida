<div class="programm">

<h4>Workshops (noch <i>mit</i> Zeit)</h4>

<!-- <dt>? Uhr</dt><dd><b></b> // </dd> -->
<dt>? Uhr</dt><dd><b>Bierkastenklettern</b> //Holger & Frickbeck </dd>
<dt>? Uhr</dt><dd><b>Frisbee</b> //Antonio	 </dd>
<dt>? Uhr</dt><dd><b>Quizshow</b> //Nils </dd>
<dt>? Uhr</dt><dd><b>Mundraubworkshop</b> //Magda </dd>
<dt>? Uhr</dt><dd><b>Baozi kochen</b> // Anna</dd>
<dt>? Uhr</dt><dd><b>Kräuterworkshop</b> //Jonatan </dd>
<dt>? Uhr</dt><dd><b>Swing</b> //Marcel </dd>

<h4>Freitag</h4>	
<dl>
<dt>ab 16h</dt><dd >Anreise</dd>
<dt>21 Uhr</dt><dd><b>Light:weave </b> // Post-Rock,Berlin </dd>
<dt>22:30 Uhr</dt><dd><b>Cup of Jazz </b> // Salon Obskur // [Jazzylectro] </dd>
<dt>0 Uhr</dt><dd><b>Lindas Tante</b> // uk funky, hip hop, garage, grime, breakbeat </dd>
<dt>? Uhr</dt><dd><b>xilian </b> // live - ambient </dd>

<dt>2 Uhr </dt><dd><b>Zeller & Audiotourist</b> // trg // [Eletronic Mainstream vs Eklektik]
<dt>4? Uhr</dt><dd><b><a href="https://soundcloud.com/djliosh">Liosh </a></b> // [Techno]</dd>
</dl>	

<h4>Samstag</h4>	
<dl>
<dt>15 Uhr</dt><dd><b><a href="http://www.bieryoga.de">Bieryoga</a></b> // der neu hype: knallt viel besser als Saftyoga </dd>
<dt>18 Uhr</dt><dd><b><a href="https://www.mixcloud.com/orens">Orens</a></b> // Funk/Soul/Organic Grooves</dd>

<dt>20 Uhr</dt><dd ><b><a href="http://petitfour-acappella.de/">Petitfour</a></b> // acapella pop (live) </dd>

<dt>22 Uhr</dt><dd><b><a href="http://www.kilaueas.de/">The Kilaueas</a></b> // Surf (live)</dd>

<dt>0 Uhr</dt><dd><b>hughda.jones</b> // Trude, Ruth & Goldammer // [Jungle/DnB]</dd>
<dt>2 Uhr</dt><dd><b>Marc W.</b> // [house]</dd>
<dt>5 Uhr</dt><dd><b> <a href="https://soundcloud.com/alexisalexisalexisalexis">  Alexis</a></b> // [techno]</dd>

</dl>	

<h4>Sonntag</h4>	
<dl>
</dl>	

<br><br><br><br>
<span style="font-size:80%">
...und so war das Programm 2015 

<h4>Freitag</h4>	
<dl>
<dt>ab 16h</dt><dd >Anreise</dd>
<dt>19:30 Uhr</dt><dd >gemeinsames Abendessen</dd>
<dt>21 Uhr</dt><dd><b>Die Rüdi Show</b> // Chanson aus den 20er Jahren</dd>
<dt>22 Uhr</dt><dd><b>Die Aufl&ouml;sung (live)</b> // [Kraut/Postrock]</dd>
<dt>0 Uhr</dt><dd><b>hughda.jones</b> // Trude, Ruth & Goldammer // [Jungle/DnB]</dd>
<dt>2 Uhr</dt><dd><b>Xilian</b> // störsignal // [house/techno]</dd>

</dl>	

<h4>Samstag</h4>	
<dl>

<dt>13 Uhr</dt><dd><b>Swing</b> Workshop 1</dd>
<dt>14 Uhr</dt><dd><b>Zeichen</b> Workshop</dd>
<dt>15 Uhr</dt><dd><b>Siebdruck</b> (eigen Shirts mitbringen)</dd>
<dt>16 Uhr</dt><dd><b>Steptanz</b> Workshop (Schuhe mitbringen)</dd>
<dt>17 Uhr</dt><dd>Horch <b>Earstorming</b> Workshop</dd>
<dt>18 Uhr</dt><dd><b>Swing</b> Workshop 2</dd>
<dt>19 Uhr</dt><dd><b>Chor</b> Workshop</dd>
<dt>20 Uhr</dt><dd>gemeinsames Abendessen</dd>
<br>
<dt><b>Live Bühne</b></dt><dd> </dd><br>
<dt>21 Uhr</dt><dd><b>Glockenspiel & Ukulele</b> </dd>
<dt>22 Uhr</dt><dd><b>Hans S&oslash;lo (live)</b> // [Neo Classic/Psychodelic Folk]</dd>
<dt>23 Uhr</dt><dd><b>Cup of Jazz </b> // Salon Obskur // [Jazzylectro]</dd>
<dt>0:30 Uhr</dt><dd><b>H&Auml;GEN DAZ </b>// sisyphos // [HipHop/DnB/Dubstep] </dd>
<dt>2 Uhr</dt><dd><b>Marc W.</b> // [techno]</dd>
<br>
<dt><b>Bar Floor</b></dt><dd> </dd><br>
<dt>21:30 Uhr</dt><dd><b>Zeller & Althoff</b> // Swing </dd>
<dt>23 Uhr</dt><dd><b>E&H </b> //  [disco & riot & funk] </dd>
<dt>0:30 Uhr</dt><dd><b>Zeller & Glanzen </b>// Trude, Ruth & Goldammer // [Eletronic Mainstream]</dd>
<dt>1 Uhr</dt><dd><b>Audiotourist </b>// [Eklektit]</dd>

</dl>

<h4>Sonntag</h4>	
<dl>	

<dt>11 Uhr</dt><dd>Heta <b>Yoga</b></dd>
<dt>13 Uhr</dt><dd><b>Swing</b> Workshop 3</dd>
<dt>15 Uhr</dt><dd>baden (mit dem Rad)</dd>

</dl>
</div>
</span>
<!-- <?= $this->Html->link('mehr infos zu den Künstlern','/Pages/programm'); ?> -->
