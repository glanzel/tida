<div>

Vom 28. Juli - 30. Juli 2017 wird drei Tage lang eine Sause nahe der polnischen Grenze steigen.<br>
<br>
<b>Wo ?</b> Die Party wird in einem kleinen Ort 85 km von Berlin-Alex und 4 km von der Oder entfernt stattfinden.<br>

Der angrenzende Bahnhof ist in 1:06h ab Berlin-Lichtenberg zu erreichen. Von dort sind es zu Fuß ca. 10 Minuten.<br>

Die Anfahrt mit dem Auto dauert laut Google 1:30h.<br>
<br>
 

<b>Was ?</b> Es wird abends / nachts 2 Tanzflächen und Livemusik in den schönen Gemäuern geben.
<br>
Tagsüber wird es verschiedene Workshops / Herumhängen / Kochen auf einem hügeligen von Wasser umschlossenen Gelände geben.
<br>
<br> 

<b>Wie ?</b> Kosten, Koch und Putzaktivitäten werden von allen getragen. Eine Beteiligung in Form von Workshops / Performance / Lesungen / musikalischen Auftritten ist ausdrücklich erwünscht.
<br>
Kinder sind willkommen.<br>

Eine Anmeldung ist obligatorisch.<br>

Wir machen keinen Gewinn mit dem Fest, wollen aber die Kosten auf alle umlegen. Wir rechnen mit rund 35-45 Euro pro Person für das ganze Wochenende inklusive Essen, Getränken und Übernachtung. Die Übernachtung kann in Zelten oder in den Kasematten stattfinden. Mehr Infos siehe unter Logistik.<br>
<br>
 

<b>Wer ?</b> Du und viele andere. Das Gelände ist groß genug auch falls es etwas mehr werden. Ladet also ruhig eure Freund*innen/Bekannte ein mitzukommen. Wir benötigen allerdings möglichst rechtzeitige Interessensbekundungen.<br>

<b>Spread the word.</b><br>
<br>
<b>Wir freuen uns!!!</b><br>


Weitere Infos sowie das konkrete Programm folgen. <br>
<br>

Alle Organisator*innen haben auch in diesem Jahr Geburtstag.<br>
<br>
anja, antonio, cathrin, dirk, grischan, hanna, marc, moritz, nils,...<br>

<br><br>

